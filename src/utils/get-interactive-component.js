import Initialized from '../examples/initialized';
import Viewed from '../examples/viewed';
import ChecklistItems from '../examples/checklist-items';
import ChecklistCompleted from '../examples/checklist-completed';
import Uploaded from '../examples/uploaded';
import Searching from '../examples/searching';
import SearchResult from '../examples/search-result';
import LikedDisliked from '../examples/liked-disliked';
import Terminated from '../examples/terminated';
// import Suspended from '../examples/lesson-attempt/suspended';
// import Resumed from '../examples/lesson-attempt/resumed';
// import TerminatedLessonAttempt from '../examples/lesson-attempt/terminated-lesson-attempt';


export const getPageComponent = (type) =>{
  // console.log(type)
  let pages = {}
  // Attempt Lesson
  pages["initialized"] = Initialized;
  pages["viewed"] = Viewed;
  pages["checklist-items"] = ChecklistItems;
  pages["checklist-completed"] = ChecklistCompleted;
  pages["uploaded"] = Uploaded;
  pages["searching"] = Searching;
  pages["search-result"] = SearchResult;
  pages["liked-disliked"] = LikedDisliked;
  pages["terminated"] = Terminated;

  return pages[type.toLowerCase()]
}
export const singleDelim = response => {
    if (!Array.isArray(response) || response.length < 1) {
      return {
        error:
          "The response parameter must be an array with at least one array item."
      };
    }

    let finalAnswer = "";
    let length = response.length;
    for (let ii = 0; ii < length; ii++) {
      finalAnswer += response[ii];
      if (ii !== length - 1) {
        finalAnswer += "[,]";
      }
    }

    return finalAnswer;
  };

  
  export const parseLaunchParams = (parsedURL, baseconf) => {

    const sp = parsedURL.search;
    const params = new URLSearchParams(sp);
    if(!baseconf){
      baseconf = {};
    }
    if (params.get("endpoint")) {
      baseconf["endpoint"] = params.get("endpoint");
    }
    if (params.get("auth")) {
      baseconf["auth"] = params.get("auth");
    }
  
    if (params.get("actor")) {
      baseconf["actor"] = params.get("actor");
    }
    if (params.get("registration")) {
      baseconf["registration"] = params.get("registration");
    }
  
    if (params.get("activity_id")) {
      baseconf["activity_id"] = params.get("activity_id");
    }
  
    if (params.get("grouping")) {
      baseconf["grouping"] = params.get("grouping");
    }
    return baseconf;
  };
import React, { useEffect, useState } from "react";

import {
  useApplicationState,
  useApplicationDispatch,
} from "../context/app-context";

// import { VIEWED, EXITED } from "../interactive-components/constants";

import { Flex, Stack, Text, Box } from "@chakra-ui/core";


const FeedBack = ({ message }) => {
  return (
    <Box mt="20px" d="flex" width={["320px","400px","600px","800px"]}>
      <Text color="gray.600" mr="4px" fontWeight="600">{"FEEDBACK:"}</Text>
      <Text color="gray.500">{message}</Text>
    </Box>
  );
};

const Content = ({
  type,
  title,
  content,
  label,
  feedbackData,
  component: Component,
  data,
}) => {
  const context = useApplicationState();
  const dispatch = useApplicationDispatch();

  const {
    sendStatement,
    state: { showFeedback },
  } = context;

  const [feedback, setFeedback] = useState(null);

  // useEffect(() => {
  //   async function postData() {
  //     let result = null;
  //     if (type === "complete") {
  //       result = sendStatement({ type: EXITED, payload: { label } }, dispatch);
  //     }
  //   }
  //   //postData();
  // }, [Component]);

  const onUpdate = async (obj) => {
    let result = await sendStatement(obj, dispatch);
    //  find feedback object based on statement type
    let found = feedbackData.find((item) => item.id === obj.type);

    if (!result.error) {
      if (found) {
        setFeedback(found.success);
      }
      dispatch({ type: "FEEDBACK", payload: true });
    }
  };

  return (
    <Flex
      // w="80%"
      direction="column"
      // alignItems="center"
      mt="2%"
      ml="20px"
      mr="20px"
    >
      <Stack width={["320px", "400px", "640px", "660px"]} >
        {Component && <Component title={title} content={content} data={data} onClick={onUpdate} />}
        {showFeedback && <FeedBack message={feedback} />}
      </Stack>
    </Flex>
  );
};

export default Content;

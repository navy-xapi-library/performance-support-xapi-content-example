import React from "react";

import { useApplicationState } from "../context/app-context";

import { useSpring, animated } from "react-spring";

import {
  Drawer,
  DrawerOverlay,
  DrawerHeader,
  DrawerBody,
  useDisclosure,
  DrawerContent,
  TabList,
  Tabs,
  Tab,
} from "@chakra-ui/core";

const StatementViewer = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();

  const context = useApplicationState();
  const {
    state: { statement, disableStatementViewer, hideStatementViewer },
  } = context;
  //disableStatementViewer
  const statementAvailable = useSpring({
    to: async (next, cancel) => {
      await next({
        opacity: 1,
        color: disableStatementViewer ? "#a1a1a1" : "#003366",
      });
      // await next({opacity: 0, color: 'rgb(14,26,19)'})
    },
    from: { opacity: 0, color: disableStatementViewer ? "#003366" : "#a1a1a1" },
  });

  // grab latest statement and strip out the id property
  let stmt = JSON.parse(statement);
  stmt = { ...stmt, id: undefined };

  return (
    <>
      {!hideStatementViewer && (
        <>
          <Tabs
            ref={btnRef}
            bg="gray.200"
            onClick={disableStatementViewer ? null : onOpen}
            position="fixed"
            top="0vh"
            right="0vh"
            isFitted
            variant="enclosed"
          >
            <TabList>
              <Tab>
                {/* <Text color="gray.600" fontWeight="600"> */}
                <animated.div style={statementAvailable}>
                  <span className="statement-viewer-font">
                    Statement Viewer
                  </span>
                </animated.div>
                {/* </Text> */}
              </Tab>
            </TabList>
          </Tabs>
          <Drawer
            placement="right"
            onClose={onClose}
            isOpen={isOpen}
            isFullHeight="true"
            size="xlg"
          >
            <DrawerOverlay />
            <DrawerContent width={["90vw", "60vw", "60vw", "60vw"]}>
              <DrawerHeader borderBottomWidth="1px">
                XAPI STATEMENT
              </DrawerHeader>
              <DrawerBody id="drawer-body-content">
                <pre className="pre">{JSON.stringify(stmt, undefined, 2)}</pre>
              </DrawerBody>
            </DrawerContent>
          </Drawer>
        </>
      )}
    </>
  );
};

export default StatementViewer;

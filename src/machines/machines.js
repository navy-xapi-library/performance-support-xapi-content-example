import { Machine } from "xstate";
import { assign } from "@xstate/immer";

const context = {
  data: undefined,
  error: undefined,
};
export const fetchMachine = Machine(
  {
    id: "fetch",
    initial: "idle",
    context,
    states: {
      idle: {
        on: { FETCH: "loading" },
      },
      loading: {
        invoke: {
          src: "fetchData",
          onDone: {
            target: "success",
            actions: "setData",
          },
          onError: {
            target: "failure",
          },
        },
      },
      success: {},
      failure: {
        on: {
          RETRY: "loading",
        },
      },
    },
  },
  {
    actions: {
      setData: assign((ctx, event) => (ctx.data = event.data.data)),
    },
    services: {
      fetchData: "fetchData",
    },
  }
);

export const navigationMachine = (totalPages) => {
  return Machine(
    {
      id: "state",
      initial: "idle",
      context: {
        totalPages,
        currentIndex: 0,
        next: true,
        previous: false,
        error: undefined,
      },
      states: {
        idle: {
          on: {
            NEXT_PAGE: "next",
            PREVIOUS_PAGE: "previous",
            GO_TO_PAGE: [
              {
                target: "goTo",
                cond: "inBound",
              },
              {
                actions: "outOfBounds",
              },
            ],
          },
        },
        next: {
          exit: (ctx) => {
            if (ctx.totalPages === ctx.currentIndex) assign((ctx.next = false));
          },
          on: {
            "": [
              {
                target: "idle",
                cond: "isNext",
                actions: ["increment", "enabledPrevious"],
              },
              { target: "idle", cond: "isEnd", actions: ["disableNext"] },
            ],
          },
        },
        previous: {
          exit: (ctx) => {
            if (ctx.currentIndex === 0) assign((ctx.previous = false));
          },
          on: {
            "": [
              {
                target: "idle",
                cond: "isPrevious",
                actions: ["decrement", "enabledNext"],
              },
              { target: "idle", cond: "isStart", actions: ["disablePrevious"] },
            ],
          },
        },
        goTo: {
          
          on: {
            "": {
              target: "idle",
              actions: (ctx, {payload }) =>{              
                let atStart = payload === 0 ? true : false;
                let atEnd = payload === ctx.totalPages ? true : false;

                assign( 
                  ctx.currentIndex = payload, 
                  ctx.next = atEnd ? false : true, 
                  ctx.previous = atStart ? false : true
                )
              }
            },
          },
        },
      },
    },
    {
      actions: {
        increment: assign((ctx) => ctx.currentIndex++),
        decrement: assign((ctx) => ctx.currentIndex--),
        enabledPrevious: assign((ctx) => (ctx.previous = true)),
        enabledNext: assign((ctx) => (ctx.next = true)),
        disablePrevious: assign((ctx) => (ctx.previous = false)),
        disableNext: assign((ctx) => (ctx.next = false)),
        outOfBounds: assign((ctx, event) => {
          console.error(`The index ${event.payload} is out of bounds`);
        }),
      },
      guards: {
        isNext: (ctx) => ctx.currentIndex < ctx.totalPages,
        isPrevious: (ctx) => ctx.currentIndex >= 1,
        isEnd: (ctx) => ctx.currentIndex === ctx.totalPages,
        isStart: (ctx) => ctx.currentIndex === 0,
        inBound: (ctx, event) => {
          console.log("inbound");
          return event.payload >= 0 && event.payload <= ctx.totalPages;
        },
      },
    }
  );
};

import React, { useEffect } from "react";
import ReactDOM from "react-dom";

import App from "./app";

// main app css since we aren't using the styles 
// from chakra-ui for the pages
import "./examples/styles.css";

import { ThemeProvider, ColorModeProvider, CSSReset } from "@chakra-ui/core";
// xstate machine hook
import { useMachine } from "@xstate/react";

import { fetchMachine } from "./machines/machines";

import axios from "axios";

import { ApplicationProvider } from "./context/app-context";

const fetchModuleData = () => {
  const params = new URLSearchParams(window.location.search);
  let example = params.has("example")
    ? params.get("example")
    : "performance-support";
  return axios.get(`./data/${example}.json`);
};

function DataProvider({ children }) {
  const [current, send] = useMachine(fetchMachine, {
    services: {
      fetchData: fetchModuleData,
    },
    devTools: true,
  });

  useEffect(() => {
    if (current.matches("idle")) {
      send("FETCH");
    }
  }, []);

  return (
    <>
      {current.matches("success") && (
        <ApplicationProvider data={current.context.data}>
          <App data={current.context.data}/>
        </ApplicationProvider>
      )}
    </>
  );
}

ReactDOM.render(
  <ThemeProvider>
    <ColorModeProvider>
      <CSSReset />
      <DataProvider />
    </ColorModeProvider>
  </ThemeProvider>,
  document.getElementById("root")
);

import React, { useState, useEffect } from "react";

import {
  useApplicationState,
  useApplicationDispatch,
} from "../context/app-context";

// import { MENU_SELECTED } from "../interactive-components/constants";

import { FaBars, FaEye } from "react-icons/fa";

// UI LIB
import {
  IconButton,
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  useDisclosure,
  Text,
  Button,
  Box,
  Stack,
  Image,
} from "@chakra-ui/core";

const SideDrawer = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();

  const dispatch = useApplicationDispatch();
  const context = useApplicationState();

  const { data, state } = context;
  const { lessons } = data;
  const {
    currentLesson,
    // showWelcome,
    // sideDrawerItems,
    hideSideDrawer,
  } = state;
  // const { context: navigationContext } = current;

  const [firstLoad, setFirstLoad] = useState(true);
  const [selected, setSelected] = useState(null);

  useEffect(() => {
    setFirstLoad(false);
  }, []);

  useEffect(() => {
    //  let index = currentPage;
    if (!firstLoad) {
      // setSelected({ name: pages[index].label, value: index });
      setSelected(currentLesson);
    }
  }, [currentLesson]);

  const menuHandler = (index) => {
    // send({ type: "GO_TO_PAGE", payload: index });
    dispatch({ type: "SET_CURRENT_LESSON", payload: index });
    // sendStatement({ type: MENU_SELECTED, payload: { value: selected.name } });
    setSelected(index);
    onClose();
  };

  return (
    <>
      {!hideSideDrawer && (
        <>
          <IconButton
            ref={btnRef}
            aria-label="Pages"
            icon={FaBars}
            color="#fff"
            border="0"
            bg="#003366"
            variantColor="blue"
            onClick={onOpen}
          />
          <Drawer
            isOpen={isOpen}
            placement="left"
            onClose={onClose}
            finalFocusRef={btnRef}
            size="sm"            
          >
            <DrawerOverlay />
            <DrawerContent maxW="375px">
              <DrawerCloseButton
                color="#003366"
                border="0"
                maxH="20px"
                maxW="20px"
              />
              <DrawerHeader
                style={{
                  borderBottom: "solid 2px rgba(210, 215, 217, 0.75)",
                }}
              >
                <Stack isInline alignItems="center">
                  <Image size="120px" src="./assests/netc_logo.png" />
                  <Text fontWeight="700" color="#003366">
                    {data.name}
                  </Text>
                </Stack>
              </DrawerHeader>
              <DrawerBody>
                <span style={{ minHeight: "20px" }}></span>
                {lessons.map((item, index) => {
                  return (
                    <Button
                      variant="ghost"
                      // variantColor="whiteAlpha"
                      w="100%"
                      onClick={() => menuHandler(index)}
                      key={index}
                      // disabled={sideDrawerItems[index]}
                    >
                      <Stack
                        isInline={true}
                        position="absolute"
                        left="10px"
                        w="100%"
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <Text
                          color="#003366"
                          fontSize={["1rem", "1rem", "1rem", "1rem"]}
                        >
                          {item.name}
                        </Text>
                        {selected === index && (
                          <Box
                            as={FaEye}
                            color="#2699FB"
                            mt="4px"
                            marginRight="20px"
                            size="20px"
                          />
                        )}
                      </Stack>
                    </Button>
                  );
                })}
              </DrawerBody>
            </DrawerContent>
          </Drawer>
        </>
      )}
    </>
  );
};

export default SideDrawer;

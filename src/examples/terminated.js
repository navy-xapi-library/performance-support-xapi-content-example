import React, { useEffect } from "react";

import { performanceSupport } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

const Viewed = () => {
  const dispatch = useApplicationDispatch();

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // Most of all of the ELearningLib methods return a promise.
    // terminate is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
    async function start() {     

      await performanceSupport.terminate(
        "https://navy.mil/netc/xapi/activities/applications/1b8d161c-c14f-11ea-b3de-0242ac130004",
        "Like & Dislike Buttons",
        "Learn how send a page view.",
      ).catch(error => console.log(error));

      dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
    }
    // dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SHOW_NAVIGATION", payload: false });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "SET_RESULT", payload: undefined });

    start();
  }, []);

  return (
    <div className="page-container">
      <h1 className="terminate-header">Ending an attempt on the application</h1>
      <p>
        When a learner closes or exits the application, a <b>Terminated Attempt Statement</b> is issued. 
        This serves as a final statement for the attempt on the application. 
      </p>
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer Button</b> to see the <b>Terminated Attempt Statement</b> that was generated 
        for this page. 
      </p>
      <p >
        The Verb is set to the "terminated" Verb, the Object is the same Object as was used in the 
        <b> Initialized Attempt Statement</b>. Also the <span className="code-text">context.registration</span> is the same as the one 
        used in all the statements in this attempt. See the <b>NETC Performance Support Profile</b> for 
        additional requirements for the statement.
      </p>
      <p className="top-bottom-margins">
        This ends the Performance Support content example. Click any of the menu sections to review again.
      </p>
    </div>
  );
};

export default Viewed;

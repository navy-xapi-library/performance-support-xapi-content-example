import React, { useEffect, useState } from "react";

import { performanceSupport } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

import {
  IconButton
} from "@chakra-ui/core";

const Uploaded = () => {
  const dispatch = useApplicationDispatch();
  const [attachment, setAttachment] = useState(null);
  const [toggle, setToggle] = useState(false);

  const onChangeHandler = (e) => {
    setAttachment(e.target.files[0]);
    setToggle(true);
  };
  const uploadAttachment = (e) => {
    e.preventDefault();
    
    // use FileReader to create an array buffer 
    var reader = new FileReader();
    reader.onload = async function () {
      var arrayBuffer = this.result;

      let attach = performanceSupport.attachment(
        arrayBuffer,
        "image",
        "image/jpeg",
        "Image Attachment.",
        "Uploading An Image Attachment."
      );

      // Most of all of the ELearningLib methods return a promise.
      // uploaded is one of them so you can use await to wait for
      // a response to return and use catch to catch an error.
      // In most cases, the response will be an empty object indicating no errors
      // otherwise an error object is return through catch describing what the
      // error could be.
      await performanceSupport.uploaded(
        "https://navy.mil/netc/xapi/activities/files/2ba63c0a-c207-11ea-b3de-0242ac130004",
        "Some File",
        "A File uploaded while taking the Performance Support Content Example",
        attach
      ).catch(error => console.log(error));

      dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
      dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
      dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
      dispatch({ type: "ENABLE_NEXT", payload: true });

    };
    reader.readAsArrayBuffer(attachment);
  };

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "SET_RESULT", payload: undefined});
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Uploading a File</h1>
      <p>
        Performance support content may provide users with the ability to
        organize and upload various types of files and resources to an
        application such as notes, images, documents, etc.
      </p>
      <p className="top-bottom-margins">
        Click the <b>Choose File Button</b> and select a file from your computer
        to upload. Next, click the
        <b> Paperclip Button</b> to upload the file and send the{" "}
        <b>Uploaded File Statement</b>.
      </p>
      <div className="uploaded-input-container">
        <form id="form" style={{marginLeft:"10px"}}>
          <input type="file" id="myFile" name="filename" onChange={onChangeHandler} />
        </form>
        <IconButton
          display={!toggle ? "none" : "inline-block"}
          disabled={!toggle}
          variantColor="blue"
          bg="#003366"
          icon="attachment"
          onClick={uploadAttachment}
          ></IconButton>
      </div>
      <p>
        Click the Statement Viewer Button to see the Uploaded File Statement
        that was generated.
      </p>
      <p className="top-bottom-margins">
        The Verb is set to the "uploaded"{" "}
        Verb, the <span className="code-text">object.id</span> is set to a
        unique identifier for the file, and the{" "}
        <span className="code-text">object.definition.type</span> is set to 
        <span className="code-text"> http://adlnet.gov/expapi/activities/file</span>. 
        Requirements for creating this Statement can be found in the{" "}
        <b>NETC Common Reference Profile</b>.
      </p>
      <p style={{ marginBottom: "80px" }}>
        When you have finished exploring the Statement, click the{" "}
        <b>NEXT Button</b> to continue to the next page,{" "}
        <i>"Searching For Terms"</i> or freely navigate by clicking one of the
        links in the main menu.
      </p>
    </div>
  );
};

export default Uploaded;

import React, { useEffect } from "react";

import { performanceSupport, ACTIVITY } from "./configuration";

import { useApplicationDispatch, useApplicationState } from "../context/app-context";

import { getColorLinks } from "./searching";

const colors =  [
  { id:1, url:"www.red.color", link: "Red | Definition of Red", description: "Red definition, any of various colors resembling the color of blood."}, 
  { id:2, url:"www.rust.color", link: "Rust (color) | Definition of Rust", description: "Rust is an orange-brown color resembling iron oxide."}, 
]

const SearchResult = () => {
  const dispatch = useApplicationDispatch();
  const context = useApplicationState();

  const { searchResult } = context.state;

  const selectedHandler = async(item, index) =>{   

    const parent = performanceSupport.createParent(
      "https://navy.mil/netc/xapi/activities/search-engine/0c6114c0-c219-11ea-b3de-0242ac130004",
      "Application Search Engine",
      "The search engine used in the performance support application.",
    );

    // Most of all of the ELearningLib methods return a promise.
    // selected is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
    await performanceSupport.selected(
      ACTIVITY.LINK,
      `https://navy.mil/netc/xapi/activities/links/link${index}`,
      item.link,
      item.description,
      parent
    ).catch(error => console.log(error));

    dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
  }

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "SET_RESULT", payload: undefined});
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Selecting a Search Result</h1>
      <p>
        Continuing the search example, this page will show the xAPI Statement
        that is generated when a user clicks one of the linked search results.
        After a user searches for a term in a performance support application,
        the search engine typically returns several results to be displayed to
        the user. Each of the results are linked to either an internal or
        external resource related to the search term. Select a search result below.
      </p>
      {
        getColorLinks(searchResult ? searchResult : "red  ").map((item, index) =>
          <div className="search-result" key={item.url} style={{cursor:"pointer"}}>
            <div style={{marginLeft:"10px"}} onClick={() => selectedHandler(item, index+1)}>
              <p>{item.url}</p>
              <h4 className="result-links">{item.link}</h4>
              <p>{item.description}</p>
            </div>
          </div>
        )          
      }
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer Button</b> to see the{" "}
        <b>Selected Search Result Statement</b> that was generated.
      </p>
      <p>
        The Verb is set to the "selected"{" "}
        Verb, the <span className="code-text">object.id</span> is the link of
        the search result the learner selected and the{" "}
        <span className="code-text">object.definition.type</span> is
        <span className="code-text">
          {" "}
          http://adlnet.gov/expapi/activities/link
        </span>
        . The
        <span className="code-text">
          {" "}
          context.contextActivities.parent
        </span>{" "}
        included the Activity representing the search engine and the{" "}
        <span className="code-text">result.extensions</span> contain the top
        search results.
      </p>
      <p className="top-bottom-margins">
        When you have finished exploring the Statement, click the{" "}
        <b>NEXT Button</b> to continue to the next page,{" "}
        <i>"Liking or Disliking a Resource"</i> or freely navigate by clicking
        one of the links in the main menu.
      </p>
    </div>
  );
};

export default SearchResult;

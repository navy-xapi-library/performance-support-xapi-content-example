import React, { useEffect } from "react";

import { performanceSupport, ACTIVITY } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

import {FaThumbsDown, FaThumbsUp } from "react-icons/fa"

const LikedDisliked = () => {
  const dispatch = useApplicationDispatch();

  const onSubmitHandler = ( value ) =>{

    const parent = performanceSupport.createParent(
      "https://navy.mil/netc/xapi/activities/search-engine/0c6114c0-c219-11ea-b3de-0242ac130004",
      "Application Search Engine",
      "The search engine used in the performance support application.",
    );

    // Most of all of the ELearningLib methods return a promise.
    // liked or disliked is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.

    value === "liked" ?
    performanceSupport.liked(        
      ACTIVITY.LINK,
      "https://navy.mil/netc/xapi/activities/links/f0f22af6-c224-11ea-b3de-0242ac130004",
      "Link1",
      "A link to a resource.",
      parent
    ).catch(error => console.log(error)) :
    performanceSupport.disliked(
      ACTIVITY.LINK,
      "https://navy.mil/netc/xapi/activities/links/f0f22af6-c224-11ea-b3de-0242ac130004",
      "Link1",
      "A link to a resource.",
      parent
    ).catch(error => console.log(error))

    dispatch({ type: "STATEMENT", payload:performanceSupport.statement });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });  
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
    dispatch({ type: "ENABLE_NEXT", payload: true });
  }

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "SET_RESULT", payload: undefined});
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });

  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Liking or Disliking a Resource</h1>
      <p>
        It may be useful to track which things are liked or disliked the most by the users in order to 
        provide quantitative insights and analytics. This page demonstrates the xAPI Statement that is 
        generated when a user likes or dislikes one of the search result resource links.  
      </p>
      <div className="search-result">
        <div style={{marginLeft:"10px"}}>
          <p>www.link1.com</p>
          <h4 style={{color:"blue"}}>Link | Definition of Link</h4>
          <p>Verb (1) linked; linking; links. Definition of link (Entry 2 of 4) transitive Verb. : to couple or connect by or as if by a link.</p>
        </div>
        <div className="like-disliked-container">
        <FaThumbsUp size="30px" className="like-disliked-icons" onClick={()=> onSubmitHandler("liked")}/>
        <FaThumbsDown size="30px" className="like-disliked-icons" onClick={()=> onSubmitHandler("disliked")}/>
        </div>
      </div>
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer Button</b> to see the <b>Liked or Disliked Statement</b> that was generated 
        for this page. 
      </p>
      <p >
        The Verb is set to the "liked" (or "disliked") Verb, the <span className="code-text">object.id</span> is a unique identifier 
        representing the thing the learner liked, and the <span className="code-text">object.definition.type</span> is the type of 
        thing that was liked. See the <b>NETC Common Reference</b> and <b>Performance Support 
        Profiles</b> for lists of Activity types, and additional requirements for <b>Liked</b> or <b>Disliked</b> Statements.
      </p>
      <p className="top-bottom-margins">
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, "Ending an Attempt on the Application" or freely navigate by clicking one of the links 
        in the main menu.
      </p>
    </div>
  );
};

export default LikedDisliked;

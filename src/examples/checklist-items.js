import React, { useEffect } from "react";

import { performanceSupport, config, ACTIVITY } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";


const ChecklistItems = () => {
  const dispatch = useApplicationDispatch();

  const updateChecklist = async (e) => {    
    let choice =  e.target.checked ? "selected" : "deselected";

    // Most of all of the ELearningLib methods return a promise.
    // selected is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.

    let parent = performanceSupport.createParent(
      "https://navy.mil/netc/xapi/activities/checklists/ca72ea12-c186-11ea-b3de-0242ac130004",
      "Performance Support Example Checklist",
      "A Performance Support Example Checklist to show how to select and deselect a checklist item."
    );
    
    e.target.checked ?
    await performanceSupport.selected(
      ACTIVITY.CHECKLIST_ITEM,
      "https://navy.mil/netc/xapi/activities/checklist-items/f0f22af6-c224-11ea-b3de-0242ac130004",
      "Checklist item",
      "Learn how to select a checklist item.",
      parent
    ).catch(error => console.log(error))
    :
    await performanceSupport.deselected(
      ACTIVITY.CHECKLIST_ITEM,
      "https://navy.mil/netc/xapi/activities/checklist-items/f0f22af6-c224-11ea-b3de-0242ac130004",
      "Checklist item",
      "Learn how to select a checklist item.",
      parent
    ).catch(error => console.log(error));

    // set global UI states
    dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "ENABLE_NEXT", payload: true });
  };

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // set some global UI states
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SET_RESULT", payload: undefined});
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Selecting Checklist Items</h1>
      <p>
        A checklist is often used in performance support scenarios to help users follow a policy or process correctly. 
        Checklist items can be selected or deselected.
      </p>
      <p className="top-bottom-margins">
        Click each checklist item to see the <b>Selected Checklist Item Statement</b> or the <b>Deselected 
        Checklist Item Statement</b> that was generated. 
      </p>
      <div className="checkbox-item">        
        <input className="checkbox-input" type="checkbox" id="cb" onChange={updateChecklist}></input>
        <label className="checkbox-label">Task 1</label>
      </div>
      <p>
        Click the <b>Statement Viewer Button</b> to see the Statements that were generated.
      </p>
      <p className="top-bottom-margins">
        The Verb is set to the "selected" Verb, the <span className="code-text">object.id</span> is set to a unique identifier 
        representing the specific checklist item, and the <span className="code-text">object.definition.type</span> is set to 
        <span className="code-text"> http://id.tincanapi.com/activitytype/checklist-item</span>. The same Registration 
        ID created in the <b>Initialized Application Statement</b> is used in this Statement to group these 
        Statements in the same attempt. Finally, the <span className="code-text">context.contextActvities.parent </span> 
        contains the Activity object representing the checklist that includes this checklist item. The 
        requirements for constructing a <b>Selected Checklist Item Statement</b> can be found in the 
        <b> Performance Support Reference Profile</b>.
      </p>
      <p style={{marginBottom:"80px"}}>
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the next page, <i>"Completing a Checklist"</i>.
      </p>
    </div>
  );
};

export default ChecklistItems;

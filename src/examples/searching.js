import React, { useEffect, useState } from "react";

import { performanceSupport } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

import {
  IconButton,
} from "@chakra-ui/core";

import { FaSearch } from "react-icons/fa";

const Searching = () => {
  const dispatch = useApplicationDispatch();
  const [value, setValue] = useState("");
  const [ showResults, setShowResults] = useState(false)
  const handleChange = (event) => setValue(event.target.value);

  const onSubmitHandler = (e) =>{
    if(value === "") return;

    setShowResults(true);

    // Most of all of the ELearningLib methods return a promise.
    // searched is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
    performanceSupport.searched(
      value, 
      "https://navy.mil/netc/xapi/activities/search-engine/b1aa0f4c-c20d-11ea-b3de-0242ac130004",
      "Application Search",
      "A search activity performed using the application’s user interface."
    ).catch(error => console.log(error));

    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "SEARCH_RESULT", payload: value});
    dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
  }

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Searching for Terms</h1>
      <p>
        Performance support applications may provide users with the ability to quickly find information 
        they are looking for either within the performance support app or in externally searched resources. 
      </p>
      <p className="top-bottom-margins">
        It is recommended to use a debounce delay to prevent sending Statements for each 
        keystroke. Only send a Statement for the full search term.
      </p>
      <p >
        This example will show a <b>Searched Terms Statement</b> after the user enters the term into the 
        search field and clicks the search button. Type "blue," "red," or "yellow" into the search field 
        and then click the search button to see the results.
      </p>
      <div className="search-container">
        <input type="search" placeholder="Search Term" onChange={handleChange} disabled={showResults}/>
        <IconButton 
              ml="2px"
              icon={FaSearch}
              bg="#003366"
              color="#fff"
              onClick={onSubmitHandler}
              disabled={showResults}
        ></IconButton>
      </div>
      { 
        showResults &&                   
            getColorLinks(value).map(item =>
              <div className="search-result" key={item.url}>
                <div style={{marginLeft:"10px"}}>
                  <p>{item.url}</p>
                  <h4 style={{color:"blue"}}>{item.link}</h4>
                  <p>{item.description}</p>
                </div>
              </div>
            )          
      }
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer Button</b> to see the <b>Searched Terms Statement</b> that was generated. 
      </p>
      <p >
        Notice that <span className="code-text">result.response</span> is used to store the search term entered by the user, the 
        Verb is set to the "searched" Verb, the <span className="code-text">object.id</span> is a unique identifier representing the 
        search engine, and the <span className="code-text">object.definition.type</span> is set to 
        <span className="code-text"> https://w3id.org/xapi/acrossx/activities/search-engine</span>. See the <b>NETC 
        Performance Support Profile</b> for the full list of requirements.
      </p>
      <p className="top-bottom-margins">
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, <i>"Selecting a Search Result"</i>.
      </p>
    </div>
  );
};

export const getColorLinks = ( color ) =>{
  let colorValue = color.toLowerCase();
    if(colorValue !== "red" && colorValue !== "blue" && colorValue !== "yellow"){
      // default to red
      colorValue = "red";
    }
    let colors = {};
    colors["red"] = [
      { url:"www.red.color", link: "Red | Definition of Red", description: "Red definition, any of various colors resembling the color of blood."}, 
      { url:"www.rust.color", link: "Rust (color) | Definition of Rust", description: "Rust is an orange-brown color resembling iron oxide."}, 
    ];
    colors["blue"] = [
      { url:"www.blue.color", link: "Blue | Definition of Blue", description: "Blue definition, the pure color of a clear sky."}, 
      { url:"www.navyblue.color", link: "Navy Blue | Definition of Navy Blue", description: "Navy blue is a very dark shade of the color blue."}, 
    ];
    colors["yellow"] = [
      { url:"www.yellow.color", link: "Yellow | Definition of Yellow", description: "Yellow is the color between orange and green on the spectrum of visible light."}, 
      { url:"www.amber.color", link: "Amber | Definition of Amber", description: "The color amber is a pure chroma color, located on the color wheel midway between the colors of yellow and orange."}, 
    ];

    return colors[colorValue];
}


export default Searching;

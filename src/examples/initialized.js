import React, { useEffect } from "react";

import { performanceSupport, config } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

const Initialized = () => {
  const dispatch = useApplicationDispatch();

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // Most of all of the ELearningLib methods return a promise.
    // startLessonAttempt is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
    async function start() {
      // set activity and group for the lesson attempt lesson
      await performanceSupport.initialize(
        "https://navy.mil/netc/xapi/activities/applications/692f09a2-c1f4-11ea-b3de-0242ac130004",
        "Performance Support App",
        "A mobile application used at the moment of need for performance support."
      )
      .catch(error => {
        console.log("error",error);
      });
      // set initialized statement to global state for Statement Viewer
      dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
    }
    // dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
    dispatch({ type: "SET_RESULT", payload: undefined});

    start();
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Initializing an Application</h1>
      <p>
        Welcome to the NETC xAPI Performance Support Content Example. It’s
        expected that developers will "look under the hood" of these examples to
        see how the source code and libraries are utilized and can be reused for
        their own projects. The primary purpose of this example is to provide
        developers with an understanding of how to apply the{" "}
        <b>NETC Performance Support Profile</b> rules in some common types of
        content:
      </p>
      <div className="top-bottom-margins o-list">
        <ol>
          <li>Pages</li>
          <li>Checklists</li>
          <li>Files</li>
          <li>Searches & Search Results</li>
          <li>Like & Dislike Buttons</li>
        </ol>
      </div>
      <p>
        Throughout these examples notice the conventions used in the Statements:
      </p>
      <div className="top-bottom-margins o-list ">
        <ul>
          <li>Verbs represent the action performed</li>
          <li>
            Object IDs uniquely identify the thing the learner is interacting
            with - pages, checklists, search results
          </li>
          <li>
            Context Registration IDs are unique for the attempt and all
            Statements in the same attempt with have the same Registration ID
          </li>
          <li>
            The Context activities category array contains the profile the
            Statement follows
          </li>
        </ul>
      </div>
      <p>
        First, let’s start with a Statement example showing an{" "}
        <b>Initialized Application Statement</b>. This Statement indicates a new
        attempt on the application. Click the <b>Statement Viewer Button</b> to
        see the <b>Initialized Application Statement</b> that was generated for
        this page.
      </p>
      <p className="top-bottom-margins">
        The Verb is set to the "initialized"{" "}
        Verb, the <span className="code-text">object.id</span> is set to a unique identifier for the application,
        and the <span className="code-text">object.definition.type</span> is set
        to <span className="code-text"> https://w3id.org/xapi/performance-support/activity-types/application.
        </span>
        The <span className="code-text">context.registration</span> is set to a unique identifier to represent the
        attempt on this content. And the{" "}
        <span className="code-text">context.contextActivities.category</span>{" "}
        contains an object that indicates that this Statement follows the rules
        in the <b>NETC Performance Support Profile</b>. See the{" "}
        <b>NETC Performance Support Profile</b> for all Statement requirements.
      </p>
      <p style={{marginBottom:"80px"}}>
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to
        continue to the next page, <i>"Viewing a Page"</i> or navigate by
        clicking one of the links in the main menu.
      </p>
    </div>
  );
};

export default Initialized;

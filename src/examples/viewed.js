import React, { useEffect } from "react";

import { performanceSupport, ACTIVITY } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

const Viewed = () => {
  const dispatch = useApplicationDispatch();

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // Most of all of the ELearningLib methods return a promise.
    // viewed is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
    async function start() {
      // set activity and group for the lesson attempt lesson
      await performanceSupport.viewed(
        ACTIVITY.PAGE,
        "https://navy.mil/netc/xapi/activities/pages/b1db16ec-b659-11ea-b3de-0242ac130004",
        "Viewing a Page",
        "Learn how to send a page view."
      ).catch(err => console.log(err.error));

      dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
    }
    // enable next button
    dispatch({ type: "ENABLE_NEXT", payload: true });
    dispatch({ type: "SHOW_NAVIGATION", payload: true });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
    dispatch({ type: "SET_RESULT", payload: undefined});

    start();
  }, []);

  return (
    <div className="page-container">
      <h1 className="header">Viewing a Page</h1>
      <p>
        A page refers to a  single "screen" within a larger body of content. While a page could be a 
        literal page of text, it can also refer to a slide, popover modal dialog, or other element that 
        presents information. Sending page views gives insight in the learner’s path through the content. 
      </p>
      <p className="top-bottom-margins">
        Click the <b>Statement Viewer Button</b> to see the <b>Viewed Page Statement</b> that was generated 
        for this page. 
      </p>
      <p >
        Notice that the Statement Verb is the "viewed" Verb, the <span className="code-text">object.id</span> is a unique identifier for 
        this page and the <span className="code-text">object.definition.type</span> is set to 
        <span className="code-text"> https://w3id.org/xapi/acrossx/activities/page</span>. The Registration identifier 
        created for the previous <b>Initialized Application Statement</b> is used in this Statement’s  
        Registration to associate the Statements. The requirements for constructing a <b>Viewed 
        Page Statement</b> can be found in the <b>NETC Common Reference Profile</b>.
      </p>
      <p className="top-bottom-margins">
        When you have finished exploring the Statement, click the <b>NEXT Button</b> to continue to the 
        next page, <i>"Selecting Checklist Items"</i> or freely navigate by clicking one of the links in the main menu.
      </p>
    </div>
  );
};

export default Viewed;

// library can be included in a script tag
//<script src="../elearningXAPI.js"></script>
import { PerformanceSupportXAPI } from "veracity";

import base64 from "base-64";
import Chance from "chance";

// holds login info
export let config = {};

export let performanceSupport = null;
export let interactionParent = null;
export let ACTIVITY = null;

export const eLearningSetUp = (credentials) => {
  config = {
    endpoint: credentials.endpoint,
    auth:
      "Basic " +
      base64.encode(`${credentials.username}:${credentials.password}`),
    actor: {
      name: credentials.name,
      objectType: "Agent",
      account: {
        homePage: "https://edipi.navy.mil",
        name: new Chance().string({length: 10, pool: '0123456789'})
      },
    },
    platform: "Android 10.0"
  };

  // instantiate lib and pass in the config object
  performanceSupport = new PerformanceSupportXAPI(config);

  // GRAB SOME CONSTANTS
  const { SCHOOL_CENTER } = performanceSupport.EXTENSIONS
  // set to exported const ACTIVITY to be used throughout the examples
  ACTIVITY = performanceSupport.ACTIVITY;

  // adding extensions for this attempt
  performanceSupport.addExtensions(SCHOOL_CENTER,"Department of Defense (DOD)");
 
};

import React, { useEffect, useState } from "react";

import { performanceSupport } from "./configuration";

import { useApplicationDispatch } from "../context/app-context";

import { Button } from "@chakra-ui/core";


const ChecklistCompleted = () => {
  const dispatch = useApplicationDispatch();
  const [checkboxItems, setChecklist] = useState([false,false,false]);
  const [completed, setCompleted] = useState(false);
  const [ disableSubmit, setDisableSubmit ] = useState(true);

  const updateChecklist = (item, checked) => {
    let update = [...checkboxItems];
    update[item] = checked;
    setChecklist(update);
  };
  // use to send complete checklist
  const completedChecklist = (arr) => {
    let totalCB = arr.length;
    let allChecked = arr.filter((item) => item === true).length;
    return allChecked === totalCB;
  };

  const onButtonHandler = async () =>{
    // Most of all of the PerformanceSupport methods return a promise.
    // completed is one of them so you can use await to wait for
    // a response to return and use catch to catch an error.
    // In most cases, the response will be an empty object indicating no errors
    // otherwise an error object is return through catch describing what the
    // error could be.
      performanceSupport.completedChecklist(
        "https://navy.mil/netc/xapi/activities/checklists/b1db16ec-b659-11ea-b3de-0242ac130004",
        "Example Checklist",
        "A Checklist for the Performance Support Content Example.",        
      ).catch(error => console.log(error));

      dispatch({ type: "STATEMENT", payload: performanceSupport.statement });
      dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: false });
      dispatch({ type: "SET_LESSON_COMPLETE", payload: null });
      dispatch({ type: "ENABLE_NEXT", payload: true });
      setDisableSubmit(true)
  }

  // useEffect is a lifecyle method that handles the mounting,
  // unmounting, update, and other lifecycles.
  // In this case, we are using useEffect to handle mounting of the component.
  // When this component mounts it will call the startLessonAttempt once and
  // only once in the application's life time.
  useEffect(() => {
    // dispatch({ type: "SET_PAGE_COMPLETE", payload: true });
    // enable next button and set global states    
    dispatch({ type: "SHOW_NAVIGATION", payload: true });    
    dispatch({ type: "ENABLE_SIDE_DRAWER_ITEMS", payload: [true, true] });
    dispatch({ type: "HIDE_SIDE_DRAWER", payload: false });
    dispatch({ type: "HIDE_STATEMENT_VIEWER", payload: false });
    dispatch({ type: "DISABLE_STATEMENT_VIEWER", payload: true });
    dispatch({ type: "ENABLE_NEXT", payload: false });
    dispatch({ type: "SET_RESULT", payload: undefined});
  }, []);

  useEffect(()=>{
    if (completedChecklist(checkboxItems)) {
      setCompleted(true);
      setDisableSubmit(false);
    }
  }, checkboxItems)

  return (
    <div className="page-container">
      <h1 className="header">Completing a Checklist</h1>
      <p>
        Once all the checklist items have been selected the application can send
        a Statement to indicate the checklist is complete.
      </p>
      <p className="top-bottom-margins">
        The <b>Complete Checklist Button</b> will become enabled after checking
        all of the checklist items. Click the <b>Complete Checklist Button</b>{" "}
        to send the <b>Completed Checklist Statement</b>.
      </p>
      <div className="checklist">
        <div className="checklist-checkbox-container">
          <input type="checkbox" id="cb`" name="cb1" disabled={completed} onChange={(e) => updateChecklist(0, e.target.checked)}></input>
          <label className="checkbox-label" htmlFor="cb1">Task 1</label>
        </div>
        <div className="checklist-checkbox-container">
          <input type="checkbox" id="cb2" name="cb2" disabled={completed} onChange={(e) => updateChecklist(1, e.target.checked)}></input>
          <label className="checkbox-label" htmlFor="cb2">Task 2</label>
        </div>
        <div className="checklist-checkbox-container">
          <input type="checkbox" id="cb3" name="cb3" disabled={completed} onChange={(e) => updateChecklist(2, e.target.checked)}></input>
          <label className="checkbox-label" htmlFor="cb3">Task 3</label>
        </div>
      </div>
      <Button  mb="30px" onClick={onButtonHandler} disabled={disableSubmit}>
          COMPLETE CHECKLIST
        </Button>
      <p>
        Click the <b>Statement Viewer Button</b> to see the{" "}
        <b>Completed Checklist Statement</b> that was generated.
      </p>
      <p className="top-bottom-margins">
        The Verb is set to the "completed" Verb, the
        Object is set to the checklist Activity that was in the{" "}
        <span className="code-text">context.contextActivities.parent</span> for
        each checklist item Statement, and the
        <span className="code-text"> context.registration</span> ID is unique
        identifier created for the <b>Initialized Application Statement</b>.
        Also, the{" "}
        <span className="code-text">context.contextActivities.grouping</span>{" "}
        includes the Activity representing the application.
      </p>
      <p style={{ marginBottom: "80px" }}>
        When you have finished exploring the Statement, click the{" "}
        <b>NEXT Button</b> to continue to the next page,{" "}
        <i>"Uploading a File"</i> or freely navigate by clicking one of the
        links in the main menu.
      </p>
    </div>
  );
};

export default ChecklistCompleted;

(function (global, factory) {
   typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('crypto')) :
   typeof define === 'function' && define.amd ? define(['exports', 'crypto'], factory) :
   (global = global || self, factory(global.navy = {}, global.crypto));
}(this, (function (exports, crypto) { 'use strict';

   crypto = crypto && Object.prototype.hasOwnProperty.call(crypto, 'default') ? crypto['default'] : crypto;

   function setup(ADL) {
     ADL.activityTypes = {
       "assessment": "http://adlnet.gov/expapi/activities/assessment",
       "attempt": "http://adlnet.gov/expapi/activities/attempt",
       "course": "http://adlnet.gov/expapi/activities/course",
       "file": "http://adlnet.gov/expapi/activities/file",
       "cmiInteraction": "http://adlnet.gov/expapi/activities/cmi.interaction",
       "interaction": "http://adlnet.gov/expapi/activities/interaction",
       "lesson": "http://adlnet.gov/expapi/activities/lesson",
       "link": "http://adlnet.gov/expapi/activities/link",
       "media": "http://adlnet.gov/expapi/activities/media",
       "meeting": "http://adlnet.gov/expapi/activities/meeting",
       "module": "http://adlnet.gov/expapi/activities/module",
       "objective": "http://adlnet.gov/expapi/activities/objective",
       "performance": "http://adlnet.gov/expapi/activities/performance",
       "profile": "http://adlnet.gov/expapi/activities/profile",
       "question": "http://adlnet.gov/expapi/activities/question",
       "simulation": "http://adlnet.gov/expapi/activities/simulation"
     };
   }

   function setup$1(ADL) {
     ADL.verbs = {
       "abandoned": {
         "id": "https://w3id.org/xapi/adl/verbs/abandoned",
         "display": {
           "en-US": "abandoned"
         }
       },
       "answered": {
         "id": "http://adlnet.gov/expapi/verbs/answered",
         "display": {
           "de-DE": "beantwortete",
           "en-US": "answered",
           "fr-FR": "a répondu",
           "es-ES": "contestó",
           "ar-AR": "أجاب"
         }
       },
       "asked": {
         "id": "http://adlnet.gov/expapi/verbs/asked",
         "display": {
           "de-DE": "fragte",
           "en-US": "asked",
           "fr-FR": "a demandé",
           "es-ES": "preguntó",
           "ar-AR": "طلب"
         }
       },
       "attempted": {
         "id": "http://adlnet.gov/expapi/verbs/attempted",
         "display": {
           "de-DE": "versuchte",
           "en-US": "attempted",
           "fr-FR": "a essayé",
           "es-ES": "intentó",
           "ar-AR": "حاول"
         }
       },
       "attended": {
         "id": "http://adlnet.gov/expapi/verbs/attended",
         "display": {
           "de-DE": "nahm teil an",
           "en-US": "attended",
           "fr-FR": "a suivi",
           "es-ES": "asistió",
           "ar-AR": "حضر"
         }
       },
       "commented": {
         "id": "http://adlnet.gov/expapi/verbs/commented",
         "display": {
           "de-DE": "kommentierte",
           "en-US": "commented",
           "fr-FR": "a commenté",
           "es-ES": "comentó",
           "ar-AR": "علق"
         }
       },
       "completed": {
         "id": "http://adlnet.gov/expapi/verbs/completed",
         "display": {
           "de-DE": "beendete",
           "en-US": "completed",
           "fr-FR": "a terminé",
           "es-ES": "completó",
           "ar-AR": "أكمل"
         }
       },
       "exited": {
         "id": "http://adlnet.gov/expapi/verbs/exited",
         "display": {
           "de-DE": "verließ",
           "en-US": "exited",
           "fr-FR": "a quitté",
           "es-ES": "salió",
           "ar-AR": "خرج"
         }
       },
       "experienced": {
         "id": "http://adlnet.gov/expapi/verbs/experienced",
         "display": {
           "de-DE": "erlebte",
           "en-US": "experienced",
           "fr-FR": "a éprouvé",
           "es-ES": "experimentó",
           "ar-AR": "شاهد"
         }
       },
       "failed": {
         "id": "http://adlnet.gov/expapi/verbs/failed",
         "display": {
           "de-DE": "verfehlte",
           "en-US": "failed",
           "fr-FR": "a échoué",
           "es-ES": "fracasó",
           "ar-AR": "فشل"
         }
       },
       "imported": {
         "id": "http://adlnet.gov/expapi/verbs/imported",
         "display": {
           "de-DE": "importierte",
           "en-US": "imported",
           "fr-FR": "a importé",
           "es-ES": "importó",
           "ar-AR": "مستورد"
         }
       },
       "initialized": {
         "id": "http://adlnet.gov/expapi/verbs/initialized",
         "display": {
           "de-DE": "initialisierte",
           "en-US": "initialized",
           "fr-FR": "a initialisé",
           "es-ES": "inicializó",
           "ar-AR": "بدأ"
         }
       },
       "interacted": {
         "id": "http://adlnet.gov/expapi/verbs/interacted",
         "display": {
           "de-DE": "interagierte",
           "en-US": "interacted",
           "fr-FR": "a interagi",
           "es-ES": "interactuó",
           "ar-AR": "تفاعل"
         }
       },
       "launched": {
         "id": "http://adlnet.gov/expapi/verbs/launched",
         "display": {
           "de-DE": "startete",
           "en-US": "launched",
           "fr-FR": "a lancé",
           "es-ES": "lanzó",
           "ar-AR": "أطلق"
         }
       },
       "mastered": {
         "id": "http://adlnet.gov/expapi/verbs/mastered",
         "display": {
           "de-DE": "meisterte",
           "en-US": "mastered",
           "fr-FR": "a maîtrisé",
           "es-ES": "dominó",
           "ar-AR": "أتقن"
         }
       },
       "passed": {
         "id": "http://adlnet.gov/expapi/verbs/passed",
         "display": {
           "de-DE": "bestand",
           "en-US": "passed",
           "fr-FR": "a réussi",
           "es-ES": "aprobó",
           "ar-AR": "نجح"
         }
       },
       "preferred": {
         "id": "http://adlnet.gov/expapi/verbs/preferred",
         "display": {
           "de-DE": "bevorzugte",
           "en-US": "preferred",
           "fr-FR": "a préféré",
           "es-ES": "prefirió",
           "ar-AR": "فضل"
         }
       },
       "progressed": {
         "id": "http://adlnet.gov/expapi/verbs/progressed",
         "display": {
           "de-DE": "machte Fortschritt mit",
           "en-US": "progressed",
           "fr-FR": "a progressé",
           "es-ES": "progresó",
           "ar-AR": "تقدم"
         }
       },
       "registered": {
         "id": "http://adlnet.gov/expapi/verbs/registered",
         "display": {
           "de-DE": "registrierte",
           "en-US": "registered",
           "fr-FR": "a enregistré",
           "es-ES": "registró",
           "ar-AR": "سجل"
         }
       },
       "responded": {
         "id": "http://adlnet.gov/expapi/verbs/responded",
         "display": {
           "de-DE": "reagierte",
           "en-US": "responded",
           "fr-FR": "a répondu",
           "es-ES": "respondió",
           "ar-AR": "استجاب"
         }
       },
       "resumed": {
         "id": "http://adlnet.gov/expapi/verbs/resumed",
         "display": {
           "de-DE": "setzte fort",
           "en-US": "resumed",
           "fr-FR": "a repris",
           "es-ES": "continuó",
           "ar-AR": "استأنف"
         }
       },
       "satisfied": {
         "id": "https://w3id.org/xapi/adl/verbs/satisfied",
         "display": {
           "de-DE": "befriedigt",
           "en-US": "satisfied",
           "fr-FR": "satisfaite",
           "es-ES": "satisfecho",
           "ar-AR": "راض"
         }
       },
       "scored": {
         "id": "http://adlnet.gov/expapi/verbs/scored",
         "display": {
           "de-DE": "erreichte",
           "en-US": "scored",
           "fr-FR": "a marqué",
           "es-ES": "anotó",
           "ar-AR": "سحل النقاط"
         }
       },
       "shared": {
         "id": "http://adlnet.gov/expapi/verbs/shared",
         "display": {
           "de-DE": "teilte",
           "en-US": "shared",
           "fr-FR": "a partagé",
           "es-ES": "compartió",
           "ar-AR": "شارك"
         }
       },
       "suspended": {
         "id": "http://adlnet.gov/expapi/verbs/suspended",
         "display": {
           "de-DE": "pausierte",
           "en-US": "suspended",
           "fr-FR": "a suspendu",
           "es-ES": "aplazó",
           "ar-AR": "علق"
         }
       },
       "terminated": {
         "id": "http://adlnet.gov/expapi/verbs/terminated",
         "display": {
           "de-DE": "beendete",
           "en-US": "terminated",
           "fr-FR": "a terminé",
           "es-ES": "terminó",
           "ar-AR": "أنهى"
         }
       },
       "voided": {
         "id": "http://adlnet.gov/expapi/verbs/voided",
         "display": {
           "de-DE": "entwertete",
           "en-US": "voided",
           "fr-FR": "a annulé",
           "es-ES": "anuló",
           "ar-AR": "فرغ"
         }
       },
       "waived": {
         "id": "https://w3id.org/xapi/adl/verbs/waived",
         "display": {
           "de-DE": "verzichtet",
           "en-US": "waived",
           "fr-FR": "renoncé",
           "es-ES": "renunciado",
           "ar-AR": "تخلى"
         }
       }
     };
   }

   function setup$2(obj) {
     var ADL = obj;

     function getQueryVariable(variable) {
       var query = window.location.search.substring(1);
       var vars = query.split('&');

       for (var i = 0; i < vars.length; i++) {
         var pair = vars[i].split('=');

         if (decodeURIComponent(pair[0]) == variable) {
           return decodeURIComponent(pair[1]);
         }
       } // console.log('Query variable %s not found', variable);

     }

     function cb_wrap(cb) {
       return function () {
         var args = arguments;
         window.setTimeout(function () {
           var callerArgs = [];

           for (var i = 0; i < args.length; i++) {
             callerArgs.push(args[i]);
           }

           cb.apply(window, callerArgs);
         }, 0);
       };
     } //The library will append the necessary launch info to each new A that is linked to the page.
     //NOTE: This cannot work if you programmatically change the window location. If you do, you must
     //execute the logic in setupCourseLinks to send the initialization data to the new location (if
     //you wish that new location to track as part of this session)


     function observeForNewLinks() {
       // select the target node
       var target = document.body; // create an observer instance

       var observer = new MutationObserver(function (mutations) {
         mutations.forEach(function (mutation) {
           for (var i in mutation.addedNodes) {
             if (mutation.addedNodes.hasOwnProperty(i)) {
               if (mutation.addedNodes[i].constructor == HTMLAnchorElement) {
                 var node = mutation.addedNodes[i];
                 setupCourseLinks([node]);
               }
             }
           }
         });
       }); // configuration of the observer:

       var config = {
         attributes: true,
         childList: true,
         subtree: true
       }; // pass in the target node, as well as the observer options

       observer.observe(target, config); // later, you can stop observing
       ///  observer.disconnect();
     } //This library will init all links in the DOM that include the attribute "courseLink = true"
     //with the information necessary for the document at that link to track as part of this session.


     function setupCourseLinks(_nodes) {
       var launchToken = getQueryVariable("xAPILaunchKey");
       var launchEndpoint = getQueryVariable("xAPILaunchService");
       var encrypted = getQueryVariable("encrypted");
       var query = "xAPILaunchKey=" + launchToken + "&xAPILaunchService=" + launchEndpoint;

       if (encrypted) {
         query += "&encrypted=true";
       }

       for (var i = 0; i < _nodes.length; i++) {
         var link = _nodes[i];
         var href = link.href;
         var courseLink = link.attributes.getNamedItem('courselink');

         if (courseLink && courseLink.value == "true") {
           if (href.indexOf("?") > -1) {
             href = href + "&" + query;
           } else href = href + "?" + query;

           link.href = href;
         }
       }
     }

     function xAPILaunch(cb, terminate_on_unload, strict_callbacks) {
       cb = cb_wrap(cb);

       try {
         var launchToken = getQueryVariable("xAPILaunchKey");
         var launchEndpoint = getQueryVariable("xAPILaunchService");
         var encrypted = getQueryVariable("encrypted");

         if (encrypted) {//here, we'd have to implement decryption for the data. This makes little sense in a client side only course
         }

         xAPILaunch.terminate = function (message) {
           var launch = new URL(launchEndpoint);
           launch.pathname += "launch/" + launchToken + "/terminate";
           var xhr2 = new XMLHttpRequest();
           xhr2.withCredentials = true;
           xhr2.crossDomain = true;
           xhr2.open('POST', launch.toString(), false);
           xhr2.setRequestHeader("Content-type", "application/json");
           xhr2.send(JSON.stringify({
             "code": 0,
             "description": message || "User closed content"
           }));
         };

         if (!launchToken || !launchEndpoint) return cb("invalid launch parameters");
         var launch = new URL(launchEndpoint);
         launch.pathname += "launch/" + launchToken;
         var xhr = new XMLHttpRequest();
         xhr.withCredentials = true;
         xhr.crossDomain = true;

         xhr.onerror = function (err) {
           //exit the try catch so inner execptions in the callback don't trigger this catch
           window.setTimeout(function () {
             return cb(err);
           });
         };

         xhr.onload = function (e) {
           if (xhr.status !== 200) {
             return xhr.onerror(xhr.responseText);
           }

           var body = JSON.parse(xhr.responseText);
           var launchData = body;
           var conf = {};
           conf['endpoint'] = launchData.endpoint;
           conf["actor"] = launchData.actor;
           conf.withCredentials = true;
           conf.strictCallbacks = strict_callbacks || false;

           window.onunload = function () {
             if (!terminate_on_unload) return;
             xAPILaunch.terminate("User closed content");
           };

           var wrapper = new ADL.XAPIWrapper.constructor();
           wrapper.changeConfig(conf); //Links that include "courseLink='true'"

           setupCourseLinks(document.body.querySelectorAll('a')); //Also, if links are added dynamically, we will do the same logic for those links.

           observeForNewLinks();
           return cb(null, body, wrapper);
         };

         xhr.open('POST', launch.toString(), true);
         xhr.send();
       } catch (e) {
         cb(e);
       }
     }
     ADL.launch = xAPILaunch;
   }

   function setup$3(obj) {
     var ADL = obj;
     var onBrowser = false;

     if (typeof window !== 'undefined') {
       ADL = window.ADL = obj.ADL || {};
       onBrowser = true;
     }

     var getObjDefName = function (o) {
       if (o.definition && o.definition.name) {
         return ADL.xapiutil.getLangVal(o.definition.name);
       }

       return undefined;
     };

     var getSubStatementDisplay = function (o) {
       if (o.objectType !== "SubStatement") return;
       if (o.object.objectType === "SubStatement") return;
       if (o.id || o.stored || o.version || o.authority) return;
       var disp = ADL.xapiutil.getActorId(o.actor) + ":" + ADL.xapiutil.getVerbDisplay(o.verb) + ":" + ADL.xapiutil.getObjectId(o.object);
       return disp;
     };

     ADL.xapiutil = {};

     ADL.xapiutil.getLang = function () {
       var lang;
       if (typeof navigator !== 'undefined') lang = navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage;else if (typeof process !== 'undefined' && typeof process.env !== 'undefined' && typeof process.env.LANG !== 'undefined') {
         var str = process.env.LANG;
         lang = str.slice(0, str.indexOf('.'));
         lang = lang.replace(/_/, '-');
       }
       return lang || "en-US";
     };

     ADL.xapiutil.getLangVal = function (langprop) {
       if (!langprop) return;
       var options = Object.keys(langprop); // test that langprop is a dict (obj)
       // skips if not a dict(obj) and returns

       if (options.length == 0) return undefined;
       var lang = ADL.xapiutil.getLang(),
           ret,
           dispGotten = false;

       do {
         //test and retest
         if (langprop[lang]) {
           ret = langprop[lang];
           dispGotten = true;
         } else if (lang.indexOf('-')) {
           lang = lang.substring(0, lang.lastIndexOf('-'));
         }
       } while (!dispGotten && lang !== "");

       return ret;
     };

     ADL.xapiutil.getMoreStatements = function (iterations, callback, searchParams) {
       if (!onBrowser) throw new Error("Node not supported.");
       var stmts = [];
       ADL.XAPIWrapper.getStatements(searchParams, null, function getMore(r) {
         if (!(r && r.response)) return;
         var res = JSON.parse(r.response);
         if (!res.statements) return;
         stmts = stmts.concat(res.statements);

         if (iterations-- <= 0) {
           callback(stmts);
         } else {
           if (res.more && res.more !== "") {
             ADL.XAPIWrapper.getStatements(searchParams, res.more, getMore);
           } else if (res.more === "") {
             callback(stmts);
           }
         }
       });
     };

     ADL.xapiutil.getActorId = function (a) {
       return a.mbox || a.openid || a.mbox_sha1sum || a.account;
     };

     ADL.xapiutil.getActorIdString = function (a) {
       var id = a.mbox || a.openid || a.mbox_sha1sum;

       if (!id) {
         if (a.account) id = a.account.homePage + ":" + a.account.name;else if (a.member) id = "Anon Group " + a.member;else id = 'unknown';
       }

       return id;
     };

     ADL.xapiutil.getActorDisplay = function (a) {
       return a.name || ADL.xapiutil.getActorIdString(a);
     };

     ADL.xapiutil.getVerbDisplay = function (v) {
       if (!v) return;

       if (v.display) {
         return ADL.xapiutil.getLangVal(v.display) || v.id;
       }

       return v.id;
     };

     ADL.xapiutil.getObjectType = function (o) {
       return o.objectType || (o.id ? "Activity" : "Agent");
     };

     ADL.xapiutil.getObjectId = function (o) {
       if (o.id) return o.id;
       var type = ADL.xapiutil.getObjectType(o);
       if (type === "Agent" || type === "Group") return ADL.xapiutil.getActorId(o);
       return undefined;
     };

     ADL.xapiutil.getObjectIdString = function (o) {
       if (!o) return 'unknown';
       if (o.id) return o.id;
       var type = ADL.xapiutil.getObjectType(o);
       if (type === "Agent" || type === "Group") return ADL.xapiutil.getActorIdString(o);else if (type == "SubStatement") {
         return getSubStatementDisplay(o);
       }
       return 'unknown';
     };

     ADL.xapiutil.getObjectDisplay = function (o) {
       if (!o) return "unknown";
       var disp = getObjDefName(o) || o.name || o.id;

       if (!disp) {
         var type = ADL.xapiutil.getObjectType(o);
         if (type === "Agent" || type == "Group") disp = ADL.xapiutil.getActorDisplay(o);else if (type == "SubStatement") {
           disp = getSubStatementDisplay(o);
         }
       }

       return disp;
     };
   }

   function setup$4(ADL) {
     function _getobj(obj, path) {
       var parts = path.split('.');
       var part = parts[0];
       path = parts.slice(1).join('.');

       if (!obj[part]) {
         if (/\[\]$/.test(part)) {
           part = part.slice(0, -2);

           if (!Array.isArray(obj[part])) {
             obj[part] = [];
           }
         } else obj[part] = {};
       }

       if (!path) return obj[part];else return _getobj(obj[part], path);
     }
     /*******************************************************************************
      * XAPIStatement - a convenience class to wrap statement objects
      *
      * This sub-API is supposed to make it easier to author valid xAPI statements
      * by adding constructors and encouraging best practices. All objects in this
      * API are fully JSON-compatible, so anything expecting an xAPI statement can
      * take an improved statement and vice versa.
      *
      * A working knowledge of what exactly the LRS expects is still expected,
      * but it's easier to map an 'I did this' statement to xAPI now.
      *
      * Tech note: All constructors also double as shallow clone functions. E.g.
      *
      *   var activity1 = new Activity('A walk in the park');
      *   var activity2 = new Activity(activity1);
      *   var activity3 = new Activity(stmt_from_lrs.object);
      *
      *******************************************************************************/

     /*
      * A convenient JSON-compatible xAPI statement wrapper
      * All args are optional, but the statement may not be complete or valid
      * Can also pass an Agent IFI, Verb ID, and an Activity ID in lieu of these args
      * @param {string} [actor]   The Agent or Group committing the action described by the statement
      * @param {string} [verb]   The Verb for the action described by the statement
      * @param {string} [object]   The receiver of the action. An Agent, Group, Activity, SubStatement, or StatementRef
      * @example
      * var stmt = new ADL.XAPIStatement(
      *     'mailto:steve.vergenz.ctr@adlnet.gov',
      *    'http://adlnet.gov/expapi/verbs/launched',
      *    'http://vwf.adlnet.gov/xapi/virtual_world_sandbox'
      * );
      * >> {
      * "actor": {
      *     "objectType": "Agent",
      *     "mbox": "mailto:steve.vergenz.ctr@adlnet.gov" },
      * "verb": {
      *     "id": "http://adlnet.gov/expapi/verbs/launched" },
      * "object": {
      *     "objectType": "Activity",
      *     "id": "http://vwf.adlnet.gov/xapi/virtual_world_sandbox" },
      * "result": {
      *      "An optional property that represents a measured outcome related to the Statement in which it is included."}}
      */


     var XAPIStatement = function (actor, verb, object, result) {
       // initialize
       // if first arg is an xapi statement, parse
       if (actor && actor.actor && actor.verb && actor.object) {
         var stmt = actor;

         for (var i in stmt) {
           if (i != 'actor' && i != 'verb' && i != 'object') this[i] = stmt[i];
         }

         actor = stmt.actor;
         verb = stmt.verb;
         object = stmt.object;
       }

       if (actor) {
         if (actor instanceof Agent) this.actor = actor;else if (actor.objectType === 'Agent' || !actor.objectType) this.actor = new Agent(actor);else if (actor.objectType === 'Group') this.actor = new Group(actor);
       } else this.actor = null;

       if (verb) {
         if (verb instanceof Verb) this.verb = verb;else this.verb = new Verb(verb);
       } else this.verb = null; // decide what kind of object was passed


       if (object) {
         if (object.objectType === 'Activity' || !object.objectType) {
           if (object instanceof Activity) this.object = object;else this.object = new Activity(object);
         } else if (object.objectType === 'Agent') {
           if (object instanceof Agent) this.object = object;else this.object = new Agent(object);
         } else if (object.objectType === 'Group') {
           if (object instanceof Group) this.object = object;else this.object = new Group(object);
         } else if (object.objectType === 'StatementRef') {
           if (object instanceof StatementRef) this.object = object;else this.object = new StatementRef(object);
         } else if (object.objectType === 'SubStatement') {
           if (object instanceof SubStatement) this.object = object;else this.object = new SubStatement(object);
         } else this.object = null;
       } else this.object = null; // add support for result object


       if (result) {
         this.result = result;
       }

       this.generateId = function () {
         this.id = ADL.ruuid();
       };
     };

     XAPIStatement.prototype.toString = function () {
       return this.actor.toString() + " " + this.verb.toString() + " " + this.object.toString() + " " + this.result.toString();
     };

     XAPIStatement.prototype.isValid = function () {
       return this.actor && this.actor.isValid() && this.verb && this.verb.isValid() && this.object && this.object.isValid() && this.result && this.result.isValid();
     };

     XAPIStatement.prototype.generateRegistration = function () {
       _getobj(this, 'context').registration = ADL.ruuid();
     };

     XAPIStatement.prototype.addParentActivity = function (activity) {
       _getobj(this, 'context.contextActivities.parent[]').push(new Activity(activity));
     };

     XAPIStatement.prototype.addGroupingActivity = function (activity) {
       _getobj(this, 'context.contextActivities.grouping[]').push(new Activity(activity));
     };

     XAPIStatement.prototype.addOtherContextActivity = function (activity) {
       _getobj(this, 'context.contextActivities.other[]').push(new Activity(activity));
     };
     /*
      * Provides an easy constructor for xAPI agent objects
      * @param {string} identifier   One of the Inverse Functional Identifiers specified in the spec.
      *     That is, an email, a hashed email, an OpenID, or an account object.
      *     See (https://github.com/adlnet/xAPI-Spec/blob/master/xAPI.md#inversefunctional)
      * @param {string} [name]   The natural-language name of the agent
      */


     var Agent = function (identifier, name) {
       this.objectType = 'Agent';
       this.name = name; // figure out what type of identifier was given

       if (identifier && (identifier.mbox || identifier.mbox_sha1sum || identifier.openid || identifier.account)) {
         for (var i in identifier) {
           this[i] = identifier[i];
         }
       } else if (/^mailto:/.test(identifier)) {
         this.mbox = identifier;
       } else if (/^[0-9a-f]{40}$/i.test(identifier)) {
         this.mbox_sha1sum = identifier;
       } else if (/^http[s]?:/.test(identifier)) {
         this.openid = identifier;
       } else if (identifier && identifier.homePage && identifier.name) {
         this.account = identifier;
       }
     };

     Agent.prototype.toString = function () {
       return this.name || this.mbox || this.openid || this.mbox_sha1sum || this.account.name;
     };

     Agent.prototype.isValid = function () {
       return this.mbox || this.mbox_sha1sum || this.openid || this.account.homePage && this.account.name || this.objectType === 'Group' && this.member;
     };
     /*
      * A type of agent, can contain multiple agents
      * @param {string} [identifier]   (optional if `members` specified) See Agent.
      * @param {string} [members]    An array of Agents describing the membership of the group
      * @param {string} [name]   The natural-language name of the agent
      */


     var Group = function (identifier, members, name) {
       Agent.call(this, identifier, name);
       this.member = members;
       this.objectType = 'Group';
     };

     Group.prototype = new Agent();
     /*
      * Really only provides a convenient language map
      * @param {string} id   The IRI of the action taken
      * @param {string} [description]    An English-language description, or a Language Map
      */

     var Verb = function (id, description) {
       // if passed a verb object then copy and return
       if (id && id.id) {
         for (var i in id) {
           this[i] = id[i];
         }

         return;
       } // save id and build language map


       this.id = id;

       if (description) {
         if (typeof description === 'string' || description instanceof String) {
           this.display = {
             'en-US': description
           };
         } else {
           this.display = description;
         }
       }
     };

     Verb.prototype.toString = function () {
       if (this.display && (this.display['en-US'] || this.display['en'])) return this.display['en-US'] || this.display['en'];else return this.id;
     };

     Verb.prototype.isValid = function () {
       return this.id;
     };
     /*
      * Describes an object that an agent interacts with
      * @param {string} id   The unique activity IRI
      * @param {string} name   An English-language identifier for the activity, or a Language Map
      * @param {string} description   An English-language description of the activity, or a Language Map
      */


     var Activity = function (id, name, description) {
       // if first arg is activity, copy everything over
       if (id && id.id) {
         var act = id;

         for (var i in act) {
           this[i] = act[i];
         }

         return;
       }

       this.objectType = 'Activity';
       this.id = id;

       if (name || description) {
         this.definition = {};
         if (typeof name === 'string' || name instanceof String) this.definition.name = {
           'en-US': name
         };else if (name) this.definition.name = name;
         if (typeof description === 'string' || description instanceof String) this.definition.description = {
           'en-US': description
         };else if (description) this.definition.description = description;
       }
     };

     Activity.prototype.toString = function () {
       if (this.definition && this.definition.name && (this.definition.name['en-US'] || this.definition.name['en'])) return this.definition.name['en-US'] || this.definition.name['en'];else return this.id;
     };

     Activity.prototype.isValid = function () {
       return this.id && (!this.objectType || this.objectType === 'Activity');
     };
     /*
      * An object that refers to a separate statement
      * @param {string} id   The UUID of another xAPI statement
      */


     var StatementRef = function (id) {
       if (id && id.id) {
         for (var i in id) {
           this[i] = id[i];
         }
       } else {
         this.objectType = 'StatementRef';
         this.id = id;
       }
     };

     StatementRef.prototype.toString = function () {
       return 'statement(' + this.id + ')';
     };

     StatementRef.prototype.isValid = function () {
       return this.id && this.objectType && this.objectType === 'StatementRef';
     };
     /*
      * A self-contained statement as the object of another statement
      * See XAPIStatement for constructor details
      * @param {string} actor   The Agent or Group committing the action described by the statement
      * @param {string} verb   The Verb for the action described by the statement
      * @param {string} object   The receiver of the action. An Agent, Group, Activity, or StatementRef
      */


     var SubStatement = function (actor, verb, object) {
       XAPIStatement.call(this, actor, verb, object);
       this.objectType = 'SubStatement';
       delete this.id;
       delete this.stored;
       delete this.version;
       delete this.authority;
     };

     SubStatement.prototype = new XAPIStatement();

     SubStatement.prototype.toString = function () {
       return '"' + SubStatement.prototype.prototype.toString.call(this) + '"';
     };

     XAPIStatement.Agent = Agent;
     XAPIStatement.Group = Group;
     XAPIStatement.Verb = Verb;
     XAPIStatement.Activity = Activity;
     XAPIStatement.StatementRef = StatementRef;
     XAPIStatement.SubStatement = SubStatement;
     ADL.XAPIStatement = XAPIStatement;
   }

   var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

   function createCommonjsModule(fn, module) {
   	return module = { exports: {} }, fn(module, module.exports), module.exports;
   }

   function commonjsRequire () {
   	throw new Error('Dynamic requires are not currently supported by @rollup/plugin-commonjs');
   }

   var core = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory();
   	}
   }(commonjsGlobal, function () {

   	/*globals window, global, require*/

   	/**
   	 * CryptoJS core components.
   	 */
   	var CryptoJS = CryptoJS || (function (Math, undefined$1) {

   	    var crypto$1;

   	    // Native crypto from window (Browser)
   	    if (typeof window !== 'undefined' && window.crypto) {
   	        crypto$1 = window.crypto;
   	    }

   	    // Native (experimental IE 11) crypto from window (Browser)
   	    if (!crypto$1 && typeof window !== 'undefined' && window.msCrypto) {
   	        crypto$1 = window.msCrypto;
   	    }

   	    // Native crypto from global (NodeJS)
   	    if (!crypto$1 && typeof commonjsGlobal !== 'undefined' && commonjsGlobal.crypto) {
   	        crypto$1 = commonjsGlobal.crypto;
   	    }

   	    // Native crypto import via require (NodeJS)
   	    if (!crypto$1 && typeof commonjsRequire === 'function') {
   	        try {
   	            crypto$1 = crypto;
   	        } catch (err) {}
   	    }

   	    /*
   	     * Cryptographically secure pseudorandom number generator
   	     *
   	     * As Math.random() is cryptographically not safe to use
   	     */
   	    var cryptoSecureRandomInt = function () {
   	        if (crypto$1) {
   	            // Use getRandomValues method (Browser)
   	            if (typeof crypto$1.getRandomValues === 'function') {
   	                try {
   	                    return crypto$1.getRandomValues(new Uint32Array(1))[0];
   	                } catch (err) {}
   	            }

   	            // Use randomBytes method (NodeJS)
   	            if (typeof crypto$1.randomBytes === 'function') {
   	                try {
   	                    return crypto$1.randomBytes(4).readInt32LE();
   	                } catch (err) {}
   	            }
   	        }

   	        throw new Error('Native crypto module could not be used to get secure random number.');
   	    };

   	    /*
   	     * Local polyfill of Object.create

   	     */
   	    var create = Object.create || (function () {
   	        function F() {}

   	        return function (obj) {
   	            var subtype;

   	            F.prototype = obj;

   	            subtype = new F();

   	            F.prototype = null;

   	            return subtype;
   	        };
   	    }());

   	    /**
   	     * CryptoJS namespace.
   	     */
   	    var C = {};

   	    /**
   	     * Library namespace.
   	     */
   	    var C_lib = C.lib = {};

   	    /**
   	     * Base object for prototypal inheritance.
   	     */
   	    var Base = C_lib.Base = (function () {


   	        return {
   	            /**
   	             * Creates a new object that inherits from this object.
   	             *
   	             * @param {Object} overrides Properties to copy into the new object.
   	             *
   	             * @return {Object} The new object.
   	             *
   	             * @static
   	             *
   	             * @example
   	             *
   	             *     var MyType = CryptoJS.lib.Base.extend({
   	             *         field: 'value',
   	             *
   	             *         method: function () {
   	             *         }
   	             *     });
   	             */
   	            extend: function (overrides) {
   	                // Spawn
   	                var subtype = create(this);

   	                // Augment
   	                if (overrides) {
   	                    subtype.mixIn(overrides);
   	                }

   	                // Create default initializer
   	                if (!subtype.hasOwnProperty('init') || this.init === subtype.init) {
   	                    subtype.init = function () {
   	                        subtype.$super.init.apply(this, arguments);
   	                    };
   	                }

   	                // Initializer's prototype is the subtype object
   	                subtype.init.prototype = subtype;

   	                // Reference supertype
   	                subtype.$super = this;

   	                return subtype;
   	            },

   	            /**
   	             * Extends this object and runs the init method.
   	             * Arguments to create() will be passed to init().
   	             *
   	             * @return {Object} The new object.
   	             *
   	             * @static
   	             *
   	             * @example
   	             *
   	             *     var instance = MyType.create();
   	             */
   	            create: function () {
   	                var instance = this.extend();
   	                instance.init.apply(instance, arguments);

   	                return instance;
   	            },

   	            /**
   	             * Initializes a newly created object.
   	             * Override this method to add some logic when your objects are created.
   	             *
   	             * @example
   	             *
   	             *     var MyType = CryptoJS.lib.Base.extend({
   	             *         init: function () {
   	             *             // ...
   	             *         }
   	             *     });
   	             */
   	            init: function () {
   	            },

   	            /**
   	             * Copies properties into this object.
   	             *
   	             * @param {Object} properties The properties to mix in.
   	             *
   	             * @example
   	             *
   	             *     MyType.mixIn({
   	             *         field: 'value'
   	             *     });
   	             */
   	            mixIn: function (properties) {
   	                for (var propertyName in properties) {
   	                    if (properties.hasOwnProperty(propertyName)) {
   	                        this[propertyName] = properties[propertyName];
   	                    }
   	                }

   	                // IE won't copy toString using the loop above
   	                if (properties.hasOwnProperty('toString')) {
   	                    this.toString = properties.toString;
   	                }
   	            },

   	            /**
   	             * Creates a copy of this object.
   	             *
   	             * @return {Object} The clone.
   	             *
   	             * @example
   	             *
   	             *     var clone = instance.clone();
   	             */
   	            clone: function () {
   	                return this.init.prototype.extend(this);
   	            }
   	        };
   	    }());

   	    /**
   	     * An array of 32-bit words.
   	     *
   	     * @property {Array} words The array of 32-bit words.
   	     * @property {number} sigBytes The number of significant bytes in this word array.
   	     */
   	    var WordArray = C_lib.WordArray = Base.extend({
   	        /**
   	         * Initializes a newly created word array.
   	         *
   	         * @param {Array} words (Optional) An array of 32-bit words.
   	         * @param {number} sigBytes (Optional) The number of significant bytes in the words.
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.lib.WordArray.create();
   	         *     var wordArray = CryptoJS.lib.WordArray.create([0x00010203, 0x04050607]);
   	         *     var wordArray = CryptoJS.lib.WordArray.create([0x00010203, 0x04050607], 6);
   	         */
   	        init: function (words, sigBytes) {
   	            words = this.words = words || [];

   	            if (sigBytes != undefined$1) {
   	                this.sigBytes = sigBytes;
   	            } else {
   	                this.sigBytes = words.length * 4;
   	            }
   	        },

   	        /**
   	         * Converts this word array to a string.
   	         *
   	         * @param {Encoder} encoder (Optional) The encoding strategy to use. Default: CryptoJS.enc.Hex
   	         *
   	         * @return {string} The stringified word array.
   	         *
   	         * @example
   	         *
   	         *     var string = wordArray + '';
   	         *     var string = wordArray.toString();
   	         *     var string = wordArray.toString(CryptoJS.enc.Utf8);
   	         */
   	        toString: function (encoder) {
   	            return (encoder || Hex).stringify(this);
   	        },

   	        /**
   	         * Concatenates a word array to this word array.
   	         *
   	         * @param {WordArray} wordArray The word array to append.
   	         *
   	         * @return {WordArray} This word array.
   	         *
   	         * @example
   	         *
   	         *     wordArray1.concat(wordArray2);
   	         */
   	        concat: function (wordArray) {
   	            // Shortcuts
   	            var thisWords = this.words;
   	            var thatWords = wordArray.words;
   	            var thisSigBytes = this.sigBytes;
   	            var thatSigBytes = wordArray.sigBytes;

   	            // Clamp excess bits
   	            this.clamp();

   	            // Concat
   	            if (thisSigBytes % 4) {
   	                // Copy one byte at a time
   	                for (var i = 0; i < thatSigBytes; i++) {
   	                    var thatByte = (thatWords[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
   	                    thisWords[(thisSigBytes + i) >>> 2] |= thatByte << (24 - ((thisSigBytes + i) % 4) * 8);
   	                }
   	            } else {
   	                // Copy one word at a time
   	                for (var i = 0; i < thatSigBytes; i += 4) {
   	                    thisWords[(thisSigBytes + i) >>> 2] = thatWords[i >>> 2];
   	                }
   	            }
   	            this.sigBytes += thatSigBytes;

   	            // Chainable
   	            return this;
   	        },

   	        /**
   	         * Removes insignificant bits.
   	         *
   	         * @example
   	         *
   	         *     wordArray.clamp();
   	         */
   	        clamp: function () {
   	            // Shortcuts
   	            var words = this.words;
   	            var sigBytes = this.sigBytes;

   	            // Clamp
   	            words[sigBytes >>> 2] &= 0xffffffff << (32 - (sigBytes % 4) * 8);
   	            words.length = Math.ceil(sigBytes / 4);
   	        },

   	        /**
   	         * Creates a copy of this word array.
   	         *
   	         * @return {WordArray} The clone.
   	         *
   	         * @example
   	         *
   	         *     var clone = wordArray.clone();
   	         */
   	        clone: function () {
   	            var clone = Base.clone.call(this);
   	            clone.words = this.words.slice(0);

   	            return clone;
   	        },

   	        /**
   	         * Creates a word array filled with random bytes.
   	         *
   	         * @param {number} nBytes The number of random bytes to generate.
   	         *
   	         * @return {WordArray} The random word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.lib.WordArray.random(16);
   	         */
   	        random: function (nBytes) {
   	            var words = [];

   	            for (var i = 0; i < nBytes; i += 4) {
   	                words.push(cryptoSecureRandomInt());
   	            }

   	            return new WordArray.init(words, nBytes);
   	        }
   	    });

   	    /**
   	     * Encoder namespace.
   	     */
   	    var C_enc = C.enc = {};

   	    /**
   	     * Hex encoding strategy.
   	     */
   	    var Hex = C_enc.Hex = {
   	        /**
   	         * Converts a word array to a hex string.
   	         *
   	         * @param {WordArray} wordArray The word array.
   	         *
   	         * @return {string} The hex string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var hexString = CryptoJS.enc.Hex.stringify(wordArray);
   	         */
   	        stringify: function (wordArray) {
   	            // Shortcuts
   	            var words = wordArray.words;
   	            var sigBytes = wordArray.sigBytes;

   	            // Convert
   	            var hexChars = [];
   	            for (var i = 0; i < sigBytes; i++) {
   	                var bite = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
   	                hexChars.push((bite >>> 4).toString(16));
   	                hexChars.push((bite & 0x0f).toString(16));
   	            }

   	            return hexChars.join('');
   	        },

   	        /**
   	         * Converts a hex string to a word array.
   	         *
   	         * @param {string} hexStr The hex string.
   	         *
   	         * @return {WordArray} The word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.enc.Hex.parse(hexString);
   	         */
   	        parse: function (hexStr) {
   	            // Shortcut
   	            var hexStrLength = hexStr.length;

   	            // Convert
   	            var words = [];
   	            for (var i = 0; i < hexStrLength; i += 2) {
   	                words[i >>> 3] |= parseInt(hexStr.substr(i, 2), 16) << (24 - (i % 8) * 4);
   	            }

   	            return new WordArray.init(words, hexStrLength / 2);
   	        }
   	    };

   	    /**
   	     * Latin1 encoding strategy.
   	     */
   	    var Latin1 = C_enc.Latin1 = {
   	        /**
   	         * Converts a word array to a Latin1 string.
   	         *
   	         * @param {WordArray} wordArray The word array.
   	         *
   	         * @return {string} The Latin1 string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var latin1String = CryptoJS.enc.Latin1.stringify(wordArray);
   	         */
   	        stringify: function (wordArray) {
   	            // Shortcuts
   	            var words = wordArray.words;
   	            var sigBytes = wordArray.sigBytes;

   	            // Convert
   	            var latin1Chars = [];
   	            for (var i = 0; i < sigBytes; i++) {
   	                var bite = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
   	                latin1Chars.push(String.fromCharCode(bite));
   	            }

   	            return latin1Chars.join('');
   	        },

   	        /**
   	         * Converts a Latin1 string to a word array.
   	         *
   	         * @param {string} latin1Str The Latin1 string.
   	         *
   	         * @return {WordArray} The word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.enc.Latin1.parse(latin1String);
   	         */
   	        parse: function (latin1Str) {
   	            // Shortcut
   	            var latin1StrLength = latin1Str.length;

   	            // Convert
   	            var words = [];
   	            for (var i = 0; i < latin1StrLength; i++) {
   	                words[i >>> 2] |= (latin1Str.charCodeAt(i) & 0xff) << (24 - (i % 4) * 8);
   	            }

   	            return new WordArray.init(words, latin1StrLength);
   	        }
   	    };

   	    /**
   	     * UTF-8 encoding strategy.
   	     */
   	    var Utf8 = C_enc.Utf8 = {
   	        /**
   	         * Converts a word array to a UTF-8 string.
   	         *
   	         * @param {WordArray} wordArray The word array.
   	         *
   	         * @return {string} The UTF-8 string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var utf8String = CryptoJS.enc.Utf8.stringify(wordArray);
   	         */
   	        stringify: function (wordArray) {
   	            try {
   	                return decodeURIComponent(escape(Latin1.stringify(wordArray)));
   	            } catch (e) {
   	                throw new Error('Malformed UTF-8 data');
   	            }
   	        },

   	        /**
   	         * Converts a UTF-8 string to a word array.
   	         *
   	         * @param {string} utf8Str The UTF-8 string.
   	         *
   	         * @return {WordArray} The word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.enc.Utf8.parse(utf8String);
   	         */
   	        parse: function (utf8Str) {
   	            return Latin1.parse(unescape(encodeURIComponent(utf8Str)));
   	        }
   	    };

   	    /**
   	     * Abstract buffered block algorithm template.
   	     *
   	     * The property blockSize must be implemented in a concrete subtype.
   	     *
   	     * @property {number} _minBufferSize The number of blocks that should be kept unprocessed in the buffer. Default: 0
   	     */
   	    var BufferedBlockAlgorithm = C_lib.BufferedBlockAlgorithm = Base.extend({
   	        /**
   	         * Resets this block algorithm's data buffer to its initial state.
   	         *
   	         * @example
   	         *
   	         *     bufferedBlockAlgorithm.reset();
   	         */
   	        reset: function () {
   	            // Initial values
   	            this._data = new WordArray.init();
   	            this._nDataBytes = 0;
   	        },

   	        /**
   	         * Adds new data to this block algorithm's buffer.
   	         *
   	         * @param {WordArray|string} data The data to append. Strings are converted to a WordArray using UTF-8.
   	         *
   	         * @example
   	         *
   	         *     bufferedBlockAlgorithm._append('data');
   	         *     bufferedBlockAlgorithm._append(wordArray);
   	         */
   	        _append: function (data) {
   	            // Convert string to WordArray, else assume WordArray already
   	            if (typeof data == 'string') {
   	                data = Utf8.parse(data);
   	            }

   	            // Append
   	            this._data.concat(data);
   	            this._nDataBytes += data.sigBytes;
   	        },

   	        /**
   	         * Processes available data blocks.
   	         *
   	         * This method invokes _doProcessBlock(offset), which must be implemented by a concrete subtype.
   	         *
   	         * @param {boolean} doFlush Whether all blocks and partial blocks should be processed.
   	         *
   	         * @return {WordArray} The processed data.
   	         *
   	         * @example
   	         *
   	         *     var processedData = bufferedBlockAlgorithm._process();
   	         *     var processedData = bufferedBlockAlgorithm._process(!!'flush');
   	         */
   	        _process: function (doFlush) {
   	            var processedWords;

   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;
   	            var dataSigBytes = data.sigBytes;
   	            var blockSize = this.blockSize;
   	            var blockSizeBytes = blockSize * 4;

   	            // Count blocks ready
   	            var nBlocksReady = dataSigBytes / blockSizeBytes;
   	            if (doFlush) {
   	                // Round up to include partial blocks
   	                nBlocksReady = Math.ceil(nBlocksReady);
   	            } else {
   	                // Round down to include only full blocks,
   	                // less the number of blocks that must remain in the buffer
   	                nBlocksReady = Math.max((nBlocksReady | 0) - this._minBufferSize, 0);
   	            }

   	            // Count words ready
   	            var nWordsReady = nBlocksReady * blockSize;

   	            // Count bytes ready
   	            var nBytesReady = Math.min(nWordsReady * 4, dataSigBytes);

   	            // Process blocks
   	            if (nWordsReady) {
   	                for (var offset = 0; offset < nWordsReady; offset += blockSize) {
   	                    // Perform concrete-algorithm logic
   	                    this._doProcessBlock(dataWords, offset);
   	                }

   	                // Remove processed words
   	                processedWords = dataWords.splice(0, nWordsReady);
   	                data.sigBytes -= nBytesReady;
   	            }

   	            // Return processed words
   	            return new WordArray.init(processedWords, nBytesReady);
   	        },

   	        /**
   	         * Creates a copy of this object.
   	         *
   	         * @return {Object} The clone.
   	         *
   	         * @example
   	         *
   	         *     var clone = bufferedBlockAlgorithm.clone();
   	         */
   	        clone: function () {
   	            var clone = Base.clone.call(this);
   	            clone._data = this._data.clone();

   	            return clone;
   	        },

   	        _minBufferSize: 0
   	    });

   	    /**
   	     * Abstract hasher template.
   	     *
   	     * @property {number} blockSize The number of 32-bit words this hasher operates on. Default: 16 (512 bits)
   	     */
   	    var Hasher = C_lib.Hasher = BufferedBlockAlgorithm.extend({
   	        /**
   	         * Configuration options.
   	         */
   	        cfg: Base.extend(),

   	        /**
   	         * Initializes a newly created hasher.
   	         *
   	         * @param {Object} cfg (Optional) The configuration options to use for this hash computation.
   	         *
   	         * @example
   	         *
   	         *     var hasher = CryptoJS.algo.SHA256.create();
   	         */
   	        init: function (cfg) {
   	            // Apply config defaults
   	            this.cfg = this.cfg.extend(cfg);

   	            // Set initial values
   	            this.reset();
   	        },

   	        /**
   	         * Resets this hasher to its initial state.
   	         *
   	         * @example
   	         *
   	         *     hasher.reset();
   	         */
   	        reset: function () {
   	            // Reset data buffer
   	            BufferedBlockAlgorithm.reset.call(this);

   	            // Perform concrete-hasher logic
   	            this._doReset();
   	        },

   	        /**
   	         * Updates this hasher with a message.
   	         *
   	         * @param {WordArray|string} messageUpdate The message to append.
   	         *
   	         * @return {Hasher} This hasher.
   	         *
   	         * @example
   	         *
   	         *     hasher.update('message');
   	         *     hasher.update(wordArray);
   	         */
   	        update: function (messageUpdate) {
   	            // Append
   	            this._append(messageUpdate);

   	            // Update the hash
   	            this._process();

   	            // Chainable
   	            return this;
   	        },

   	        /**
   	         * Finalizes the hash computation.
   	         * Note that the finalize operation is effectively a destructive, read-once operation.
   	         *
   	         * @param {WordArray|string} messageUpdate (Optional) A final message update.
   	         *
   	         * @return {WordArray} The hash.
   	         *
   	         * @example
   	         *
   	         *     var hash = hasher.finalize();
   	         *     var hash = hasher.finalize('message');
   	         *     var hash = hasher.finalize(wordArray);
   	         */
   	        finalize: function (messageUpdate) {
   	            // Final message update
   	            if (messageUpdate) {
   	                this._append(messageUpdate);
   	            }

   	            // Perform concrete-hasher logic
   	            var hash = this._doFinalize();

   	            return hash;
   	        },

   	        blockSize: 512/32,

   	        /**
   	         * Creates a shortcut function to a hasher's object interface.
   	         *
   	         * @param {Hasher} hasher The hasher to create a helper for.
   	         *
   	         * @return {Function} The shortcut function.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var SHA256 = CryptoJS.lib.Hasher._createHelper(CryptoJS.algo.SHA256);
   	         */
   	        _createHelper: function (hasher) {
   	            return function (message, cfg) {
   	                return new hasher.init(cfg).finalize(message);
   	            };
   	        },

   	        /**
   	         * Creates a shortcut function to the HMAC's object interface.
   	         *
   	         * @param {Hasher} hasher The hasher to use in this HMAC helper.
   	         *
   	         * @return {Function} The shortcut function.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var HmacSHA256 = CryptoJS.lib.Hasher._createHmacHelper(CryptoJS.algo.SHA256);
   	         */
   	        _createHmacHelper: function (hasher) {
   	            return function (message, key) {
   	                return new C_algo.HMAC.init(hasher, key).finalize(message);
   	            };
   	        }
   	    });

   	    /**
   	     * Algorithm namespace.
   	     */
   	    var C_algo = C.algo = {};

   	    return C;
   	}(Math));


   	return CryptoJS;

   }));
   });

   var x64Core = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function (undefined$1) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var Base = C_lib.Base;
   	    var X32WordArray = C_lib.WordArray;

   	    /**
   	     * x64 namespace.
   	     */
   	    var C_x64 = C.x64 = {};

   	    /**
   	     * A 64-bit word.
   	     */
   	    var X64Word = C_x64.Word = Base.extend({
   	        /**
   	         * Initializes a newly created 64-bit word.
   	         *
   	         * @param {number} high The high 32 bits.
   	         * @param {number} low The low 32 bits.
   	         *
   	         * @example
   	         *
   	         *     var x64Word = CryptoJS.x64.Word.create(0x00010203, 0x04050607);
   	         */
   	        init: function (high, low) {
   	            this.high = high;
   	            this.low = low;
   	        }

   	        /**
   	         * Bitwise NOTs this word.
   	         *
   	         * @return {X64Word} A new x64-Word object after negating.
   	         *
   	         * @example
   	         *
   	         *     var negated = x64Word.not();
   	         */
   	        // not: function () {
   	            // var high = ~this.high;
   	            // var low = ~this.low;

   	            // return X64Word.create(high, low);
   	        // },

   	        /**
   	         * Bitwise ANDs this word with the passed word.
   	         *
   	         * @param {X64Word} word The x64-Word to AND with this word.
   	         *
   	         * @return {X64Word} A new x64-Word object after ANDing.
   	         *
   	         * @example
   	         *
   	         *     var anded = x64Word.and(anotherX64Word);
   	         */
   	        // and: function (word) {
   	            // var high = this.high & word.high;
   	            // var low = this.low & word.low;

   	            // return X64Word.create(high, low);
   	        // },

   	        /**
   	         * Bitwise ORs this word with the passed word.
   	         *
   	         * @param {X64Word} word The x64-Word to OR with this word.
   	         *
   	         * @return {X64Word} A new x64-Word object after ORing.
   	         *
   	         * @example
   	         *
   	         *     var ored = x64Word.or(anotherX64Word);
   	         */
   	        // or: function (word) {
   	            // var high = this.high | word.high;
   	            // var low = this.low | word.low;

   	            // return X64Word.create(high, low);
   	        // },

   	        /**
   	         * Bitwise XORs this word with the passed word.
   	         *
   	         * @param {X64Word} word The x64-Word to XOR with this word.
   	         *
   	         * @return {X64Word} A new x64-Word object after XORing.
   	         *
   	         * @example
   	         *
   	         *     var xored = x64Word.xor(anotherX64Word);
   	         */
   	        // xor: function (word) {
   	            // var high = this.high ^ word.high;
   	            // var low = this.low ^ word.low;

   	            // return X64Word.create(high, low);
   	        // },

   	        /**
   	         * Shifts this word n bits to the left.
   	         *
   	         * @param {number} n The number of bits to shift.
   	         *
   	         * @return {X64Word} A new x64-Word object after shifting.
   	         *
   	         * @example
   	         *
   	         *     var shifted = x64Word.shiftL(25);
   	         */
   	        // shiftL: function (n) {
   	            // if (n < 32) {
   	                // var high = (this.high << n) | (this.low >>> (32 - n));
   	                // var low = this.low << n;
   	            // } else {
   	                // var high = this.low << (n - 32);
   	                // var low = 0;
   	            // }

   	            // return X64Word.create(high, low);
   	        // },

   	        /**
   	         * Shifts this word n bits to the right.
   	         *
   	         * @param {number} n The number of bits to shift.
   	         *
   	         * @return {X64Word} A new x64-Word object after shifting.
   	         *
   	         * @example
   	         *
   	         *     var shifted = x64Word.shiftR(7);
   	         */
   	        // shiftR: function (n) {
   	            // if (n < 32) {
   	                // var low = (this.low >>> n) | (this.high << (32 - n));
   	                // var high = this.high >>> n;
   	            // } else {
   	                // var low = this.high >>> (n - 32);
   	                // var high = 0;
   	            // }

   	            // return X64Word.create(high, low);
   	        // },

   	        /**
   	         * Rotates this word n bits to the left.
   	         *
   	         * @param {number} n The number of bits to rotate.
   	         *
   	         * @return {X64Word} A new x64-Word object after rotating.
   	         *
   	         * @example
   	         *
   	         *     var rotated = x64Word.rotL(25);
   	         */
   	        // rotL: function (n) {
   	            // return this.shiftL(n).or(this.shiftR(64 - n));
   	        // },

   	        /**
   	         * Rotates this word n bits to the right.
   	         *
   	         * @param {number} n The number of bits to rotate.
   	         *
   	         * @return {X64Word} A new x64-Word object after rotating.
   	         *
   	         * @example
   	         *
   	         *     var rotated = x64Word.rotR(7);
   	         */
   	        // rotR: function (n) {
   	            // return this.shiftR(n).or(this.shiftL(64 - n));
   	        // },

   	        /**
   	         * Adds this word with the passed word.
   	         *
   	         * @param {X64Word} word The x64-Word to add with this word.
   	         *
   	         * @return {X64Word} A new x64-Word object after adding.
   	         *
   	         * @example
   	         *
   	         *     var added = x64Word.add(anotherX64Word);
   	         */
   	        // add: function (word) {
   	            // var low = (this.low + word.low) | 0;
   	            // var carry = (low >>> 0) < (this.low >>> 0) ? 1 : 0;
   	            // var high = (this.high + word.high + carry) | 0;

   	            // return X64Word.create(high, low);
   	        // }
   	    });

   	    /**
   	     * An array of 64-bit words.
   	     *
   	     * @property {Array} words The array of CryptoJS.x64.Word objects.
   	     * @property {number} sigBytes The number of significant bytes in this word array.
   	     */
   	    var X64WordArray = C_x64.WordArray = Base.extend({
   	        /**
   	         * Initializes a newly created word array.
   	         *
   	         * @param {Array} words (Optional) An array of CryptoJS.x64.Word objects.
   	         * @param {number} sigBytes (Optional) The number of significant bytes in the words.
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.x64.WordArray.create();
   	         *
   	         *     var wordArray = CryptoJS.x64.WordArray.create([
   	         *         CryptoJS.x64.Word.create(0x00010203, 0x04050607),
   	         *         CryptoJS.x64.Word.create(0x18191a1b, 0x1c1d1e1f)
   	         *     ]);
   	         *
   	         *     var wordArray = CryptoJS.x64.WordArray.create([
   	         *         CryptoJS.x64.Word.create(0x00010203, 0x04050607),
   	         *         CryptoJS.x64.Word.create(0x18191a1b, 0x1c1d1e1f)
   	         *     ], 10);
   	         */
   	        init: function (words, sigBytes) {
   	            words = this.words = words || [];

   	            if (sigBytes != undefined$1) {
   	                this.sigBytes = sigBytes;
   	            } else {
   	                this.sigBytes = words.length * 8;
   	            }
   	        },

   	        /**
   	         * Converts this 64-bit word array to a 32-bit word array.
   	         *
   	         * @return {CryptoJS.lib.WordArray} This word array's data as a 32-bit word array.
   	         *
   	         * @example
   	         *
   	         *     var x32WordArray = x64WordArray.toX32();
   	         */
   	        toX32: function () {
   	            // Shortcuts
   	            var x64Words = this.words;
   	            var x64WordsLength = x64Words.length;

   	            // Convert
   	            var x32Words = [];
   	            for (var i = 0; i < x64WordsLength; i++) {
   	                var x64Word = x64Words[i];
   	                x32Words.push(x64Word.high);
   	                x32Words.push(x64Word.low);
   	            }

   	            return X32WordArray.create(x32Words, this.sigBytes);
   	        },

   	        /**
   	         * Creates a copy of this word array.
   	         *
   	         * @return {X64WordArray} The clone.
   	         *
   	         * @example
   	         *
   	         *     var clone = x64WordArray.clone();
   	         */
   	        clone: function () {
   	            var clone = Base.clone.call(this);

   	            // Clone "words" array
   	            var words = clone.words = this.words.slice(0);

   	            // Clone each X64Word object
   	            var wordsLength = words.length;
   	            for (var i = 0; i < wordsLength; i++) {
   	                words[i] = words[i].clone();
   	            }

   	            return clone;
   	        }
   	    });
   	}());


   	return CryptoJS;

   }));
   });

   var libTypedarrays = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Check if typed arrays are supported
   	    if (typeof ArrayBuffer != 'function') {
   	        return;
   	    }

   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;

   	    // Reference original init
   	    var superInit = WordArray.init;

   	    // Augment WordArray.init to handle typed arrays
   	    var subInit = WordArray.init = function (typedArray) {
   	        // Convert buffers to uint8
   	        if (typedArray instanceof ArrayBuffer) {
   	            typedArray = new Uint8Array(typedArray);
   	        }

   	        // Convert other array views to uint8
   	        if (
   	            typedArray instanceof Int8Array ||
   	            (typeof Uint8ClampedArray !== "undefined" && typedArray instanceof Uint8ClampedArray) ||
   	            typedArray instanceof Int16Array ||
   	            typedArray instanceof Uint16Array ||
   	            typedArray instanceof Int32Array ||
   	            typedArray instanceof Uint32Array ||
   	            typedArray instanceof Float32Array ||
   	            typedArray instanceof Float64Array
   	        ) {
   	            typedArray = new Uint8Array(typedArray.buffer, typedArray.byteOffset, typedArray.byteLength);
   	        }

   	        // Handle Uint8Array
   	        if (typedArray instanceof Uint8Array) {
   	            // Shortcut
   	            var typedArrayByteLength = typedArray.byteLength;

   	            // Extract bytes
   	            var words = [];
   	            for (var i = 0; i < typedArrayByteLength; i++) {
   	                words[i >>> 2] |= typedArray[i] << (24 - (i % 4) * 8);
   	            }

   	            // Initialize this word array
   	            superInit.call(this, words, typedArrayByteLength);
   	        } else {
   	            // Else call normal init
   	            superInit.apply(this, arguments);
   	        }
   	    };

   	    subInit.prototype = WordArray;
   	}());


   	return CryptoJS.lib.WordArray;

   }));
   });

   var encUtf16 = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var C_enc = C.enc;

   	    /**
   	     * UTF-16 BE encoding strategy.
   	     */
   	    var Utf16BE = C_enc.Utf16 = C_enc.Utf16BE = {
   	        /**
   	         * Converts a word array to a UTF-16 BE string.
   	         *
   	         * @param {WordArray} wordArray The word array.
   	         *
   	         * @return {string} The UTF-16 BE string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var utf16String = CryptoJS.enc.Utf16.stringify(wordArray);
   	         */
   	        stringify: function (wordArray) {
   	            // Shortcuts
   	            var words = wordArray.words;
   	            var sigBytes = wordArray.sigBytes;

   	            // Convert
   	            var utf16Chars = [];
   	            for (var i = 0; i < sigBytes; i += 2) {
   	                var codePoint = (words[i >>> 2] >>> (16 - (i % 4) * 8)) & 0xffff;
   	                utf16Chars.push(String.fromCharCode(codePoint));
   	            }

   	            return utf16Chars.join('');
   	        },

   	        /**
   	         * Converts a UTF-16 BE string to a word array.
   	         *
   	         * @param {string} utf16Str The UTF-16 BE string.
   	         *
   	         * @return {WordArray} The word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.enc.Utf16.parse(utf16String);
   	         */
   	        parse: function (utf16Str) {
   	            // Shortcut
   	            var utf16StrLength = utf16Str.length;

   	            // Convert
   	            var words = [];
   	            for (var i = 0; i < utf16StrLength; i++) {
   	                words[i >>> 1] |= utf16Str.charCodeAt(i) << (16 - (i % 2) * 16);
   	            }

   	            return WordArray.create(words, utf16StrLength * 2);
   	        }
   	    };

   	    /**
   	     * UTF-16 LE encoding strategy.
   	     */
   	    C_enc.Utf16LE = {
   	        /**
   	         * Converts a word array to a UTF-16 LE string.
   	         *
   	         * @param {WordArray} wordArray The word array.
   	         *
   	         * @return {string} The UTF-16 LE string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var utf16Str = CryptoJS.enc.Utf16LE.stringify(wordArray);
   	         */
   	        stringify: function (wordArray) {
   	            // Shortcuts
   	            var words = wordArray.words;
   	            var sigBytes = wordArray.sigBytes;

   	            // Convert
   	            var utf16Chars = [];
   	            for (var i = 0; i < sigBytes; i += 2) {
   	                var codePoint = swapEndian((words[i >>> 2] >>> (16 - (i % 4) * 8)) & 0xffff);
   	                utf16Chars.push(String.fromCharCode(codePoint));
   	            }

   	            return utf16Chars.join('');
   	        },

   	        /**
   	         * Converts a UTF-16 LE string to a word array.
   	         *
   	         * @param {string} utf16Str The UTF-16 LE string.
   	         *
   	         * @return {WordArray} The word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.enc.Utf16LE.parse(utf16Str);
   	         */
   	        parse: function (utf16Str) {
   	            // Shortcut
   	            var utf16StrLength = utf16Str.length;

   	            // Convert
   	            var words = [];
   	            for (var i = 0; i < utf16StrLength; i++) {
   	                words[i >>> 1] |= swapEndian(utf16Str.charCodeAt(i) << (16 - (i % 2) * 16));
   	            }

   	            return WordArray.create(words, utf16StrLength * 2);
   	        }
   	    };

   	    function swapEndian(word) {
   	        return ((word << 8) & 0xff00ff00) | ((word >>> 8) & 0x00ff00ff);
   	    }
   	}());


   	return CryptoJS.enc.Utf16;

   }));
   });

   var encBase64 = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var C_enc = C.enc;

   	    /**
   	     * Base64 encoding strategy.
   	     */
   	    var Base64 = C_enc.Base64 = {
   	        /**
   	         * Converts a word array to a Base64 string.
   	         *
   	         * @param {WordArray} wordArray The word array.
   	         *
   	         * @return {string} The Base64 string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var base64String = CryptoJS.enc.Base64.stringify(wordArray);
   	         */
   	        stringify: function (wordArray) {
   	            // Shortcuts
   	            var words = wordArray.words;
   	            var sigBytes = wordArray.sigBytes;
   	            var map = this._map;

   	            // Clamp excess bits
   	            wordArray.clamp();

   	            // Convert
   	            var base64Chars = [];
   	            for (var i = 0; i < sigBytes; i += 3) {
   	                var byte1 = (words[i >>> 2]       >>> (24 - (i % 4) * 8))       & 0xff;
   	                var byte2 = (words[(i + 1) >>> 2] >>> (24 - ((i + 1) % 4) * 8)) & 0xff;
   	                var byte3 = (words[(i + 2) >>> 2] >>> (24 - ((i + 2) % 4) * 8)) & 0xff;

   	                var triplet = (byte1 << 16) | (byte2 << 8) | byte3;

   	                for (var j = 0; (j < 4) && (i + j * 0.75 < sigBytes); j++) {
   	                    base64Chars.push(map.charAt((triplet >>> (6 * (3 - j))) & 0x3f));
   	                }
   	            }

   	            // Add padding
   	            var paddingChar = map.charAt(64);
   	            if (paddingChar) {
   	                while (base64Chars.length % 4) {
   	                    base64Chars.push(paddingChar);
   	                }
   	            }

   	            return base64Chars.join('');
   	        },

   	        /**
   	         * Converts a Base64 string to a word array.
   	         *
   	         * @param {string} base64Str The Base64 string.
   	         *
   	         * @return {WordArray} The word array.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var wordArray = CryptoJS.enc.Base64.parse(base64String);
   	         */
   	        parse: function (base64Str) {
   	            // Shortcuts
   	            var base64StrLength = base64Str.length;
   	            var map = this._map;
   	            var reverseMap = this._reverseMap;

   	            if (!reverseMap) {
   	                    reverseMap = this._reverseMap = [];
   	                    for (var j = 0; j < map.length; j++) {
   	                        reverseMap[map.charCodeAt(j)] = j;
   	                    }
   	            }

   	            // Ignore padding
   	            var paddingChar = map.charAt(64);
   	            if (paddingChar) {
   	                var paddingIndex = base64Str.indexOf(paddingChar);
   	                if (paddingIndex !== -1) {
   	                    base64StrLength = paddingIndex;
   	                }
   	            }

   	            // Convert
   	            return parseLoop(base64Str, base64StrLength, reverseMap);

   	        },

   	        _map: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
   	    };

   	    function parseLoop(base64Str, base64StrLength, reverseMap) {
   	      var words = [];
   	      var nBytes = 0;
   	      for (var i = 0; i < base64StrLength; i++) {
   	          if (i % 4) {
   	              var bits1 = reverseMap[base64Str.charCodeAt(i - 1)] << ((i % 4) * 2);
   	              var bits2 = reverseMap[base64Str.charCodeAt(i)] >>> (6 - (i % 4) * 2);
   	              var bitsCombined = bits1 | bits2;
   	              words[nBytes >>> 2] |= bitsCombined << (24 - (nBytes % 4) * 8);
   	              nBytes++;
   	          }
   	      }
   	      return WordArray.create(words, nBytes);
   	    }
   	}());


   	return CryptoJS.enc.Base64;

   }));
   });

   var md5 = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function (Math) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var Hasher = C_lib.Hasher;
   	    var C_algo = C.algo;

   	    // Constants table
   	    var T = [];

   	    // Compute constants
   	    (function () {
   	        for (var i = 0; i < 64; i++) {
   	            T[i] = (Math.abs(Math.sin(i + 1)) * 0x100000000) | 0;
   	        }
   	    }());

   	    /**
   	     * MD5 hash algorithm.
   	     */
   	    var MD5 = C_algo.MD5 = Hasher.extend({
   	        _doReset: function () {
   	            this._hash = new WordArray.init([
   	                0x67452301, 0xefcdab89,
   	                0x98badcfe, 0x10325476
   	            ]);
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Swap endian
   	            for (var i = 0; i < 16; i++) {
   	                // Shortcuts
   	                var offset_i = offset + i;
   	                var M_offset_i = M[offset_i];

   	                M[offset_i] = (
   	                    (((M_offset_i << 8)  | (M_offset_i >>> 24)) & 0x00ff00ff) |
   	                    (((M_offset_i << 24) | (M_offset_i >>> 8))  & 0xff00ff00)
   	                );
   	            }

   	            // Shortcuts
   	            var H = this._hash.words;

   	            var M_offset_0  = M[offset + 0];
   	            var M_offset_1  = M[offset + 1];
   	            var M_offset_2  = M[offset + 2];
   	            var M_offset_3  = M[offset + 3];
   	            var M_offset_4  = M[offset + 4];
   	            var M_offset_5  = M[offset + 5];
   	            var M_offset_6  = M[offset + 6];
   	            var M_offset_7  = M[offset + 7];
   	            var M_offset_8  = M[offset + 8];
   	            var M_offset_9  = M[offset + 9];
   	            var M_offset_10 = M[offset + 10];
   	            var M_offset_11 = M[offset + 11];
   	            var M_offset_12 = M[offset + 12];
   	            var M_offset_13 = M[offset + 13];
   	            var M_offset_14 = M[offset + 14];
   	            var M_offset_15 = M[offset + 15];

   	            // Working varialbes
   	            var a = H[0];
   	            var b = H[1];
   	            var c = H[2];
   	            var d = H[3];

   	            // Computation
   	            a = FF(a, b, c, d, M_offset_0,  7,  T[0]);
   	            d = FF(d, a, b, c, M_offset_1,  12, T[1]);
   	            c = FF(c, d, a, b, M_offset_2,  17, T[2]);
   	            b = FF(b, c, d, a, M_offset_3,  22, T[3]);
   	            a = FF(a, b, c, d, M_offset_4,  7,  T[4]);
   	            d = FF(d, a, b, c, M_offset_5,  12, T[5]);
   	            c = FF(c, d, a, b, M_offset_6,  17, T[6]);
   	            b = FF(b, c, d, a, M_offset_7,  22, T[7]);
   	            a = FF(a, b, c, d, M_offset_8,  7,  T[8]);
   	            d = FF(d, a, b, c, M_offset_9,  12, T[9]);
   	            c = FF(c, d, a, b, M_offset_10, 17, T[10]);
   	            b = FF(b, c, d, a, M_offset_11, 22, T[11]);
   	            a = FF(a, b, c, d, M_offset_12, 7,  T[12]);
   	            d = FF(d, a, b, c, M_offset_13, 12, T[13]);
   	            c = FF(c, d, a, b, M_offset_14, 17, T[14]);
   	            b = FF(b, c, d, a, M_offset_15, 22, T[15]);

   	            a = GG(a, b, c, d, M_offset_1,  5,  T[16]);
   	            d = GG(d, a, b, c, M_offset_6,  9,  T[17]);
   	            c = GG(c, d, a, b, M_offset_11, 14, T[18]);
   	            b = GG(b, c, d, a, M_offset_0,  20, T[19]);
   	            a = GG(a, b, c, d, M_offset_5,  5,  T[20]);
   	            d = GG(d, a, b, c, M_offset_10, 9,  T[21]);
   	            c = GG(c, d, a, b, M_offset_15, 14, T[22]);
   	            b = GG(b, c, d, a, M_offset_4,  20, T[23]);
   	            a = GG(a, b, c, d, M_offset_9,  5,  T[24]);
   	            d = GG(d, a, b, c, M_offset_14, 9,  T[25]);
   	            c = GG(c, d, a, b, M_offset_3,  14, T[26]);
   	            b = GG(b, c, d, a, M_offset_8,  20, T[27]);
   	            a = GG(a, b, c, d, M_offset_13, 5,  T[28]);
   	            d = GG(d, a, b, c, M_offset_2,  9,  T[29]);
   	            c = GG(c, d, a, b, M_offset_7,  14, T[30]);
   	            b = GG(b, c, d, a, M_offset_12, 20, T[31]);

   	            a = HH(a, b, c, d, M_offset_5,  4,  T[32]);
   	            d = HH(d, a, b, c, M_offset_8,  11, T[33]);
   	            c = HH(c, d, a, b, M_offset_11, 16, T[34]);
   	            b = HH(b, c, d, a, M_offset_14, 23, T[35]);
   	            a = HH(a, b, c, d, M_offset_1,  4,  T[36]);
   	            d = HH(d, a, b, c, M_offset_4,  11, T[37]);
   	            c = HH(c, d, a, b, M_offset_7,  16, T[38]);
   	            b = HH(b, c, d, a, M_offset_10, 23, T[39]);
   	            a = HH(a, b, c, d, M_offset_13, 4,  T[40]);
   	            d = HH(d, a, b, c, M_offset_0,  11, T[41]);
   	            c = HH(c, d, a, b, M_offset_3,  16, T[42]);
   	            b = HH(b, c, d, a, M_offset_6,  23, T[43]);
   	            a = HH(a, b, c, d, M_offset_9,  4,  T[44]);
   	            d = HH(d, a, b, c, M_offset_12, 11, T[45]);
   	            c = HH(c, d, a, b, M_offset_15, 16, T[46]);
   	            b = HH(b, c, d, a, M_offset_2,  23, T[47]);

   	            a = II(a, b, c, d, M_offset_0,  6,  T[48]);
   	            d = II(d, a, b, c, M_offset_7,  10, T[49]);
   	            c = II(c, d, a, b, M_offset_14, 15, T[50]);
   	            b = II(b, c, d, a, M_offset_5,  21, T[51]);
   	            a = II(a, b, c, d, M_offset_12, 6,  T[52]);
   	            d = II(d, a, b, c, M_offset_3,  10, T[53]);
   	            c = II(c, d, a, b, M_offset_10, 15, T[54]);
   	            b = II(b, c, d, a, M_offset_1,  21, T[55]);
   	            a = II(a, b, c, d, M_offset_8,  6,  T[56]);
   	            d = II(d, a, b, c, M_offset_15, 10, T[57]);
   	            c = II(c, d, a, b, M_offset_6,  15, T[58]);
   	            b = II(b, c, d, a, M_offset_13, 21, T[59]);
   	            a = II(a, b, c, d, M_offset_4,  6,  T[60]);
   	            d = II(d, a, b, c, M_offset_11, 10, T[61]);
   	            c = II(c, d, a, b, M_offset_2,  15, T[62]);
   	            b = II(b, c, d, a, M_offset_9,  21, T[63]);

   	            // Intermediate hash value
   	            H[0] = (H[0] + a) | 0;
   	            H[1] = (H[1] + b) | 0;
   	            H[2] = (H[2] + c) | 0;
   	            H[3] = (H[3] + d) | 0;
   	        },

   	        _doFinalize: function () {
   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;

   	            var nBitsTotal = this._nDataBytes * 8;
   	            var nBitsLeft = data.sigBytes * 8;

   	            // Add padding
   	            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);

   	            var nBitsTotalH = Math.floor(nBitsTotal / 0x100000000);
   	            var nBitsTotalL = nBitsTotal;
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 15] = (
   	                (((nBitsTotalH << 8)  | (nBitsTotalH >>> 24)) & 0x00ff00ff) |
   	                (((nBitsTotalH << 24) | (nBitsTotalH >>> 8))  & 0xff00ff00)
   	            );
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = (
   	                (((nBitsTotalL << 8)  | (nBitsTotalL >>> 24)) & 0x00ff00ff) |
   	                (((nBitsTotalL << 24) | (nBitsTotalL >>> 8))  & 0xff00ff00)
   	            );

   	            data.sigBytes = (dataWords.length + 1) * 4;

   	            // Hash final blocks
   	            this._process();

   	            // Shortcuts
   	            var hash = this._hash;
   	            var H = hash.words;

   	            // Swap endian
   	            for (var i = 0; i < 4; i++) {
   	                // Shortcut
   	                var H_i = H[i];

   	                H[i] = (((H_i << 8)  | (H_i >>> 24)) & 0x00ff00ff) |
   	                       (((H_i << 24) | (H_i >>> 8))  & 0xff00ff00);
   	            }

   	            // Return final computed hash
   	            return hash;
   	        },

   	        clone: function () {
   	            var clone = Hasher.clone.call(this);
   	            clone._hash = this._hash.clone();

   	            return clone;
   	        }
   	    });

   	    function FF(a, b, c, d, x, s, t) {
   	        var n = a + ((b & c) | (~b & d)) + x + t;
   	        return ((n << s) | (n >>> (32 - s))) + b;
   	    }

   	    function GG(a, b, c, d, x, s, t) {
   	        var n = a + ((b & d) | (c & ~d)) + x + t;
   	        return ((n << s) | (n >>> (32 - s))) + b;
   	    }

   	    function HH(a, b, c, d, x, s, t) {
   	        var n = a + (b ^ c ^ d) + x + t;
   	        return ((n << s) | (n >>> (32 - s))) + b;
   	    }

   	    function II(a, b, c, d, x, s, t) {
   	        var n = a + (c ^ (b | ~d)) + x + t;
   	        return ((n << s) | (n >>> (32 - s))) + b;
   	    }

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.MD5('message');
   	     *     var hash = CryptoJS.MD5(wordArray);
   	     */
   	    C.MD5 = Hasher._createHelper(MD5);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacMD5(message, key);
   	     */
   	    C.HmacMD5 = Hasher._createHmacHelper(MD5);
   	}(Math));


   	return CryptoJS.MD5;

   }));
   });

   var sha1 = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var Hasher = C_lib.Hasher;
   	    var C_algo = C.algo;

   	    // Reusable object
   	    var W = [];

   	    /**
   	     * SHA-1 hash algorithm.
   	     */
   	    var SHA1 = C_algo.SHA1 = Hasher.extend({
   	        _doReset: function () {
   	            this._hash = new WordArray.init([
   	                0x67452301, 0xefcdab89,
   	                0x98badcfe, 0x10325476,
   	                0xc3d2e1f0
   	            ]);
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Shortcut
   	            var H = this._hash.words;

   	            // Working variables
   	            var a = H[0];
   	            var b = H[1];
   	            var c = H[2];
   	            var d = H[3];
   	            var e = H[4];

   	            // Computation
   	            for (var i = 0; i < 80; i++) {
   	                if (i < 16) {
   	                    W[i] = M[offset + i] | 0;
   	                } else {
   	                    var n = W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16];
   	                    W[i] = (n << 1) | (n >>> 31);
   	                }

   	                var t = ((a << 5) | (a >>> 27)) + e + W[i];
   	                if (i < 20) {
   	                    t += ((b & c) | (~b & d)) + 0x5a827999;
   	                } else if (i < 40) {
   	                    t += (b ^ c ^ d) + 0x6ed9eba1;
   	                } else if (i < 60) {
   	                    t += ((b & c) | (b & d) | (c & d)) - 0x70e44324;
   	                } else /* if (i < 80) */ {
   	                    t += (b ^ c ^ d) - 0x359d3e2a;
   	                }

   	                e = d;
   	                d = c;
   	                c = (b << 30) | (b >>> 2);
   	                b = a;
   	                a = t;
   	            }

   	            // Intermediate hash value
   	            H[0] = (H[0] + a) | 0;
   	            H[1] = (H[1] + b) | 0;
   	            H[2] = (H[2] + c) | 0;
   	            H[3] = (H[3] + d) | 0;
   	            H[4] = (H[4] + e) | 0;
   	        },

   	        _doFinalize: function () {
   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;

   	            var nBitsTotal = this._nDataBytes * 8;
   	            var nBitsLeft = data.sigBytes * 8;

   	            // Add padding
   	            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = Math.floor(nBitsTotal / 0x100000000);
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 15] = nBitsTotal;
   	            data.sigBytes = dataWords.length * 4;

   	            // Hash final blocks
   	            this._process();

   	            // Return final computed hash
   	            return this._hash;
   	        },

   	        clone: function () {
   	            var clone = Hasher.clone.call(this);
   	            clone._hash = this._hash.clone();

   	            return clone;
   	        }
   	    });

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.SHA1('message');
   	     *     var hash = CryptoJS.SHA1(wordArray);
   	     */
   	    C.SHA1 = Hasher._createHelper(SHA1);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacSHA1(message, key);
   	     */
   	    C.HmacSHA1 = Hasher._createHmacHelper(SHA1);
   	}());


   	return CryptoJS.SHA1;

   }));
   });

   var sha256 = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function (Math) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var Hasher = C_lib.Hasher;
   	    var C_algo = C.algo;

   	    // Initialization and round constants tables
   	    var H = [];
   	    var K = [];

   	    // Compute constants
   	    (function () {
   	        function isPrime(n) {
   	            var sqrtN = Math.sqrt(n);
   	            for (var factor = 2; factor <= sqrtN; factor++) {
   	                if (!(n % factor)) {
   	                    return false;
   	                }
   	            }

   	            return true;
   	        }

   	        function getFractionalBits(n) {
   	            return ((n - (n | 0)) * 0x100000000) | 0;
   	        }

   	        var n = 2;
   	        var nPrime = 0;
   	        while (nPrime < 64) {
   	            if (isPrime(n)) {
   	                if (nPrime < 8) {
   	                    H[nPrime] = getFractionalBits(Math.pow(n, 1 / 2));
   	                }
   	                K[nPrime] = getFractionalBits(Math.pow(n, 1 / 3));

   	                nPrime++;
   	            }

   	            n++;
   	        }
   	    }());

   	    // Reusable object
   	    var W = [];

   	    /**
   	     * SHA-256 hash algorithm.
   	     */
   	    var SHA256 = C_algo.SHA256 = Hasher.extend({
   	        _doReset: function () {
   	            this._hash = new WordArray.init(H.slice(0));
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Shortcut
   	            var H = this._hash.words;

   	            // Working variables
   	            var a = H[0];
   	            var b = H[1];
   	            var c = H[2];
   	            var d = H[3];
   	            var e = H[4];
   	            var f = H[5];
   	            var g = H[6];
   	            var h = H[7];

   	            // Computation
   	            for (var i = 0; i < 64; i++) {
   	                if (i < 16) {
   	                    W[i] = M[offset + i] | 0;
   	                } else {
   	                    var gamma0x = W[i - 15];
   	                    var gamma0  = ((gamma0x << 25) | (gamma0x >>> 7))  ^
   	                                  ((gamma0x << 14) | (gamma0x >>> 18)) ^
   	                                   (gamma0x >>> 3);

   	                    var gamma1x = W[i - 2];
   	                    var gamma1  = ((gamma1x << 15) | (gamma1x >>> 17)) ^
   	                                  ((gamma1x << 13) | (gamma1x >>> 19)) ^
   	                                   (gamma1x >>> 10);

   	                    W[i] = gamma0 + W[i - 7] + gamma1 + W[i - 16];
   	                }

   	                var ch  = (e & f) ^ (~e & g);
   	                var maj = (a & b) ^ (a & c) ^ (b & c);

   	                var sigma0 = ((a << 30) | (a >>> 2)) ^ ((a << 19) | (a >>> 13)) ^ ((a << 10) | (a >>> 22));
   	                var sigma1 = ((e << 26) | (e >>> 6)) ^ ((e << 21) | (e >>> 11)) ^ ((e << 7)  | (e >>> 25));

   	                var t1 = h + sigma1 + ch + K[i] + W[i];
   	                var t2 = sigma0 + maj;

   	                h = g;
   	                g = f;
   	                f = e;
   	                e = (d + t1) | 0;
   	                d = c;
   	                c = b;
   	                b = a;
   	                a = (t1 + t2) | 0;
   	            }

   	            // Intermediate hash value
   	            H[0] = (H[0] + a) | 0;
   	            H[1] = (H[1] + b) | 0;
   	            H[2] = (H[2] + c) | 0;
   	            H[3] = (H[3] + d) | 0;
   	            H[4] = (H[4] + e) | 0;
   	            H[5] = (H[5] + f) | 0;
   	            H[6] = (H[6] + g) | 0;
   	            H[7] = (H[7] + h) | 0;
   	        },

   	        _doFinalize: function () {
   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;

   	            var nBitsTotal = this._nDataBytes * 8;
   	            var nBitsLeft = data.sigBytes * 8;

   	            // Add padding
   	            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = Math.floor(nBitsTotal / 0x100000000);
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 15] = nBitsTotal;
   	            data.sigBytes = dataWords.length * 4;

   	            // Hash final blocks
   	            this._process();

   	            // Return final computed hash
   	            return this._hash;
   	        },

   	        clone: function () {
   	            var clone = Hasher.clone.call(this);
   	            clone._hash = this._hash.clone();

   	            return clone;
   	        }
   	    });

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.SHA256('message');
   	     *     var hash = CryptoJS.SHA256(wordArray);
   	     */
   	    C.SHA256 = Hasher._createHelper(SHA256);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacSHA256(message, key);
   	     */
   	    C.HmacSHA256 = Hasher._createHmacHelper(SHA256);
   	}(Math));


   	return CryptoJS.SHA256;

   }));
   });

   var sha224 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, sha256);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var C_algo = C.algo;
   	    var SHA256 = C_algo.SHA256;

   	    /**
   	     * SHA-224 hash algorithm.
   	     */
   	    var SHA224 = C_algo.SHA224 = SHA256.extend({
   	        _doReset: function () {
   	            this._hash = new WordArray.init([
   	                0xc1059ed8, 0x367cd507, 0x3070dd17, 0xf70e5939,
   	                0xffc00b31, 0x68581511, 0x64f98fa7, 0xbefa4fa4
   	            ]);
   	        },

   	        _doFinalize: function () {
   	            var hash = SHA256._doFinalize.call(this);

   	            hash.sigBytes -= 4;

   	            return hash;
   	        }
   	    });

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.SHA224('message');
   	     *     var hash = CryptoJS.SHA224(wordArray);
   	     */
   	    C.SHA224 = SHA256._createHelper(SHA224);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacSHA224(message, key);
   	     */
   	    C.HmacSHA224 = SHA256._createHmacHelper(SHA224);
   	}());


   	return CryptoJS.SHA224;

   }));
   });

   var sha512 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, x64Core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var Hasher = C_lib.Hasher;
   	    var C_x64 = C.x64;
   	    var X64Word = C_x64.Word;
   	    var X64WordArray = C_x64.WordArray;
   	    var C_algo = C.algo;

   	    function X64Word_create() {
   	        return X64Word.create.apply(X64Word, arguments);
   	    }

   	    // Constants
   	    var K = [
   	        X64Word_create(0x428a2f98, 0xd728ae22), X64Word_create(0x71374491, 0x23ef65cd),
   	        X64Word_create(0xb5c0fbcf, 0xec4d3b2f), X64Word_create(0xe9b5dba5, 0x8189dbbc),
   	        X64Word_create(0x3956c25b, 0xf348b538), X64Word_create(0x59f111f1, 0xb605d019),
   	        X64Word_create(0x923f82a4, 0xaf194f9b), X64Word_create(0xab1c5ed5, 0xda6d8118),
   	        X64Word_create(0xd807aa98, 0xa3030242), X64Word_create(0x12835b01, 0x45706fbe),
   	        X64Word_create(0x243185be, 0x4ee4b28c), X64Word_create(0x550c7dc3, 0xd5ffb4e2),
   	        X64Word_create(0x72be5d74, 0xf27b896f), X64Word_create(0x80deb1fe, 0x3b1696b1),
   	        X64Word_create(0x9bdc06a7, 0x25c71235), X64Word_create(0xc19bf174, 0xcf692694),
   	        X64Word_create(0xe49b69c1, 0x9ef14ad2), X64Word_create(0xefbe4786, 0x384f25e3),
   	        X64Word_create(0x0fc19dc6, 0x8b8cd5b5), X64Word_create(0x240ca1cc, 0x77ac9c65),
   	        X64Word_create(0x2de92c6f, 0x592b0275), X64Word_create(0x4a7484aa, 0x6ea6e483),
   	        X64Word_create(0x5cb0a9dc, 0xbd41fbd4), X64Word_create(0x76f988da, 0x831153b5),
   	        X64Word_create(0x983e5152, 0xee66dfab), X64Word_create(0xa831c66d, 0x2db43210),
   	        X64Word_create(0xb00327c8, 0x98fb213f), X64Word_create(0xbf597fc7, 0xbeef0ee4),
   	        X64Word_create(0xc6e00bf3, 0x3da88fc2), X64Word_create(0xd5a79147, 0x930aa725),
   	        X64Word_create(0x06ca6351, 0xe003826f), X64Word_create(0x14292967, 0x0a0e6e70),
   	        X64Word_create(0x27b70a85, 0x46d22ffc), X64Word_create(0x2e1b2138, 0x5c26c926),
   	        X64Word_create(0x4d2c6dfc, 0x5ac42aed), X64Word_create(0x53380d13, 0x9d95b3df),
   	        X64Word_create(0x650a7354, 0x8baf63de), X64Word_create(0x766a0abb, 0x3c77b2a8),
   	        X64Word_create(0x81c2c92e, 0x47edaee6), X64Word_create(0x92722c85, 0x1482353b),
   	        X64Word_create(0xa2bfe8a1, 0x4cf10364), X64Word_create(0xa81a664b, 0xbc423001),
   	        X64Word_create(0xc24b8b70, 0xd0f89791), X64Word_create(0xc76c51a3, 0x0654be30),
   	        X64Word_create(0xd192e819, 0xd6ef5218), X64Word_create(0xd6990624, 0x5565a910),
   	        X64Word_create(0xf40e3585, 0x5771202a), X64Word_create(0x106aa070, 0x32bbd1b8),
   	        X64Word_create(0x19a4c116, 0xb8d2d0c8), X64Word_create(0x1e376c08, 0x5141ab53),
   	        X64Word_create(0x2748774c, 0xdf8eeb99), X64Word_create(0x34b0bcb5, 0xe19b48a8),
   	        X64Word_create(0x391c0cb3, 0xc5c95a63), X64Word_create(0x4ed8aa4a, 0xe3418acb),
   	        X64Word_create(0x5b9cca4f, 0x7763e373), X64Word_create(0x682e6ff3, 0xd6b2b8a3),
   	        X64Word_create(0x748f82ee, 0x5defb2fc), X64Word_create(0x78a5636f, 0x43172f60),
   	        X64Word_create(0x84c87814, 0xa1f0ab72), X64Word_create(0x8cc70208, 0x1a6439ec),
   	        X64Word_create(0x90befffa, 0x23631e28), X64Word_create(0xa4506ceb, 0xde82bde9),
   	        X64Word_create(0xbef9a3f7, 0xb2c67915), X64Word_create(0xc67178f2, 0xe372532b),
   	        X64Word_create(0xca273ece, 0xea26619c), X64Word_create(0xd186b8c7, 0x21c0c207),
   	        X64Word_create(0xeada7dd6, 0xcde0eb1e), X64Word_create(0xf57d4f7f, 0xee6ed178),
   	        X64Word_create(0x06f067aa, 0x72176fba), X64Word_create(0x0a637dc5, 0xa2c898a6),
   	        X64Word_create(0x113f9804, 0xbef90dae), X64Word_create(0x1b710b35, 0x131c471b),
   	        X64Word_create(0x28db77f5, 0x23047d84), X64Word_create(0x32caab7b, 0x40c72493),
   	        X64Word_create(0x3c9ebe0a, 0x15c9bebc), X64Word_create(0x431d67c4, 0x9c100d4c),
   	        X64Word_create(0x4cc5d4be, 0xcb3e42b6), X64Word_create(0x597f299c, 0xfc657e2a),
   	        X64Word_create(0x5fcb6fab, 0x3ad6faec), X64Word_create(0x6c44198c, 0x4a475817)
   	    ];

   	    // Reusable objects
   	    var W = [];
   	    (function () {
   	        for (var i = 0; i < 80; i++) {
   	            W[i] = X64Word_create();
   	        }
   	    }());

   	    /**
   	     * SHA-512 hash algorithm.
   	     */
   	    var SHA512 = C_algo.SHA512 = Hasher.extend({
   	        _doReset: function () {
   	            this._hash = new X64WordArray.init([
   	                new X64Word.init(0x6a09e667, 0xf3bcc908), new X64Word.init(0xbb67ae85, 0x84caa73b),
   	                new X64Word.init(0x3c6ef372, 0xfe94f82b), new X64Word.init(0xa54ff53a, 0x5f1d36f1),
   	                new X64Word.init(0x510e527f, 0xade682d1), new X64Word.init(0x9b05688c, 0x2b3e6c1f),
   	                new X64Word.init(0x1f83d9ab, 0xfb41bd6b), new X64Word.init(0x5be0cd19, 0x137e2179)
   	            ]);
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Shortcuts
   	            var H = this._hash.words;

   	            var H0 = H[0];
   	            var H1 = H[1];
   	            var H2 = H[2];
   	            var H3 = H[3];
   	            var H4 = H[4];
   	            var H5 = H[5];
   	            var H6 = H[6];
   	            var H7 = H[7];

   	            var H0h = H0.high;
   	            var H0l = H0.low;
   	            var H1h = H1.high;
   	            var H1l = H1.low;
   	            var H2h = H2.high;
   	            var H2l = H2.low;
   	            var H3h = H3.high;
   	            var H3l = H3.low;
   	            var H4h = H4.high;
   	            var H4l = H4.low;
   	            var H5h = H5.high;
   	            var H5l = H5.low;
   	            var H6h = H6.high;
   	            var H6l = H6.low;
   	            var H7h = H7.high;
   	            var H7l = H7.low;

   	            // Working variables
   	            var ah = H0h;
   	            var al = H0l;
   	            var bh = H1h;
   	            var bl = H1l;
   	            var ch = H2h;
   	            var cl = H2l;
   	            var dh = H3h;
   	            var dl = H3l;
   	            var eh = H4h;
   	            var el = H4l;
   	            var fh = H5h;
   	            var fl = H5l;
   	            var gh = H6h;
   	            var gl = H6l;
   	            var hh = H7h;
   	            var hl = H7l;

   	            // Rounds
   	            for (var i = 0; i < 80; i++) {
   	                var Wil;
   	                var Wih;

   	                // Shortcut
   	                var Wi = W[i];

   	                // Extend message
   	                if (i < 16) {
   	                    Wih = Wi.high = M[offset + i * 2]     | 0;
   	                    Wil = Wi.low  = M[offset + i * 2 + 1] | 0;
   	                } else {
   	                    // Gamma0
   	                    var gamma0x  = W[i - 15];
   	                    var gamma0xh = gamma0x.high;
   	                    var gamma0xl = gamma0x.low;
   	                    var gamma0h  = ((gamma0xh >>> 1) | (gamma0xl << 31)) ^ ((gamma0xh >>> 8) | (gamma0xl << 24)) ^ (gamma0xh >>> 7);
   	                    var gamma0l  = ((gamma0xl >>> 1) | (gamma0xh << 31)) ^ ((gamma0xl >>> 8) | (gamma0xh << 24)) ^ ((gamma0xl >>> 7) | (gamma0xh << 25));

   	                    // Gamma1
   	                    var gamma1x  = W[i - 2];
   	                    var gamma1xh = gamma1x.high;
   	                    var gamma1xl = gamma1x.low;
   	                    var gamma1h  = ((gamma1xh >>> 19) | (gamma1xl << 13)) ^ ((gamma1xh << 3) | (gamma1xl >>> 29)) ^ (gamma1xh >>> 6);
   	                    var gamma1l  = ((gamma1xl >>> 19) | (gamma1xh << 13)) ^ ((gamma1xl << 3) | (gamma1xh >>> 29)) ^ ((gamma1xl >>> 6) | (gamma1xh << 26));

   	                    // W[i] = gamma0 + W[i - 7] + gamma1 + W[i - 16]
   	                    var Wi7  = W[i - 7];
   	                    var Wi7h = Wi7.high;
   	                    var Wi7l = Wi7.low;

   	                    var Wi16  = W[i - 16];
   	                    var Wi16h = Wi16.high;
   	                    var Wi16l = Wi16.low;

   	                    Wil = gamma0l + Wi7l;
   	                    Wih = gamma0h + Wi7h + ((Wil >>> 0) < (gamma0l >>> 0) ? 1 : 0);
   	                    Wil = Wil + gamma1l;
   	                    Wih = Wih + gamma1h + ((Wil >>> 0) < (gamma1l >>> 0) ? 1 : 0);
   	                    Wil = Wil + Wi16l;
   	                    Wih = Wih + Wi16h + ((Wil >>> 0) < (Wi16l >>> 0) ? 1 : 0);

   	                    Wi.high = Wih;
   	                    Wi.low  = Wil;
   	                }

   	                var chh  = (eh & fh) ^ (~eh & gh);
   	                var chl  = (el & fl) ^ (~el & gl);
   	                var majh = (ah & bh) ^ (ah & ch) ^ (bh & ch);
   	                var majl = (al & bl) ^ (al & cl) ^ (bl & cl);

   	                var sigma0h = ((ah >>> 28) | (al << 4))  ^ ((ah << 30)  | (al >>> 2)) ^ ((ah << 25) | (al >>> 7));
   	                var sigma0l = ((al >>> 28) | (ah << 4))  ^ ((al << 30)  | (ah >>> 2)) ^ ((al << 25) | (ah >>> 7));
   	                var sigma1h = ((eh >>> 14) | (el << 18)) ^ ((eh >>> 18) | (el << 14)) ^ ((eh << 23) | (el >>> 9));
   	                var sigma1l = ((el >>> 14) | (eh << 18)) ^ ((el >>> 18) | (eh << 14)) ^ ((el << 23) | (eh >>> 9));

   	                // t1 = h + sigma1 + ch + K[i] + W[i]
   	                var Ki  = K[i];
   	                var Kih = Ki.high;
   	                var Kil = Ki.low;

   	                var t1l = hl + sigma1l;
   	                var t1h = hh + sigma1h + ((t1l >>> 0) < (hl >>> 0) ? 1 : 0);
   	                var t1l = t1l + chl;
   	                var t1h = t1h + chh + ((t1l >>> 0) < (chl >>> 0) ? 1 : 0);
   	                var t1l = t1l + Kil;
   	                var t1h = t1h + Kih + ((t1l >>> 0) < (Kil >>> 0) ? 1 : 0);
   	                var t1l = t1l + Wil;
   	                var t1h = t1h + Wih + ((t1l >>> 0) < (Wil >>> 0) ? 1 : 0);

   	                // t2 = sigma0 + maj
   	                var t2l = sigma0l + majl;
   	                var t2h = sigma0h + majh + ((t2l >>> 0) < (sigma0l >>> 0) ? 1 : 0);

   	                // Update working variables
   	                hh = gh;
   	                hl = gl;
   	                gh = fh;
   	                gl = fl;
   	                fh = eh;
   	                fl = el;
   	                el = (dl + t1l) | 0;
   	                eh = (dh + t1h + ((el >>> 0) < (dl >>> 0) ? 1 : 0)) | 0;
   	                dh = ch;
   	                dl = cl;
   	                ch = bh;
   	                cl = bl;
   	                bh = ah;
   	                bl = al;
   	                al = (t1l + t2l) | 0;
   	                ah = (t1h + t2h + ((al >>> 0) < (t1l >>> 0) ? 1 : 0)) | 0;
   	            }

   	            // Intermediate hash value
   	            H0l = H0.low  = (H0l + al);
   	            H0.high = (H0h + ah + ((H0l >>> 0) < (al >>> 0) ? 1 : 0));
   	            H1l = H1.low  = (H1l + bl);
   	            H1.high = (H1h + bh + ((H1l >>> 0) < (bl >>> 0) ? 1 : 0));
   	            H2l = H2.low  = (H2l + cl);
   	            H2.high = (H2h + ch + ((H2l >>> 0) < (cl >>> 0) ? 1 : 0));
   	            H3l = H3.low  = (H3l + dl);
   	            H3.high = (H3h + dh + ((H3l >>> 0) < (dl >>> 0) ? 1 : 0));
   	            H4l = H4.low  = (H4l + el);
   	            H4.high = (H4h + eh + ((H4l >>> 0) < (el >>> 0) ? 1 : 0));
   	            H5l = H5.low  = (H5l + fl);
   	            H5.high = (H5h + fh + ((H5l >>> 0) < (fl >>> 0) ? 1 : 0));
   	            H6l = H6.low  = (H6l + gl);
   	            H6.high = (H6h + gh + ((H6l >>> 0) < (gl >>> 0) ? 1 : 0));
   	            H7l = H7.low  = (H7l + hl);
   	            H7.high = (H7h + hh + ((H7l >>> 0) < (hl >>> 0) ? 1 : 0));
   	        },

   	        _doFinalize: function () {
   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;

   	            var nBitsTotal = this._nDataBytes * 8;
   	            var nBitsLeft = data.sigBytes * 8;

   	            // Add padding
   	            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
   	            dataWords[(((nBitsLeft + 128) >>> 10) << 5) + 30] = Math.floor(nBitsTotal / 0x100000000);
   	            dataWords[(((nBitsLeft + 128) >>> 10) << 5) + 31] = nBitsTotal;
   	            data.sigBytes = dataWords.length * 4;

   	            // Hash final blocks
   	            this._process();

   	            // Convert hash to 32-bit word array before returning
   	            var hash = this._hash.toX32();

   	            // Return final computed hash
   	            return hash;
   	        },

   	        clone: function () {
   	            var clone = Hasher.clone.call(this);
   	            clone._hash = this._hash.clone();

   	            return clone;
   	        },

   	        blockSize: 1024/32
   	    });

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.SHA512('message');
   	     *     var hash = CryptoJS.SHA512(wordArray);
   	     */
   	    C.SHA512 = Hasher._createHelper(SHA512);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacSHA512(message, key);
   	     */
   	    C.HmacSHA512 = Hasher._createHmacHelper(SHA512);
   	}());


   	return CryptoJS.SHA512;

   }));
   });

   var sha384 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, x64Core, sha512);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_x64 = C.x64;
   	    var X64Word = C_x64.Word;
   	    var X64WordArray = C_x64.WordArray;
   	    var C_algo = C.algo;
   	    var SHA512 = C_algo.SHA512;

   	    /**
   	     * SHA-384 hash algorithm.
   	     */
   	    var SHA384 = C_algo.SHA384 = SHA512.extend({
   	        _doReset: function () {
   	            this._hash = new X64WordArray.init([
   	                new X64Word.init(0xcbbb9d5d, 0xc1059ed8), new X64Word.init(0x629a292a, 0x367cd507),
   	                new X64Word.init(0x9159015a, 0x3070dd17), new X64Word.init(0x152fecd8, 0xf70e5939),
   	                new X64Word.init(0x67332667, 0xffc00b31), new X64Word.init(0x8eb44a87, 0x68581511),
   	                new X64Word.init(0xdb0c2e0d, 0x64f98fa7), new X64Word.init(0x47b5481d, 0xbefa4fa4)
   	            ]);
   	        },

   	        _doFinalize: function () {
   	            var hash = SHA512._doFinalize.call(this);

   	            hash.sigBytes -= 16;

   	            return hash;
   	        }
   	    });

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.SHA384('message');
   	     *     var hash = CryptoJS.SHA384(wordArray);
   	     */
   	    C.SHA384 = SHA512._createHelper(SHA384);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacSHA384(message, key);
   	     */
   	    C.HmacSHA384 = SHA512._createHmacHelper(SHA384);
   	}());


   	return CryptoJS.SHA384;

   }));
   });

   var sha3 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, x64Core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function (Math) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var Hasher = C_lib.Hasher;
   	    var C_x64 = C.x64;
   	    var X64Word = C_x64.Word;
   	    var C_algo = C.algo;

   	    // Constants tables
   	    var RHO_OFFSETS = [];
   	    var PI_INDEXES  = [];
   	    var ROUND_CONSTANTS = [];

   	    // Compute Constants
   	    (function () {
   	        // Compute rho offset constants
   	        var x = 1, y = 0;
   	        for (var t = 0; t < 24; t++) {
   	            RHO_OFFSETS[x + 5 * y] = ((t + 1) * (t + 2) / 2) % 64;

   	            var newX = y % 5;
   	            var newY = (2 * x + 3 * y) % 5;
   	            x = newX;
   	            y = newY;
   	        }

   	        // Compute pi index constants
   	        for (var x = 0; x < 5; x++) {
   	            for (var y = 0; y < 5; y++) {
   	                PI_INDEXES[x + 5 * y] = y + ((2 * x + 3 * y) % 5) * 5;
   	            }
   	        }

   	        // Compute round constants
   	        var LFSR = 0x01;
   	        for (var i = 0; i < 24; i++) {
   	            var roundConstantMsw = 0;
   	            var roundConstantLsw = 0;

   	            for (var j = 0; j < 7; j++) {
   	                if (LFSR & 0x01) {
   	                    var bitPosition = (1 << j) - 1;
   	                    if (bitPosition < 32) {
   	                        roundConstantLsw ^= 1 << bitPosition;
   	                    } else /* if (bitPosition >= 32) */ {
   	                        roundConstantMsw ^= 1 << (bitPosition - 32);
   	                    }
   	                }

   	                // Compute next LFSR
   	                if (LFSR & 0x80) {
   	                    // Primitive polynomial over GF(2): x^8 + x^6 + x^5 + x^4 + 1
   	                    LFSR = (LFSR << 1) ^ 0x71;
   	                } else {
   	                    LFSR <<= 1;
   	                }
   	            }

   	            ROUND_CONSTANTS[i] = X64Word.create(roundConstantMsw, roundConstantLsw);
   	        }
   	    }());

   	    // Reusable objects for temporary values
   	    var T = [];
   	    (function () {
   	        for (var i = 0; i < 25; i++) {
   	            T[i] = X64Word.create();
   	        }
   	    }());

   	    /**
   	     * SHA-3 hash algorithm.
   	     */
   	    var SHA3 = C_algo.SHA3 = Hasher.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {number} outputLength
   	         *   The desired number of bits in the output hash.
   	         *   Only values permitted are: 224, 256, 384, 512.
   	         *   Default: 512
   	         */
   	        cfg: Hasher.cfg.extend({
   	            outputLength: 512
   	        }),

   	        _doReset: function () {
   	            var state = this._state = [];
   	            for (var i = 0; i < 25; i++) {
   	                state[i] = new X64Word.init();
   	            }

   	            this.blockSize = (1600 - 2 * this.cfg.outputLength) / 32;
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Shortcuts
   	            var state = this._state;
   	            var nBlockSizeLanes = this.blockSize / 2;

   	            // Absorb
   	            for (var i = 0; i < nBlockSizeLanes; i++) {
   	                // Shortcuts
   	                var M2i  = M[offset + 2 * i];
   	                var M2i1 = M[offset + 2 * i + 1];

   	                // Swap endian
   	                M2i = (
   	                    (((M2i << 8)  | (M2i >>> 24)) & 0x00ff00ff) |
   	                    (((M2i << 24) | (M2i >>> 8))  & 0xff00ff00)
   	                );
   	                M2i1 = (
   	                    (((M2i1 << 8)  | (M2i1 >>> 24)) & 0x00ff00ff) |
   	                    (((M2i1 << 24) | (M2i1 >>> 8))  & 0xff00ff00)
   	                );

   	                // Absorb message into state
   	                var lane = state[i];
   	                lane.high ^= M2i1;
   	                lane.low  ^= M2i;
   	            }

   	            // Rounds
   	            for (var round = 0; round < 24; round++) {
   	                // Theta
   	                for (var x = 0; x < 5; x++) {
   	                    // Mix column lanes
   	                    var tMsw = 0, tLsw = 0;
   	                    for (var y = 0; y < 5; y++) {
   	                        var lane = state[x + 5 * y];
   	                        tMsw ^= lane.high;
   	                        tLsw ^= lane.low;
   	                    }

   	                    // Temporary values
   	                    var Tx = T[x];
   	                    Tx.high = tMsw;
   	                    Tx.low  = tLsw;
   	                }
   	                for (var x = 0; x < 5; x++) {
   	                    // Shortcuts
   	                    var Tx4 = T[(x + 4) % 5];
   	                    var Tx1 = T[(x + 1) % 5];
   	                    var Tx1Msw = Tx1.high;
   	                    var Tx1Lsw = Tx1.low;

   	                    // Mix surrounding columns
   	                    var tMsw = Tx4.high ^ ((Tx1Msw << 1) | (Tx1Lsw >>> 31));
   	                    var tLsw = Tx4.low  ^ ((Tx1Lsw << 1) | (Tx1Msw >>> 31));
   	                    for (var y = 0; y < 5; y++) {
   	                        var lane = state[x + 5 * y];
   	                        lane.high ^= tMsw;
   	                        lane.low  ^= tLsw;
   	                    }
   	                }

   	                // Rho Pi
   	                for (var laneIndex = 1; laneIndex < 25; laneIndex++) {
   	                    var tMsw;
   	                    var tLsw;

   	                    // Shortcuts
   	                    var lane = state[laneIndex];
   	                    var laneMsw = lane.high;
   	                    var laneLsw = lane.low;
   	                    var rhoOffset = RHO_OFFSETS[laneIndex];

   	                    // Rotate lanes
   	                    if (rhoOffset < 32) {
   	                        tMsw = (laneMsw << rhoOffset) | (laneLsw >>> (32 - rhoOffset));
   	                        tLsw = (laneLsw << rhoOffset) | (laneMsw >>> (32 - rhoOffset));
   	                    } else /* if (rhoOffset >= 32) */ {
   	                        tMsw = (laneLsw << (rhoOffset - 32)) | (laneMsw >>> (64 - rhoOffset));
   	                        tLsw = (laneMsw << (rhoOffset - 32)) | (laneLsw >>> (64 - rhoOffset));
   	                    }

   	                    // Transpose lanes
   	                    var TPiLane = T[PI_INDEXES[laneIndex]];
   	                    TPiLane.high = tMsw;
   	                    TPiLane.low  = tLsw;
   	                }

   	                // Rho pi at x = y = 0
   	                var T0 = T[0];
   	                var state0 = state[0];
   	                T0.high = state0.high;
   	                T0.low  = state0.low;

   	                // Chi
   	                for (var x = 0; x < 5; x++) {
   	                    for (var y = 0; y < 5; y++) {
   	                        // Shortcuts
   	                        var laneIndex = x + 5 * y;
   	                        var lane = state[laneIndex];
   	                        var TLane = T[laneIndex];
   	                        var Tx1Lane = T[((x + 1) % 5) + 5 * y];
   	                        var Tx2Lane = T[((x + 2) % 5) + 5 * y];

   	                        // Mix rows
   	                        lane.high = TLane.high ^ (~Tx1Lane.high & Tx2Lane.high);
   	                        lane.low  = TLane.low  ^ (~Tx1Lane.low  & Tx2Lane.low);
   	                    }
   	                }

   	                // Iota
   	                var lane = state[0];
   	                var roundConstant = ROUND_CONSTANTS[round];
   	                lane.high ^= roundConstant.high;
   	                lane.low  ^= roundConstant.low;
   	            }
   	        },

   	        _doFinalize: function () {
   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;
   	            var nBitsLeft = data.sigBytes * 8;
   	            var blockSizeBits = this.blockSize * 32;

   	            // Add padding
   	            dataWords[nBitsLeft >>> 5] |= 0x1 << (24 - nBitsLeft % 32);
   	            dataWords[((Math.ceil((nBitsLeft + 1) / blockSizeBits) * blockSizeBits) >>> 5) - 1] |= 0x80;
   	            data.sigBytes = dataWords.length * 4;

   	            // Hash final blocks
   	            this._process();

   	            // Shortcuts
   	            var state = this._state;
   	            var outputLengthBytes = this.cfg.outputLength / 8;
   	            var outputLengthLanes = outputLengthBytes / 8;

   	            // Squeeze
   	            var hashWords = [];
   	            for (var i = 0; i < outputLengthLanes; i++) {
   	                // Shortcuts
   	                var lane = state[i];
   	                var laneMsw = lane.high;
   	                var laneLsw = lane.low;

   	                // Swap endian
   	                laneMsw = (
   	                    (((laneMsw << 8)  | (laneMsw >>> 24)) & 0x00ff00ff) |
   	                    (((laneMsw << 24) | (laneMsw >>> 8))  & 0xff00ff00)
   	                );
   	                laneLsw = (
   	                    (((laneLsw << 8)  | (laneLsw >>> 24)) & 0x00ff00ff) |
   	                    (((laneLsw << 24) | (laneLsw >>> 8))  & 0xff00ff00)
   	                );

   	                // Squeeze state to retrieve hash
   	                hashWords.push(laneLsw);
   	                hashWords.push(laneMsw);
   	            }

   	            // Return final computed hash
   	            return new WordArray.init(hashWords, outputLengthBytes);
   	        },

   	        clone: function () {
   	            var clone = Hasher.clone.call(this);

   	            var state = clone._state = this._state.slice(0);
   	            for (var i = 0; i < 25; i++) {
   	                state[i] = state[i].clone();
   	            }

   	            return clone;
   	        }
   	    });

   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.SHA3('message');
   	     *     var hash = CryptoJS.SHA3(wordArray);
   	     */
   	    C.SHA3 = Hasher._createHelper(SHA3);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacSHA3(message, key);
   	     */
   	    C.HmacSHA3 = Hasher._createHmacHelper(SHA3);
   	}(Math));


   	return CryptoJS.SHA3;

   }));
   });

   var ripemd160 = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/** @preserve
   	(c) 2012 by Cédric Mesnil. All rights reserved.

   	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   	    - Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   	    - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   	*/

   	(function (Math) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var Hasher = C_lib.Hasher;
   	    var C_algo = C.algo;

   	    // Constants table
   	    var _zl = WordArray.create([
   	        0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
   	        7,  4, 13,  1, 10,  6, 15,  3, 12,  0,  9,  5,  2, 14, 11,  8,
   	        3, 10, 14,  4,  9, 15,  8,  1,  2,  7,  0,  6, 13, 11,  5, 12,
   	        1,  9, 11, 10,  0,  8, 12,  4, 13,  3,  7, 15, 14,  5,  6,  2,
   	        4,  0,  5,  9,  7, 12,  2, 10, 14,  1,  3,  8, 11,  6, 15, 13]);
   	    var _zr = WordArray.create([
   	        5, 14,  7,  0,  9,  2, 11,  4, 13,  6, 15,  8,  1, 10,  3, 12,
   	        6, 11,  3,  7,  0, 13,  5, 10, 14, 15,  8, 12,  4,  9,  1,  2,
   	        15,  5,  1,  3,  7, 14,  6,  9, 11,  8, 12,  2, 10,  0,  4, 13,
   	        8,  6,  4,  1,  3, 11, 15,  0,  5, 12,  2, 13,  9,  7, 10, 14,
   	        12, 15, 10,  4,  1,  5,  8,  7,  6,  2, 13, 14,  0,  3,  9, 11]);
   	    var _sl = WordArray.create([
   	         11, 14, 15, 12,  5,  8,  7,  9, 11, 13, 14, 15,  6,  7,  9,  8,
   	        7, 6,   8, 13, 11,  9,  7, 15,  7, 12, 15,  9, 11,  7, 13, 12,
   	        11, 13,  6,  7, 14,  9, 13, 15, 14,  8, 13,  6,  5, 12,  7,  5,
   	          11, 12, 14, 15, 14, 15,  9,  8,  9, 14,  5,  6,  8,  6,  5, 12,
   	        9, 15,  5, 11,  6,  8, 13, 12,  5, 12, 13, 14, 11,  8,  5,  6 ]);
   	    var _sr = WordArray.create([
   	        8,  9,  9, 11, 13, 15, 15,  5,  7,  7,  8, 11, 14, 14, 12,  6,
   	        9, 13, 15,  7, 12,  8,  9, 11,  7,  7, 12,  7,  6, 15, 13, 11,
   	        9,  7, 15, 11,  8,  6,  6, 14, 12, 13,  5, 14, 13, 13,  7,  5,
   	        15,  5,  8, 11, 14, 14,  6, 14,  6,  9, 12,  9, 12,  5, 15,  8,
   	        8,  5, 12,  9, 12,  5, 14,  6,  8, 13,  6,  5, 15, 13, 11, 11 ]);

   	    var _hl =  WordArray.create([ 0x00000000, 0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xA953FD4E]);
   	    var _hr =  WordArray.create([ 0x50A28BE6, 0x5C4DD124, 0x6D703EF3, 0x7A6D76E9, 0x00000000]);

   	    /**
   	     * RIPEMD160 hash algorithm.
   	     */
   	    var RIPEMD160 = C_algo.RIPEMD160 = Hasher.extend({
   	        _doReset: function () {
   	            this._hash  = WordArray.create([0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0]);
   	        },

   	        _doProcessBlock: function (M, offset) {

   	            // Swap endian
   	            for (var i = 0; i < 16; i++) {
   	                // Shortcuts
   	                var offset_i = offset + i;
   	                var M_offset_i = M[offset_i];

   	                // Swap
   	                M[offset_i] = (
   	                    (((M_offset_i << 8)  | (M_offset_i >>> 24)) & 0x00ff00ff) |
   	                    (((M_offset_i << 24) | (M_offset_i >>> 8))  & 0xff00ff00)
   	                );
   	            }
   	            // Shortcut
   	            var H  = this._hash.words;
   	            var hl = _hl.words;
   	            var hr = _hr.words;
   	            var zl = _zl.words;
   	            var zr = _zr.words;
   	            var sl = _sl.words;
   	            var sr = _sr.words;

   	            // Working variables
   	            var al, bl, cl, dl, el;
   	            var ar, br, cr, dr, er;

   	            ar = al = H[0];
   	            br = bl = H[1];
   	            cr = cl = H[2];
   	            dr = dl = H[3];
   	            er = el = H[4];
   	            // Computation
   	            var t;
   	            for (var i = 0; i < 80; i += 1) {
   	                t = (al +  M[offset+zl[i]])|0;
   	                if (i<16){
   		            t +=  f1(bl,cl,dl) + hl[0];
   	                } else if (i<32) {
   		            t +=  f2(bl,cl,dl) + hl[1];
   	                } else if (i<48) {
   		            t +=  f3(bl,cl,dl) + hl[2];
   	                } else if (i<64) {
   		            t +=  f4(bl,cl,dl) + hl[3];
   	                } else {// if (i<80) {
   		            t +=  f5(bl,cl,dl) + hl[4];
   	                }
   	                t = t|0;
   	                t =  rotl(t,sl[i]);
   	                t = (t+el)|0;
   	                al = el;
   	                el = dl;
   	                dl = rotl(cl, 10);
   	                cl = bl;
   	                bl = t;

   	                t = (ar + M[offset+zr[i]])|0;
   	                if (i<16){
   		            t +=  f5(br,cr,dr) + hr[0];
   	                } else if (i<32) {
   		            t +=  f4(br,cr,dr) + hr[1];
   	                } else if (i<48) {
   		            t +=  f3(br,cr,dr) + hr[2];
   	                } else if (i<64) {
   		            t +=  f2(br,cr,dr) + hr[3];
   	                } else {// if (i<80) {
   		            t +=  f1(br,cr,dr) + hr[4];
   	                }
   	                t = t|0;
   	                t =  rotl(t,sr[i]) ;
   	                t = (t+er)|0;
   	                ar = er;
   	                er = dr;
   	                dr = rotl(cr, 10);
   	                cr = br;
   	                br = t;
   	            }
   	            // Intermediate hash value
   	            t    = (H[1] + cl + dr)|0;
   	            H[1] = (H[2] + dl + er)|0;
   	            H[2] = (H[3] + el + ar)|0;
   	            H[3] = (H[4] + al + br)|0;
   	            H[4] = (H[0] + bl + cr)|0;
   	            H[0] =  t;
   	        },

   	        _doFinalize: function () {
   	            // Shortcuts
   	            var data = this._data;
   	            var dataWords = data.words;

   	            var nBitsTotal = this._nDataBytes * 8;
   	            var nBitsLeft = data.sigBytes * 8;

   	            // Add padding
   	            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
   	            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = (
   	                (((nBitsTotal << 8)  | (nBitsTotal >>> 24)) & 0x00ff00ff) |
   	                (((nBitsTotal << 24) | (nBitsTotal >>> 8))  & 0xff00ff00)
   	            );
   	            data.sigBytes = (dataWords.length + 1) * 4;

   	            // Hash final blocks
   	            this._process();

   	            // Shortcuts
   	            var hash = this._hash;
   	            var H = hash.words;

   	            // Swap endian
   	            for (var i = 0; i < 5; i++) {
   	                // Shortcut
   	                var H_i = H[i];

   	                // Swap
   	                H[i] = (((H_i << 8)  | (H_i >>> 24)) & 0x00ff00ff) |
   	                       (((H_i << 24) | (H_i >>> 8))  & 0xff00ff00);
   	            }

   	            // Return final computed hash
   	            return hash;
   	        },

   	        clone: function () {
   	            var clone = Hasher.clone.call(this);
   	            clone._hash = this._hash.clone();

   	            return clone;
   	        }
   	    });


   	    function f1(x, y, z) {
   	        return ((x) ^ (y) ^ (z));

   	    }

   	    function f2(x, y, z) {
   	        return (((x)&(y)) | ((~x)&(z)));
   	    }

   	    function f3(x, y, z) {
   	        return (((x) | (~(y))) ^ (z));
   	    }

   	    function f4(x, y, z) {
   	        return (((x) & (z)) | ((y)&(~(z))));
   	    }

   	    function f5(x, y, z) {
   	        return ((x) ^ ((y) |(~(z))));

   	    }

   	    function rotl(x,n) {
   	        return (x<<n) | (x>>>(32-n));
   	    }


   	    /**
   	     * Shortcut function to the hasher's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     *
   	     * @return {WordArray} The hash.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hash = CryptoJS.RIPEMD160('message');
   	     *     var hash = CryptoJS.RIPEMD160(wordArray);
   	     */
   	    C.RIPEMD160 = Hasher._createHelper(RIPEMD160);

   	    /**
   	     * Shortcut function to the HMAC's object interface.
   	     *
   	     * @param {WordArray|string} message The message to hash.
   	     * @param {WordArray|string} key The secret key.
   	     *
   	     * @return {WordArray} The HMAC.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var hmac = CryptoJS.HmacRIPEMD160(message, key);
   	     */
   	    C.HmacRIPEMD160 = Hasher._createHmacHelper(RIPEMD160);
   	}());


   	return CryptoJS.RIPEMD160;

   }));
   });

   var hmac = createCommonjsModule(function (module, exports) {
   (function (root, factory) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var Base = C_lib.Base;
   	    var C_enc = C.enc;
   	    var Utf8 = C_enc.Utf8;
   	    var C_algo = C.algo;

   	    /**
   	     * HMAC algorithm.
   	     */
   	    var HMAC = C_algo.HMAC = Base.extend({
   	        /**
   	         * Initializes a newly created HMAC.
   	         *
   	         * @param {Hasher} hasher The hash algorithm to use.
   	         * @param {WordArray|string} key The secret key.
   	         *
   	         * @example
   	         *
   	         *     var hmacHasher = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA256, key);
   	         */
   	        init: function (hasher, key) {
   	            // Init hasher
   	            hasher = this._hasher = new hasher.init();

   	            // Convert string to WordArray, else assume WordArray already
   	            if (typeof key == 'string') {
   	                key = Utf8.parse(key);
   	            }

   	            // Shortcuts
   	            var hasherBlockSize = hasher.blockSize;
   	            var hasherBlockSizeBytes = hasherBlockSize * 4;

   	            // Allow arbitrary length keys
   	            if (key.sigBytes > hasherBlockSizeBytes) {
   	                key = hasher.finalize(key);
   	            }

   	            // Clamp excess bits
   	            key.clamp();

   	            // Clone key for inner and outer pads
   	            var oKey = this._oKey = key.clone();
   	            var iKey = this._iKey = key.clone();

   	            // Shortcuts
   	            var oKeyWords = oKey.words;
   	            var iKeyWords = iKey.words;

   	            // XOR keys with pad constants
   	            for (var i = 0; i < hasherBlockSize; i++) {
   	                oKeyWords[i] ^= 0x5c5c5c5c;
   	                iKeyWords[i] ^= 0x36363636;
   	            }
   	            oKey.sigBytes = iKey.sigBytes = hasherBlockSizeBytes;

   	            // Set initial values
   	            this.reset();
   	        },

   	        /**
   	         * Resets this HMAC to its initial state.
   	         *
   	         * @example
   	         *
   	         *     hmacHasher.reset();
   	         */
   	        reset: function () {
   	            // Shortcut
   	            var hasher = this._hasher;

   	            // Reset
   	            hasher.reset();
   	            hasher.update(this._iKey);
   	        },

   	        /**
   	         * Updates this HMAC with a message.
   	         *
   	         * @param {WordArray|string} messageUpdate The message to append.
   	         *
   	         * @return {HMAC} This HMAC instance.
   	         *
   	         * @example
   	         *
   	         *     hmacHasher.update('message');
   	         *     hmacHasher.update(wordArray);
   	         */
   	        update: function (messageUpdate) {
   	            this._hasher.update(messageUpdate);

   	            // Chainable
   	            return this;
   	        },

   	        /**
   	         * Finalizes the HMAC computation.
   	         * Note that the finalize operation is effectively a destructive, read-once operation.
   	         *
   	         * @param {WordArray|string} messageUpdate (Optional) A final message update.
   	         *
   	         * @return {WordArray} The HMAC.
   	         *
   	         * @example
   	         *
   	         *     var hmac = hmacHasher.finalize();
   	         *     var hmac = hmacHasher.finalize('message');
   	         *     var hmac = hmacHasher.finalize(wordArray);
   	         */
   	        finalize: function (messageUpdate) {
   	            // Shortcut
   	            var hasher = this._hasher;

   	            // Compute HMAC
   	            var innerHash = hasher.finalize(messageUpdate);
   	            hasher.reset();
   	            var hmac = hasher.finalize(this._oKey.clone().concat(innerHash));

   	            return hmac;
   	        }
   	    });
   	}());


   }));
   });

   var pbkdf2 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, sha1, hmac);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var Base = C_lib.Base;
   	    var WordArray = C_lib.WordArray;
   	    var C_algo = C.algo;
   	    var SHA1 = C_algo.SHA1;
   	    var HMAC = C_algo.HMAC;

   	    /**
   	     * Password-Based Key Derivation Function 2 algorithm.
   	     */
   	    var PBKDF2 = C_algo.PBKDF2 = Base.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {number} keySize The key size in words to generate. Default: 4 (128 bits)
   	         * @property {Hasher} hasher The hasher to use. Default: SHA1
   	         * @property {number} iterations The number of iterations to perform. Default: 1
   	         */
   	        cfg: Base.extend({
   	            keySize: 128/32,
   	            hasher: SHA1,
   	            iterations: 1
   	        }),

   	        /**
   	         * Initializes a newly created key derivation function.
   	         *
   	         * @param {Object} cfg (Optional) The configuration options to use for the derivation.
   	         *
   	         * @example
   	         *
   	         *     var kdf = CryptoJS.algo.PBKDF2.create();
   	         *     var kdf = CryptoJS.algo.PBKDF2.create({ keySize: 8 });
   	         *     var kdf = CryptoJS.algo.PBKDF2.create({ keySize: 8, iterations: 1000 });
   	         */
   	        init: function (cfg) {
   	            this.cfg = this.cfg.extend(cfg);
   	        },

   	        /**
   	         * Computes the Password-Based Key Derivation Function 2.
   	         *
   	         * @param {WordArray|string} password The password.
   	         * @param {WordArray|string} salt A salt.
   	         *
   	         * @return {WordArray} The derived key.
   	         *
   	         * @example
   	         *
   	         *     var key = kdf.compute(password, salt);
   	         */
   	        compute: function (password, salt) {
   	            // Shortcut
   	            var cfg = this.cfg;

   	            // Init HMAC
   	            var hmac = HMAC.create(cfg.hasher, password);

   	            // Initial values
   	            var derivedKey = WordArray.create();
   	            var blockIndex = WordArray.create([0x00000001]);

   	            // Shortcuts
   	            var derivedKeyWords = derivedKey.words;
   	            var blockIndexWords = blockIndex.words;
   	            var keySize = cfg.keySize;
   	            var iterations = cfg.iterations;

   	            // Generate key
   	            while (derivedKeyWords.length < keySize) {
   	                var block = hmac.update(salt).finalize(blockIndex);
   	                hmac.reset();

   	                // Shortcuts
   	                var blockWords = block.words;
   	                var blockWordsLength = blockWords.length;

   	                // Iterations
   	                var intermediate = block;
   	                for (var i = 1; i < iterations; i++) {
   	                    intermediate = hmac.finalize(intermediate);
   	                    hmac.reset();

   	                    // Shortcut
   	                    var intermediateWords = intermediate.words;

   	                    // XOR intermediate with block
   	                    for (var j = 0; j < blockWordsLength; j++) {
   	                        blockWords[j] ^= intermediateWords[j];
   	                    }
   	                }

   	                derivedKey.concat(block);
   	                blockIndexWords[0]++;
   	            }
   	            derivedKey.sigBytes = keySize * 4;

   	            return derivedKey;
   	        }
   	    });

   	    /**
   	     * Computes the Password-Based Key Derivation Function 2.
   	     *
   	     * @param {WordArray|string} password The password.
   	     * @param {WordArray|string} salt A salt.
   	     * @param {Object} cfg (Optional) The configuration options to use for this computation.
   	     *
   	     * @return {WordArray} The derived key.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var key = CryptoJS.PBKDF2(password, salt);
   	     *     var key = CryptoJS.PBKDF2(password, salt, { keySize: 8 });
   	     *     var key = CryptoJS.PBKDF2(password, salt, { keySize: 8, iterations: 1000 });
   	     */
   	    C.PBKDF2 = function (password, salt, cfg) {
   	        return PBKDF2.create(cfg).compute(password, salt);
   	    };
   	}());


   	return CryptoJS.PBKDF2;

   }));
   });

   var evpkdf = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, sha1, hmac);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var Base = C_lib.Base;
   	    var WordArray = C_lib.WordArray;
   	    var C_algo = C.algo;
   	    var MD5 = C_algo.MD5;

   	    /**
   	     * This key derivation function is meant to conform with EVP_BytesToKey.
   	     * www.openssl.org/docs/crypto/EVP_BytesToKey.html
   	     */
   	    var EvpKDF = C_algo.EvpKDF = Base.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {number} keySize The key size in words to generate. Default: 4 (128 bits)
   	         * @property {Hasher} hasher The hash algorithm to use. Default: MD5
   	         * @property {number} iterations The number of iterations to perform. Default: 1
   	         */
   	        cfg: Base.extend({
   	            keySize: 128/32,
   	            hasher: MD5,
   	            iterations: 1
   	        }),

   	        /**
   	         * Initializes a newly created key derivation function.
   	         *
   	         * @param {Object} cfg (Optional) The configuration options to use for the derivation.
   	         *
   	         * @example
   	         *
   	         *     var kdf = CryptoJS.algo.EvpKDF.create();
   	         *     var kdf = CryptoJS.algo.EvpKDF.create({ keySize: 8 });
   	         *     var kdf = CryptoJS.algo.EvpKDF.create({ keySize: 8, iterations: 1000 });
   	         */
   	        init: function (cfg) {
   	            this.cfg = this.cfg.extend(cfg);
   	        },

   	        /**
   	         * Derives a key from a password.
   	         *
   	         * @param {WordArray|string} password The password.
   	         * @param {WordArray|string} salt A salt.
   	         *
   	         * @return {WordArray} The derived key.
   	         *
   	         * @example
   	         *
   	         *     var key = kdf.compute(password, salt);
   	         */
   	        compute: function (password, salt) {
   	            var block;

   	            // Shortcut
   	            var cfg = this.cfg;

   	            // Init hasher
   	            var hasher = cfg.hasher.create();

   	            // Initial values
   	            var derivedKey = WordArray.create();

   	            // Shortcuts
   	            var derivedKeyWords = derivedKey.words;
   	            var keySize = cfg.keySize;
   	            var iterations = cfg.iterations;

   	            // Generate key
   	            while (derivedKeyWords.length < keySize) {
   	                if (block) {
   	                    hasher.update(block);
   	                }
   	                block = hasher.update(password).finalize(salt);
   	                hasher.reset();

   	                // Iterations
   	                for (var i = 1; i < iterations; i++) {
   	                    block = hasher.finalize(block);
   	                    hasher.reset();
   	                }

   	                derivedKey.concat(block);
   	            }
   	            derivedKey.sigBytes = keySize * 4;

   	            return derivedKey;
   	        }
   	    });

   	    /**
   	     * Derives a key from a password.
   	     *
   	     * @param {WordArray|string} password The password.
   	     * @param {WordArray|string} salt A salt.
   	     * @param {Object} cfg (Optional) The configuration options to use for this computation.
   	     *
   	     * @return {WordArray} The derived key.
   	     *
   	     * @static
   	     *
   	     * @example
   	     *
   	     *     var key = CryptoJS.EvpKDF(password, salt);
   	     *     var key = CryptoJS.EvpKDF(password, salt, { keySize: 8 });
   	     *     var key = CryptoJS.EvpKDF(password, salt, { keySize: 8, iterations: 1000 });
   	     */
   	    C.EvpKDF = function (password, salt, cfg) {
   	        return EvpKDF.create(cfg).compute(password, salt);
   	    };
   	}());


   	return CryptoJS.EvpKDF;

   }));
   });

   var cipherCore = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, evpkdf);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * Cipher core components.
   	 */
   	CryptoJS.lib.Cipher || (function (undefined$1) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var Base = C_lib.Base;
   	    var WordArray = C_lib.WordArray;
   	    var BufferedBlockAlgorithm = C_lib.BufferedBlockAlgorithm;
   	    var C_enc = C.enc;
   	    var Base64 = C_enc.Base64;
   	    var C_algo = C.algo;
   	    var EvpKDF = C_algo.EvpKDF;

   	    /**
   	     * Abstract base cipher template.
   	     *
   	     * @property {number} keySize This cipher's key size. Default: 4 (128 bits)
   	     * @property {number} ivSize This cipher's IV size. Default: 4 (128 bits)
   	     * @property {number} _ENC_XFORM_MODE A constant representing encryption mode.
   	     * @property {number} _DEC_XFORM_MODE A constant representing decryption mode.
   	     */
   	    var Cipher = C_lib.Cipher = BufferedBlockAlgorithm.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {WordArray} iv The IV to use for this operation.
   	         */
   	        cfg: Base.extend(),

   	        /**
   	         * Creates this cipher in encryption mode.
   	         *
   	         * @param {WordArray} key The key.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @return {Cipher} A cipher instance.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var cipher = CryptoJS.algo.AES.createEncryptor(keyWordArray, { iv: ivWordArray });
   	         */
   	        createEncryptor: function (key, cfg) {
   	            return this.create(this._ENC_XFORM_MODE, key, cfg);
   	        },

   	        /**
   	         * Creates this cipher in decryption mode.
   	         *
   	         * @param {WordArray} key The key.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @return {Cipher} A cipher instance.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var cipher = CryptoJS.algo.AES.createDecryptor(keyWordArray, { iv: ivWordArray });
   	         */
   	        createDecryptor: function (key, cfg) {
   	            return this.create(this._DEC_XFORM_MODE, key, cfg);
   	        },

   	        /**
   	         * Initializes a newly created cipher.
   	         *
   	         * @param {number} xformMode Either the encryption or decryption transormation mode constant.
   	         * @param {WordArray} key The key.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @example
   	         *
   	         *     var cipher = CryptoJS.algo.AES.create(CryptoJS.algo.AES._ENC_XFORM_MODE, keyWordArray, { iv: ivWordArray });
   	         */
   	        init: function (xformMode, key, cfg) {
   	            // Apply config defaults
   	            this.cfg = this.cfg.extend(cfg);

   	            // Store transform mode and key
   	            this._xformMode = xformMode;
   	            this._key = key;

   	            // Set initial values
   	            this.reset();
   	        },

   	        /**
   	         * Resets this cipher to its initial state.
   	         *
   	         * @example
   	         *
   	         *     cipher.reset();
   	         */
   	        reset: function () {
   	            // Reset data buffer
   	            BufferedBlockAlgorithm.reset.call(this);

   	            // Perform concrete-cipher logic
   	            this._doReset();
   	        },

   	        /**
   	         * Adds data to be encrypted or decrypted.
   	         *
   	         * @param {WordArray|string} dataUpdate The data to encrypt or decrypt.
   	         *
   	         * @return {WordArray} The data after processing.
   	         *
   	         * @example
   	         *
   	         *     var encrypted = cipher.process('data');
   	         *     var encrypted = cipher.process(wordArray);
   	         */
   	        process: function (dataUpdate) {
   	            // Append
   	            this._append(dataUpdate);

   	            // Process available blocks
   	            return this._process();
   	        },

   	        /**
   	         * Finalizes the encryption or decryption process.
   	         * Note that the finalize operation is effectively a destructive, read-once operation.
   	         *
   	         * @param {WordArray|string} dataUpdate The final data to encrypt or decrypt.
   	         *
   	         * @return {WordArray} The data after final processing.
   	         *
   	         * @example
   	         *
   	         *     var encrypted = cipher.finalize();
   	         *     var encrypted = cipher.finalize('data');
   	         *     var encrypted = cipher.finalize(wordArray);
   	         */
   	        finalize: function (dataUpdate) {
   	            // Final data update
   	            if (dataUpdate) {
   	                this._append(dataUpdate);
   	            }

   	            // Perform concrete-cipher logic
   	            var finalProcessedData = this._doFinalize();

   	            return finalProcessedData;
   	        },

   	        keySize: 128/32,

   	        ivSize: 128/32,

   	        _ENC_XFORM_MODE: 1,

   	        _DEC_XFORM_MODE: 2,

   	        /**
   	         * Creates shortcut functions to a cipher's object interface.
   	         *
   	         * @param {Cipher} cipher The cipher to create a helper for.
   	         *
   	         * @return {Object} An object with encrypt and decrypt shortcut functions.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var AES = CryptoJS.lib.Cipher._createHelper(CryptoJS.algo.AES);
   	         */
   	        _createHelper: (function () {
   	            function selectCipherStrategy(key) {
   	                if (typeof key == 'string') {
   	                    return PasswordBasedCipher;
   	                } else {
   	                    return SerializableCipher;
   	                }
   	            }

   	            return function (cipher) {
   	                return {
   	                    encrypt: function (message, key, cfg) {
   	                        return selectCipherStrategy(key).encrypt(cipher, message, key, cfg);
   	                    },

   	                    decrypt: function (ciphertext, key, cfg) {
   	                        return selectCipherStrategy(key).decrypt(cipher, ciphertext, key, cfg);
   	                    }
   	                };
   	            };
   	        }())
   	    });

   	    /**
   	     * Abstract base stream cipher template.
   	     *
   	     * @property {number} blockSize The number of 32-bit words this cipher operates on. Default: 1 (32 bits)
   	     */
   	    var StreamCipher = C_lib.StreamCipher = Cipher.extend({
   	        _doFinalize: function () {
   	            // Process partial blocks
   	            var finalProcessedBlocks = this._process(!!'flush');

   	            return finalProcessedBlocks;
   	        },

   	        blockSize: 1
   	    });

   	    /**
   	     * Mode namespace.
   	     */
   	    var C_mode = C.mode = {};

   	    /**
   	     * Abstract base block cipher mode template.
   	     */
   	    var BlockCipherMode = C_lib.BlockCipherMode = Base.extend({
   	        /**
   	         * Creates this mode for encryption.
   	         *
   	         * @param {Cipher} cipher A block cipher instance.
   	         * @param {Array} iv The IV words.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var mode = CryptoJS.mode.CBC.createEncryptor(cipher, iv.words);
   	         */
   	        createEncryptor: function (cipher, iv) {
   	            return this.Encryptor.create(cipher, iv);
   	        },

   	        /**
   	         * Creates this mode for decryption.
   	         *
   	         * @param {Cipher} cipher A block cipher instance.
   	         * @param {Array} iv The IV words.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var mode = CryptoJS.mode.CBC.createDecryptor(cipher, iv.words);
   	         */
   	        createDecryptor: function (cipher, iv) {
   	            return this.Decryptor.create(cipher, iv);
   	        },

   	        /**
   	         * Initializes a newly created mode.
   	         *
   	         * @param {Cipher} cipher A block cipher instance.
   	         * @param {Array} iv The IV words.
   	         *
   	         * @example
   	         *
   	         *     var mode = CryptoJS.mode.CBC.Encryptor.create(cipher, iv.words);
   	         */
   	        init: function (cipher, iv) {
   	            this._cipher = cipher;
   	            this._iv = iv;
   	        }
   	    });

   	    /**
   	     * Cipher Block Chaining mode.
   	     */
   	    var CBC = C_mode.CBC = (function () {
   	        /**
   	         * Abstract base CBC mode.
   	         */
   	        var CBC = BlockCipherMode.extend();

   	        /**
   	         * CBC encryptor.
   	         */
   	        CBC.Encryptor = CBC.extend({
   	            /**
   	             * Processes the data block at offset.
   	             *
   	             * @param {Array} words The data words to operate on.
   	             * @param {number} offset The offset where the block starts.
   	             *
   	             * @example
   	             *
   	             *     mode.processBlock(data.words, offset);
   	             */
   	            processBlock: function (words, offset) {
   	                // Shortcuts
   	                var cipher = this._cipher;
   	                var blockSize = cipher.blockSize;

   	                // XOR and encrypt
   	                xorBlock.call(this, words, offset, blockSize);
   	                cipher.encryptBlock(words, offset);

   	                // Remember this block to use with next block
   	                this._prevBlock = words.slice(offset, offset + blockSize);
   	            }
   	        });

   	        /**
   	         * CBC decryptor.
   	         */
   	        CBC.Decryptor = CBC.extend({
   	            /**
   	             * Processes the data block at offset.
   	             *
   	             * @param {Array} words The data words to operate on.
   	             * @param {number} offset The offset where the block starts.
   	             *
   	             * @example
   	             *
   	             *     mode.processBlock(data.words, offset);
   	             */
   	            processBlock: function (words, offset) {
   	                // Shortcuts
   	                var cipher = this._cipher;
   	                var blockSize = cipher.blockSize;

   	                // Remember this block to use with next block
   	                var thisBlock = words.slice(offset, offset + blockSize);

   	                // Decrypt and XOR
   	                cipher.decryptBlock(words, offset);
   	                xorBlock.call(this, words, offset, blockSize);

   	                // This block becomes the previous block
   	                this._prevBlock = thisBlock;
   	            }
   	        });

   	        function xorBlock(words, offset, blockSize) {
   	            var block;

   	            // Shortcut
   	            var iv = this._iv;

   	            // Choose mixing block
   	            if (iv) {
   	                block = iv;

   	                // Remove IV for subsequent blocks
   	                this._iv = undefined$1;
   	            } else {
   	                block = this._prevBlock;
   	            }

   	            // XOR blocks
   	            for (var i = 0; i < blockSize; i++) {
   	                words[offset + i] ^= block[i];
   	            }
   	        }

   	        return CBC;
   	    }());

   	    /**
   	     * Padding namespace.
   	     */
   	    var C_pad = C.pad = {};

   	    /**
   	     * PKCS #5/7 padding strategy.
   	     */
   	    var Pkcs7 = C_pad.Pkcs7 = {
   	        /**
   	         * Pads data using the algorithm defined in PKCS #5/7.
   	         *
   	         * @param {WordArray} data The data to pad.
   	         * @param {number} blockSize The multiple that the data should be padded to.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     CryptoJS.pad.Pkcs7.pad(wordArray, 4);
   	         */
   	        pad: function (data, blockSize) {
   	            // Shortcut
   	            var blockSizeBytes = blockSize * 4;

   	            // Count padding bytes
   	            var nPaddingBytes = blockSizeBytes - data.sigBytes % blockSizeBytes;

   	            // Create padding word
   	            var paddingWord = (nPaddingBytes << 24) | (nPaddingBytes << 16) | (nPaddingBytes << 8) | nPaddingBytes;

   	            // Create padding
   	            var paddingWords = [];
   	            for (var i = 0; i < nPaddingBytes; i += 4) {
   	                paddingWords.push(paddingWord);
   	            }
   	            var padding = WordArray.create(paddingWords, nPaddingBytes);

   	            // Add padding
   	            data.concat(padding);
   	        },

   	        /**
   	         * Unpads data that had been padded using the algorithm defined in PKCS #5/7.
   	         *
   	         * @param {WordArray} data The data to unpad.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     CryptoJS.pad.Pkcs7.unpad(wordArray);
   	         */
   	        unpad: function (data) {
   	            // Get number of padding bytes from last byte
   	            var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

   	            // Remove padding
   	            data.sigBytes -= nPaddingBytes;
   	        }
   	    };

   	    /**
   	     * Abstract base block cipher template.
   	     *
   	     * @property {number} blockSize The number of 32-bit words this cipher operates on. Default: 4 (128 bits)
   	     */
   	    var BlockCipher = C_lib.BlockCipher = Cipher.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {Mode} mode The block mode to use. Default: CBC
   	         * @property {Padding} padding The padding strategy to use. Default: Pkcs7
   	         */
   	        cfg: Cipher.cfg.extend({
   	            mode: CBC,
   	            padding: Pkcs7
   	        }),

   	        reset: function () {
   	            var modeCreator;

   	            // Reset cipher
   	            Cipher.reset.call(this);

   	            // Shortcuts
   	            var cfg = this.cfg;
   	            var iv = cfg.iv;
   	            var mode = cfg.mode;

   	            // Reset block mode
   	            if (this._xformMode == this._ENC_XFORM_MODE) {
   	                modeCreator = mode.createEncryptor;
   	            } else /* if (this._xformMode == this._DEC_XFORM_MODE) */ {
   	                modeCreator = mode.createDecryptor;
   	                // Keep at least one block in the buffer for unpadding
   	                this._minBufferSize = 1;
   	            }

   	            if (this._mode && this._mode.__creator == modeCreator) {
   	                this._mode.init(this, iv && iv.words);
   	            } else {
   	                this._mode = modeCreator.call(mode, this, iv && iv.words);
   	                this._mode.__creator = modeCreator;
   	            }
   	        },

   	        _doProcessBlock: function (words, offset) {
   	            this._mode.processBlock(words, offset);
   	        },

   	        _doFinalize: function () {
   	            var finalProcessedBlocks;

   	            // Shortcut
   	            var padding = this.cfg.padding;

   	            // Finalize
   	            if (this._xformMode == this._ENC_XFORM_MODE) {
   	                // Pad data
   	                padding.pad(this._data, this.blockSize);

   	                // Process final blocks
   	                finalProcessedBlocks = this._process(!!'flush');
   	            } else /* if (this._xformMode == this._DEC_XFORM_MODE) */ {
   	                // Process final blocks
   	                finalProcessedBlocks = this._process(!!'flush');

   	                // Unpad data
   	                padding.unpad(finalProcessedBlocks);
   	            }

   	            return finalProcessedBlocks;
   	        },

   	        blockSize: 128/32
   	    });

   	    /**
   	     * A collection of cipher parameters.
   	     *
   	     * @property {WordArray} ciphertext The raw ciphertext.
   	     * @property {WordArray} key The key to this ciphertext.
   	     * @property {WordArray} iv The IV used in the ciphering operation.
   	     * @property {WordArray} salt The salt used with a key derivation function.
   	     * @property {Cipher} algorithm The cipher algorithm.
   	     * @property {Mode} mode The block mode used in the ciphering operation.
   	     * @property {Padding} padding The padding scheme used in the ciphering operation.
   	     * @property {number} blockSize The block size of the cipher.
   	     * @property {Format} formatter The default formatting strategy to convert this cipher params object to a string.
   	     */
   	    var CipherParams = C_lib.CipherParams = Base.extend({
   	        /**
   	         * Initializes a newly created cipher params object.
   	         *
   	         * @param {Object} cipherParams An object with any of the possible cipher parameters.
   	         *
   	         * @example
   	         *
   	         *     var cipherParams = CryptoJS.lib.CipherParams.create({
   	         *         ciphertext: ciphertextWordArray,
   	         *         key: keyWordArray,
   	         *         iv: ivWordArray,
   	         *         salt: saltWordArray,
   	         *         algorithm: CryptoJS.algo.AES,
   	         *         mode: CryptoJS.mode.CBC,
   	         *         padding: CryptoJS.pad.PKCS7,
   	         *         blockSize: 4,
   	         *         formatter: CryptoJS.format.OpenSSL
   	         *     });
   	         */
   	        init: function (cipherParams) {
   	            this.mixIn(cipherParams);
   	        },

   	        /**
   	         * Converts this cipher params object to a string.
   	         *
   	         * @param {Format} formatter (Optional) The formatting strategy to use.
   	         *
   	         * @return {string} The stringified cipher params.
   	         *
   	         * @throws Error If neither the formatter nor the default formatter is set.
   	         *
   	         * @example
   	         *
   	         *     var string = cipherParams + '';
   	         *     var string = cipherParams.toString();
   	         *     var string = cipherParams.toString(CryptoJS.format.OpenSSL);
   	         */
   	        toString: function (formatter) {
   	            return (formatter || this.formatter).stringify(this);
   	        }
   	    });

   	    /**
   	     * Format namespace.
   	     */
   	    var C_format = C.format = {};

   	    /**
   	     * OpenSSL formatting strategy.
   	     */
   	    var OpenSSLFormatter = C_format.OpenSSL = {
   	        /**
   	         * Converts a cipher params object to an OpenSSL-compatible string.
   	         *
   	         * @param {CipherParams} cipherParams The cipher params object.
   	         *
   	         * @return {string} The OpenSSL-compatible string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var openSSLString = CryptoJS.format.OpenSSL.stringify(cipherParams);
   	         */
   	        stringify: function (cipherParams) {
   	            var wordArray;

   	            // Shortcuts
   	            var ciphertext = cipherParams.ciphertext;
   	            var salt = cipherParams.salt;

   	            // Format
   	            if (salt) {
   	                wordArray = WordArray.create([0x53616c74, 0x65645f5f]).concat(salt).concat(ciphertext);
   	            } else {
   	                wordArray = ciphertext;
   	            }

   	            return wordArray.toString(Base64);
   	        },

   	        /**
   	         * Converts an OpenSSL-compatible string to a cipher params object.
   	         *
   	         * @param {string} openSSLStr The OpenSSL-compatible string.
   	         *
   	         * @return {CipherParams} The cipher params object.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var cipherParams = CryptoJS.format.OpenSSL.parse(openSSLString);
   	         */
   	        parse: function (openSSLStr) {
   	            var salt;

   	            // Parse base64
   	            var ciphertext = Base64.parse(openSSLStr);

   	            // Shortcut
   	            var ciphertextWords = ciphertext.words;

   	            // Test for salt
   	            if (ciphertextWords[0] == 0x53616c74 && ciphertextWords[1] == 0x65645f5f) {
   	                // Extract salt
   	                salt = WordArray.create(ciphertextWords.slice(2, 4));

   	                // Remove salt from ciphertext
   	                ciphertextWords.splice(0, 4);
   	                ciphertext.sigBytes -= 16;
   	            }

   	            return CipherParams.create({ ciphertext: ciphertext, salt: salt });
   	        }
   	    };

   	    /**
   	     * A cipher wrapper that returns ciphertext as a serializable cipher params object.
   	     */
   	    var SerializableCipher = C_lib.SerializableCipher = Base.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {Formatter} format The formatting strategy to convert cipher param objects to and from a string. Default: OpenSSL
   	         */
   	        cfg: Base.extend({
   	            format: OpenSSLFormatter
   	        }),

   	        /**
   	         * Encrypts a message.
   	         *
   	         * @param {Cipher} cipher The cipher algorithm to use.
   	         * @param {WordArray|string} message The message to encrypt.
   	         * @param {WordArray} key The key.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @return {CipherParams} A cipher params object.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, message, key);
   	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, message, key, { iv: iv });
   	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, message, key, { iv: iv, format: CryptoJS.format.OpenSSL });
   	         */
   	        encrypt: function (cipher, message, key, cfg) {
   	            // Apply config defaults
   	            cfg = this.cfg.extend(cfg);

   	            // Encrypt
   	            var encryptor = cipher.createEncryptor(key, cfg);
   	            var ciphertext = encryptor.finalize(message);

   	            // Shortcut
   	            var cipherCfg = encryptor.cfg;

   	            // Create and return serializable cipher params
   	            return CipherParams.create({
   	                ciphertext: ciphertext,
   	                key: key,
   	                iv: cipherCfg.iv,
   	                algorithm: cipher,
   	                mode: cipherCfg.mode,
   	                padding: cipherCfg.padding,
   	                blockSize: cipher.blockSize,
   	                formatter: cfg.format
   	            });
   	        },

   	        /**
   	         * Decrypts serialized ciphertext.
   	         *
   	         * @param {Cipher} cipher The cipher algorithm to use.
   	         * @param {CipherParams|string} ciphertext The ciphertext to decrypt.
   	         * @param {WordArray} key The key.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @return {WordArray} The plaintext.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var plaintext = CryptoJS.lib.SerializableCipher.decrypt(CryptoJS.algo.AES, formattedCiphertext, key, { iv: iv, format: CryptoJS.format.OpenSSL });
   	         *     var plaintext = CryptoJS.lib.SerializableCipher.decrypt(CryptoJS.algo.AES, ciphertextParams, key, { iv: iv, format: CryptoJS.format.OpenSSL });
   	         */
   	        decrypt: function (cipher, ciphertext, key, cfg) {
   	            // Apply config defaults
   	            cfg = this.cfg.extend(cfg);

   	            // Convert string to CipherParams
   	            ciphertext = this._parse(ciphertext, cfg.format);

   	            // Decrypt
   	            var plaintext = cipher.createDecryptor(key, cfg).finalize(ciphertext.ciphertext);

   	            return plaintext;
   	        },

   	        /**
   	         * Converts serialized ciphertext to CipherParams,
   	         * else assumed CipherParams already and returns ciphertext unchanged.
   	         *
   	         * @param {CipherParams|string} ciphertext The ciphertext.
   	         * @param {Formatter} format The formatting strategy to use to parse serialized ciphertext.
   	         *
   	         * @return {CipherParams} The unserialized ciphertext.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var ciphertextParams = CryptoJS.lib.SerializableCipher._parse(ciphertextStringOrParams, format);
   	         */
   	        _parse: function (ciphertext, format) {
   	            if (typeof ciphertext == 'string') {
   	                return format.parse(ciphertext, this);
   	            } else {
   	                return ciphertext;
   	            }
   	        }
   	    });

   	    /**
   	     * Key derivation function namespace.
   	     */
   	    var C_kdf = C.kdf = {};

   	    /**
   	     * OpenSSL key derivation function.
   	     */
   	    var OpenSSLKdf = C_kdf.OpenSSL = {
   	        /**
   	         * Derives a key and IV from a password.
   	         *
   	         * @param {string} password The password to derive from.
   	         * @param {number} keySize The size in words of the key to generate.
   	         * @param {number} ivSize The size in words of the IV to generate.
   	         * @param {WordArray|string} salt (Optional) A 64-bit salt to use. If omitted, a salt will be generated randomly.
   	         *
   	         * @return {CipherParams} A cipher params object with the key, IV, and salt.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var derivedParams = CryptoJS.kdf.OpenSSL.execute('Password', 256/32, 128/32);
   	         *     var derivedParams = CryptoJS.kdf.OpenSSL.execute('Password', 256/32, 128/32, 'saltsalt');
   	         */
   	        execute: function (password, keySize, ivSize, salt) {
   	            // Generate random salt
   	            if (!salt) {
   	                salt = WordArray.random(64/8);
   	            }

   	            // Derive key and IV
   	            var key = EvpKDF.create({ keySize: keySize + ivSize }).compute(password, salt);

   	            // Separate key and IV
   	            var iv = WordArray.create(key.words.slice(keySize), ivSize * 4);
   	            key.sigBytes = keySize * 4;

   	            // Return params
   	            return CipherParams.create({ key: key, iv: iv, salt: salt });
   	        }
   	    };

   	    /**
   	     * A serializable cipher wrapper that derives the key from a password,
   	     * and returns ciphertext as a serializable cipher params object.
   	     */
   	    var PasswordBasedCipher = C_lib.PasswordBasedCipher = SerializableCipher.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {KDF} kdf The key derivation function to use to generate a key and IV from a password. Default: OpenSSL
   	         */
   	        cfg: SerializableCipher.cfg.extend({
   	            kdf: OpenSSLKdf
   	        }),

   	        /**
   	         * Encrypts a message using a password.
   	         *
   	         * @param {Cipher} cipher The cipher algorithm to use.
   	         * @param {WordArray|string} message The message to encrypt.
   	         * @param {string} password The password.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @return {CipherParams} A cipher params object.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var ciphertextParams = CryptoJS.lib.PasswordBasedCipher.encrypt(CryptoJS.algo.AES, message, 'password');
   	         *     var ciphertextParams = CryptoJS.lib.PasswordBasedCipher.encrypt(CryptoJS.algo.AES, message, 'password', { format: CryptoJS.format.OpenSSL });
   	         */
   	        encrypt: function (cipher, message, password, cfg) {
   	            // Apply config defaults
   	            cfg = this.cfg.extend(cfg);

   	            // Derive key and other params
   	            var derivedParams = cfg.kdf.execute(password, cipher.keySize, cipher.ivSize);

   	            // Add IV to config
   	            cfg.iv = derivedParams.iv;

   	            // Encrypt
   	            var ciphertext = SerializableCipher.encrypt.call(this, cipher, message, derivedParams.key, cfg);

   	            // Mix in derived params
   	            ciphertext.mixIn(derivedParams);

   	            return ciphertext;
   	        },

   	        /**
   	         * Decrypts serialized ciphertext using a password.
   	         *
   	         * @param {Cipher} cipher The cipher algorithm to use.
   	         * @param {CipherParams|string} ciphertext The ciphertext to decrypt.
   	         * @param {string} password The password.
   	         * @param {Object} cfg (Optional) The configuration options to use for this operation.
   	         *
   	         * @return {WordArray} The plaintext.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var plaintext = CryptoJS.lib.PasswordBasedCipher.decrypt(CryptoJS.algo.AES, formattedCiphertext, 'password', { format: CryptoJS.format.OpenSSL });
   	         *     var plaintext = CryptoJS.lib.PasswordBasedCipher.decrypt(CryptoJS.algo.AES, ciphertextParams, 'password', { format: CryptoJS.format.OpenSSL });
   	         */
   	        decrypt: function (cipher, ciphertext, password, cfg) {
   	            // Apply config defaults
   	            cfg = this.cfg.extend(cfg);

   	            // Convert string to CipherParams
   	            ciphertext = this._parse(ciphertext, cfg.format);

   	            // Derive key and other params
   	            var derivedParams = cfg.kdf.execute(password, cipher.keySize, cipher.ivSize, ciphertext.salt);

   	            // Add IV to config
   	            cfg.iv = derivedParams.iv;

   	            // Decrypt
   	            var plaintext = SerializableCipher.decrypt.call(this, cipher, ciphertext, derivedParams.key, cfg);

   	            return plaintext;
   	        }
   	    });
   	}());


   }));
   });

   var modeCfb = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * Cipher Feedback block mode.
   	 */
   	CryptoJS.mode.CFB = (function () {
   	    var CFB = CryptoJS.lib.BlockCipherMode.extend();

   	    CFB.Encryptor = CFB.extend({
   	        processBlock: function (words, offset) {
   	            // Shortcuts
   	            var cipher = this._cipher;
   	            var blockSize = cipher.blockSize;

   	            generateKeystreamAndEncrypt.call(this, words, offset, blockSize, cipher);

   	            // Remember this block to use with next block
   	            this._prevBlock = words.slice(offset, offset + blockSize);
   	        }
   	    });

   	    CFB.Decryptor = CFB.extend({
   	        processBlock: function (words, offset) {
   	            // Shortcuts
   	            var cipher = this._cipher;
   	            var blockSize = cipher.blockSize;

   	            // Remember this block to use with next block
   	            var thisBlock = words.slice(offset, offset + blockSize);

   	            generateKeystreamAndEncrypt.call(this, words, offset, blockSize, cipher);

   	            // This block becomes the previous block
   	            this._prevBlock = thisBlock;
   	        }
   	    });

   	    function generateKeystreamAndEncrypt(words, offset, blockSize, cipher) {
   	        var keystream;

   	        // Shortcut
   	        var iv = this._iv;

   	        // Generate keystream
   	        if (iv) {
   	            keystream = iv.slice(0);

   	            // Remove IV for subsequent blocks
   	            this._iv = undefined;
   	        } else {
   	            keystream = this._prevBlock;
   	        }
   	        cipher.encryptBlock(keystream, 0);

   	        // Encrypt
   	        for (var i = 0; i < blockSize; i++) {
   	            words[offset + i] ^= keystream[i];
   	        }
   	    }

   	    return CFB;
   	}());


   	return CryptoJS.mode.CFB;

   }));
   });

   var modeCtr = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * Counter block mode.
   	 */
   	CryptoJS.mode.CTR = (function () {
   	    var CTR = CryptoJS.lib.BlockCipherMode.extend();

   	    var Encryptor = CTR.Encryptor = CTR.extend({
   	        processBlock: function (words, offset) {
   	            // Shortcuts
   	            var cipher = this._cipher;
   	            var blockSize = cipher.blockSize;
   	            var iv = this._iv;
   	            var counter = this._counter;

   	            // Generate keystream
   	            if (iv) {
   	                counter = this._counter = iv.slice(0);

   	                // Remove IV for subsequent blocks
   	                this._iv = undefined;
   	            }
   	            var keystream = counter.slice(0);
   	            cipher.encryptBlock(keystream, 0);

   	            // Increment counter
   	            counter[blockSize - 1] = (counter[blockSize - 1] + 1) | 0;

   	            // Encrypt
   	            for (var i = 0; i < blockSize; i++) {
   	                words[offset + i] ^= keystream[i];
   	            }
   	        }
   	    });

   	    CTR.Decryptor = Encryptor;

   	    return CTR;
   	}());


   	return CryptoJS.mode.CTR;

   }));
   });

   var modeCtrGladman = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/** @preserve
   	 * Counter block mode compatible with  Dr Brian Gladman fileenc.c
   	 * derived from CryptoJS.mode.CTR
   	 * Jan Hruby jhruby.web@gmail.com
   	 */
   	CryptoJS.mode.CTRGladman = (function () {
   	    var CTRGladman = CryptoJS.lib.BlockCipherMode.extend();

   		function incWord(word)
   		{
   			if (((word >> 24) & 0xff) === 0xff) { //overflow
   			var b1 = (word >> 16)&0xff;
   			var b2 = (word >> 8)&0xff;
   			var b3 = word & 0xff;

   			if (b1 === 0xff) // overflow b1
   			{
   			b1 = 0;
   			if (b2 === 0xff)
   			{
   				b2 = 0;
   				if (b3 === 0xff)
   				{
   					b3 = 0;
   				}
   				else
   				{
   					++b3;
   				}
   			}
   			else
   			{
   				++b2;
   			}
   			}
   			else
   			{
   			++b1;
   			}

   			word = 0;
   			word += (b1 << 16);
   			word += (b2 << 8);
   			word += b3;
   			}
   			else
   			{
   			word += (0x01 << 24);
   			}
   			return word;
   		}

   		function incCounter(counter)
   		{
   			if ((counter[0] = incWord(counter[0])) === 0)
   			{
   				// encr_data in fileenc.c from  Dr Brian Gladman's counts only with DWORD j < 8
   				counter[1] = incWord(counter[1]);
   			}
   			return counter;
   		}

   	    var Encryptor = CTRGladman.Encryptor = CTRGladman.extend({
   	        processBlock: function (words, offset) {
   	            // Shortcuts
   	            var cipher = this._cipher;
   	            var blockSize = cipher.blockSize;
   	            var iv = this._iv;
   	            var counter = this._counter;

   	            // Generate keystream
   	            if (iv) {
   	                counter = this._counter = iv.slice(0);

   	                // Remove IV for subsequent blocks
   	                this._iv = undefined;
   	            }

   				incCounter(counter);

   				var keystream = counter.slice(0);
   	            cipher.encryptBlock(keystream, 0);

   	            // Encrypt
   	            for (var i = 0; i < blockSize; i++) {
   	                words[offset + i] ^= keystream[i];
   	            }
   	        }
   	    });

   	    CTRGladman.Decryptor = Encryptor;

   	    return CTRGladman;
   	}());




   	return CryptoJS.mode.CTRGladman;

   }));
   });

   var modeOfb = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * Output Feedback block mode.
   	 */
   	CryptoJS.mode.OFB = (function () {
   	    var OFB = CryptoJS.lib.BlockCipherMode.extend();

   	    var Encryptor = OFB.Encryptor = OFB.extend({
   	        processBlock: function (words, offset) {
   	            // Shortcuts
   	            var cipher = this._cipher;
   	            var blockSize = cipher.blockSize;
   	            var iv = this._iv;
   	            var keystream = this._keystream;

   	            // Generate keystream
   	            if (iv) {
   	                keystream = this._keystream = iv.slice(0);

   	                // Remove IV for subsequent blocks
   	                this._iv = undefined;
   	            }
   	            cipher.encryptBlock(keystream, 0);

   	            // Encrypt
   	            for (var i = 0; i < blockSize; i++) {
   	                words[offset + i] ^= keystream[i];
   	            }
   	        }
   	    });

   	    OFB.Decryptor = Encryptor;

   	    return OFB;
   	}());


   	return CryptoJS.mode.OFB;

   }));
   });

   var modeEcb = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * Electronic Codebook block mode.
   	 */
   	CryptoJS.mode.ECB = (function () {
   	    var ECB = CryptoJS.lib.BlockCipherMode.extend();

   	    ECB.Encryptor = ECB.extend({
   	        processBlock: function (words, offset) {
   	            this._cipher.encryptBlock(words, offset);
   	        }
   	    });

   	    ECB.Decryptor = ECB.extend({
   	        processBlock: function (words, offset) {
   	            this._cipher.decryptBlock(words, offset);
   	        }
   	    });

   	    return ECB;
   	}());


   	return CryptoJS.mode.ECB;

   }));
   });

   var padAnsix923 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * ANSI X.923 padding strategy.
   	 */
   	CryptoJS.pad.AnsiX923 = {
   	    pad: function (data, blockSize) {
   	        // Shortcuts
   	        var dataSigBytes = data.sigBytes;
   	        var blockSizeBytes = blockSize * 4;

   	        // Count padding bytes
   	        var nPaddingBytes = blockSizeBytes - dataSigBytes % blockSizeBytes;

   	        // Compute last byte position
   	        var lastBytePos = dataSigBytes + nPaddingBytes - 1;

   	        // Pad
   	        data.clamp();
   	        data.words[lastBytePos >>> 2] |= nPaddingBytes << (24 - (lastBytePos % 4) * 8);
   	        data.sigBytes += nPaddingBytes;
   	    },

   	    unpad: function (data) {
   	        // Get number of padding bytes from last byte
   	        var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

   	        // Remove padding
   	        data.sigBytes -= nPaddingBytes;
   	    }
   	};


   	return CryptoJS.pad.Ansix923;

   }));
   });

   var padIso10126 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * ISO 10126 padding strategy.
   	 */
   	CryptoJS.pad.Iso10126 = {
   	    pad: function (data, blockSize) {
   	        // Shortcut
   	        var blockSizeBytes = blockSize * 4;

   	        // Count padding bytes
   	        var nPaddingBytes = blockSizeBytes - data.sigBytes % blockSizeBytes;

   	        // Pad
   	        data.concat(CryptoJS.lib.WordArray.random(nPaddingBytes - 1)).
   	             concat(CryptoJS.lib.WordArray.create([nPaddingBytes << 24], 1));
   	    },

   	    unpad: function (data) {
   	        // Get number of padding bytes from last byte
   	        var nPaddingBytes = data.words[(data.sigBytes - 1) >>> 2] & 0xff;

   	        // Remove padding
   	        data.sigBytes -= nPaddingBytes;
   	    }
   	};


   	return CryptoJS.pad.Iso10126;

   }));
   });

   var padIso97971 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * ISO/IEC 9797-1 Padding Method 2.
   	 */
   	CryptoJS.pad.Iso97971 = {
   	    pad: function (data, blockSize) {
   	        // Add 0x80 byte
   	        data.concat(CryptoJS.lib.WordArray.create([0x80000000], 1));

   	        // Zero pad the rest
   	        CryptoJS.pad.ZeroPadding.pad(data, blockSize);
   	    },

   	    unpad: function (data) {
   	        // Remove zero padding
   	        CryptoJS.pad.ZeroPadding.unpad(data);

   	        // Remove one more byte -- the 0x80 byte
   	        data.sigBytes--;
   	    }
   	};


   	return CryptoJS.pad.Iso97971;

   }));
   });

   var padZeropadding = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * Zero padding strategy.
   	 */
   	CryptoJS.pad.ZeroPadding = {
   	    pad: function (data, blockSize) {
   	        // Shortcut
   	        var blockSizeBytes = blockSize * 4;

   	        // Pad
   	        data.clamp();
   	        data.sigBytes += blockSizeBytes - ((data.sigBytes % blockSizeBytes) || blockSizeBytes);
   	    },

   	    unpad: function (data) {
   	        // Shortcut
   	        var dataWords = data.words;

   	        // Unpad
   	        var i = data.sigBytes - 1;
   	        for (var i = data.sigBytes - 1; i >= 0; i--) {
   	            if (((dataWords[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff)) {
   	                data.sigBytes = i + 1;
   	                break;
   	            }
   	        }
   	    }
   	};


   	return CryptoJS.pad.ZeroPadding;

   }));
   });

   var padNopadding = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	/**
   	 * A noop padding strategy.
   	 */
   	CryptoJS.pad.NoPadding = {
   	    pad: function () {
   	    },

   	    unpad: function () {
   	    }
   	};


   	return CryptoJS.pad.NoPadding;

   }));
   });

   var formatHex = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function (undefined$1) {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var CipherParams = C_lib.CipherParams;
   	    var C_enc = C.enc;
   	    var Hex = C_enc.Hex;
   	    var C_format = C.format;

   	    var HexFormatter = C_format.Hex = {
   	        /**
   	         * Converts the ciphertext of a cipher params object to a hexadecimally encoded string.
   	         *
   	         * @param {CipherParams} cipherParams The cipher params object.
   	         *
   	         * @return {string} The hexadecimally encoded string.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var hexString = CryptoJS.format.Hex.stringify(cipherParams);
   	         */
   	        stringify: function (cipherParams) {
   	            return cipherParams.ciphertext.toString(Hex);
   	        },

   	        /**
   	         * Converts a hexadecimally encoded ciphertext string to a cipher params object.
   	         *
   	         * @param {string} input The hexadecimally encoded string.
   	         *
   	         * @return {CipherParams} The cipher params object.
   	         *
   	         * @static
   	         *
   	         * @example
   	         *
   	         *     var cipherParams = CryptoJS.format.Hex.parse(hexString);
   	         */
   	        parse: function (input) {
   	            var ciphertext = Hex.parse(input);
   	            return CipherParams.create({ ciphertext: ciphertext });
   	        }
   	    };
   	}());


   	return CryptoJS.format.Hex;

   }));
   });

   var aes = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, encBase64, md5, evpkdf, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var BlockCipher = C_lib.BlockCipher;
   	    var C_algo = C.algo;

   	    // Lookup tables
   	    var SBOX = [];
   	    var INV_SBOX = [];
   	    var SUB_MIX_0 = [];
   	    var SUB_MIX_1 = [];
   	    var SUB_MIX_2 = [];
   	    var SUB_MIX_3 = [];
   	    var INV_SUB_MIX_0 = [];
   	    var INV_SUB_MIX_1 = [];
   	    var INV_SUB_MIX_2 = [];
   	    var INV_SUB_MIX_3 = [];

   	    // Compute lookup tables
   	    (function () {
   	        // Compute double table
   	        var d = [];
   	        for (var i = 0; i < 256; i++) {
   	            if (i < 128) {
   	                d[i] = i << 1;
   	            } else {
   	                d[i] = (i << 1) ^ 0x11b;
   	            }
   	        }

   	        // Walk GF(2^8)
   	        var x = 0;
   	        var xi = 0;
   	        for (var i = 0; i < 256; i++) {
   	            // Compute sbox
   	            var sx = xi ^ (xi << 1) ^ (xi << 2) ^ (xi << 3) ^ (xi << 4);
   	            sx = (sx >>> 8) ^ (sx & 0xff) ^ 0x63;
   	            SBOX[x] = sx;
   	            INV_SBOX[sx] = x;

   	            // Compute multiplication
   	            var x2 = d[x];
   	            var x4 = d[x2];
   	            var x8 = d[x4];

   	            // Compute sub bytes, mix columns tables
   	            var t = (d[sx] * 0x101) ^ (sx * 0x1010100);
   	            SUB_MIX_0[x] = (t << 24) | (t >>> 8);
   	            SUB_MIX_1[x] = (t << 16) | (t >>> 16);
   	            SUB_MIX_2[x] = (t << 8)  | (t >>> 24);
   	            SUB_MIX_3[x] = t;

   	            // Compute inv sub bytes, inv mix columns tables
   	            var t = (x8 * 0x1010101) ^ (x4 * 0x10001) ^ (x2 * 0x101) ^ (x * 0x1010100);
   	            INV_SUB_MIX_0[sx] = (t << 24) | (t >>> 8);
   	            INV_SUB_MIX_1[sx] = (t << 16) | (t >>> 16);
   	            INV_SUB_MIX_2[sx] = (t << 8)  | (t >>> 24);
   	            INV_SUB_MIX_3[sx] = t;

   	            // Compute next counter
   	            if (!x) {
   	                x = xi = 1;
   	            } else {
   	                x = x2 ^ d[d[d[x8 ^ x2]]];
   	                xi ^= d[d[xi]];
   	            }
   	        }
   	    }());

   	    // Precomputed Rcon lookup
   	    var RCON = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36];

   	    /**
   	     * AES block cipher algorithm.
   	     */
   	    var AES = C_algo.AES = BlockCipher.extend({
   	        _doReset: function () {
   	            var t;

   	            // Skip reset of nRounds has been set before and key did not change
   	            if (this._nRounds && this._keyPriorReset === this._key) {
   	                return;
   	            }

   	            // Shortcuts
   	            var key = this._keyPriorReset = this._key;
   	            var keyWords = key.words;
   	            var keySize = key.sigBytes / 4;

   	            // Compute number of rounds
   	            var nRounds = this._nRounds = keySize + 6;

   	            // Compute number of key schedule rows
   	            var ksRows = (nRounds + 1) * 4;

   	            // Compute key schedule
   	            var keySchedule = this._keySchedule = [];
   	            for (var ksRow = 0; ksRow < ksRows; ksRow++) {
   	                if (ksRow < keySize) {
   	                    keySchedule[ksRow] = keyWords[ksRow];
   	                } else {
   	                    t = keySchedule[ksRow - 1];

   	                    if (!(ksRow % keySize)) {
   	                        // Rot word
   	                        t = (t << 8) | (t >>> 24);

   	                        // Sub word
   	                        t = (SBOX[t >>> 24] << 24) | (SBOX[(t >>> 16) & 0xff] << 16) | (SBOX[(t >>> 8) & 0xff] << 8) | SBOX[t & 0xff];

   	                        // Mix Rcon
   	                        t ^= RCON[(ksRow / keySize) | 0] << 24;
   	                    } else if (keySize > 6 && ksRow % keySize == 4) {
   	                        // Sub word
   	                        t = (SBOX[t >>> 24] << 24) | (SBOX[(t >>> 16) & 0xff] << 16) | (SBOX[(t >>> 8) & 0xff] << 8) | SBOX[t & 0xff];
   	                    }

   	                    keySchedule[ksRow] = keySchedule[ksRow - keySize] ^ t;
   	                }
   	            }

   	            // Compute inv key schedule
   	            var invKeySchedule = this._invKeySchedule = [];
   	            for (var invKsRow = 0; invKsRow < ksRows; invKsRow++) {
   	                var ksRow = ksRows - invKsRow;

   	                if (invKsRow % 4) {
   	                    var t = keySchedule[ksRow];
   	                } else {
   	                    var t = keySchedule[ksRow - 4];
   	                }

   	                if (invKsRow < 4 || ksRow <= 4) {
   	                    invKeySchedule[invKsRow] = t;
   	                } else {
   	                    invKeySchedule[invKsRow] = INV_SUB_MIX_0[SBOX[t >>> 24]] ^ INV_SUB_MIX_1[SBOX[(t >>> 16) & 0xff]] ^
   	                                               INV_SUB_MIX_2[SBOX[(t >>> 8) & 0xff]] ^ INV_SUB_MIX_3[SBOX[t & 0xff]];
   	                }
   	            }
   	        },

   	        encryptBlock: function (M, offset) {
   	            this._doCryptBlock(M, offset, this._keySchedule, SUB_MIX_0, SUB_MIX_1, SUB_MIX_2, SUB_MIX_3, SBOX);
   	        },

   	        decryptBlock: function (M, offset) {
   	            // Swap 2nd and 4th rows
   	            var t = M[offset + 1];
   	            M[offset + 1] = M[offset + 3];
   	            M[offset + 3] = t;

   	            this._doCryptBlock(M, offset, this._invKeySchedule, INV_SUB_MIX_0, INV_SUB_MIX_1, INV_SUB_MIX_2, INV_SUB_MIX_3, INV_SBOX);

   	            // Inv swap 2nd and 4th rows
   	            var t = M[offset + 1];
   	            M[offset + 1] = M[offset + 3];
   	            M[offset + 3] = t;
   	        },

   	        _doCryptBlock: function (M, offset, keySchedule, SUB_MIX_0, SUB_MIX_1, SUB_MIX_2, SUB_MIX_3, SBOX) {
   	            // Shortcut
   	            var nRounds = this._nRounds;

   	            // Get input, add round key
   	            var s0 = M[offset]     ^ keySchedule[0];
   	            var s1 = M[offset + 1] ^ keySchedule[1];
   	            var s2 = M[offset + 2] ^ keySchedule[2];
   	            var s3 = M[offset + 3] ^ keySchedule[3];

   	            // Key schedule row counter
   	            var ksRow = 4;

   	            // Rounds
   	            for (var round = 1; round < nRounds; round++) {
   	                // Shift rows, sub bytes, mix columns, add round key
   	                var t0 = SUB_MIX_0[s0 >>> 24] ^ SUB_MIX_1[(s1 >>> 16) & 0xff] ^ SUB_MIX_2[(s2 >>> 8) & 0xff] ^ SUB_MIX_3[s3 & 0xff] ^ keySchedule[ksRow++];
   	                var t1 = SUB_MIX_0[s1 >>> 24] ^ SUB_MIX_1[(s2 >>> 16) & 0xff] ^ SUB_MIX_2[(s3 >>> 8) & 0xff] ^ SUB_MIX_3[s0 & 0xff] ^ keySchedule[ksRow++];
   	                var t2 = SUB_MIX_0[s2 >>> 24] ^ SUB_MIX_1[(s3 >>> 16) & 0xff] ^ SUB_MIX_2[(s0 >>> 8) & 0xff] ^ SUB_MIX_3[s1 & 0xff] ^ keySchedule[ksRow++];
   	                var t3 = SUB_MIX_0[s3 >>> 24] ^ SUB_MIX_1[(s0 >>> 16) & 0xff] ^ SUB_MIX_2[(s1 >>> 8) & 0xff] ^ SUB_MIX_3[s2 & 0xff] ^ keySchedule[ksRow++];

   	                // Update state
   	                s0 = t0;
   	                s1 = t1;
   	                s2 = t2;
   	                s3 = t3;
   	            }

   	            // Shift rows, sub bytes, add round key
   	            var t0 = ((SBOX[s0 >>> 24] << 24) | (SBOX[(s1 >>> 16) & 0xff] << 16) | (SBOX[(s2 >>> 8) & 0xff] << 8) | SBOX[s3 & 0xff]) ^ keySchedule[ksRow++];
   	            var t1 = ((SBOX[s1 >>> 24] << 24) | (SBOX[(s2 >>> 16) & 0xff] << 16) | (SBOX[(s3 >>> 8) & 0xff] << 8) | SBOX[s0 & 0xff]) ^ keySchedule[ksRow++];
   	            var t2 = ((SBOX[s2 >>> 24] << 24) | (SBOX[(s3 >>> 16) & 0xff] << 16) | (SBOX[(s0 >>> 8) & 0xff] << 8) | SBOX[s1 & 0xff]) ^ keySchedule[ksRow++];
   	            var t3 = ((SBOX[s3 >>> 24] << 24) | (SBOX[(s0 >>> 16) & 0xff] << 16) | (SBOX[(s1 >>> 8) & 0xff] << 8) | SBOX[s2 & 0xff]) ^ keySchedule[ksRow++];

   	            // Set output
   	            M[offset]     = t0;
   	            M[offset + 1] = t1;
   	            M[offset + 2] = t2;
   	            M[offset + 3] = t3;
   	        },

   	        keySize: 256/32
   	    });

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.AES.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.AES.decrypt(ciphertext, key, cfg);
   	     */
   	    C.AES = BlockCipher._createHelper(AES);
   	}());


   	return CryptoJS.AES;

   }));
   });

   var tripledes = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, encBase64, md5, evpkdf, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var WordArray = C_lib.WordArray;
   	    var BlockCipher = C_lib.BlockCipher;
   	    var C_algo = C.algo;

   	    // Permuted Choice 1 constants
   	    var PC1 = [
   	        57, 49, 41, 33, 25, 17, 9,  1,
   	        58, 50, 42, 34, 26, 18, 10, 2,
   	        59, 51, 43, 35, 27, 19, 11, 3,
   	        60, 52, 44, 36, 63, 55, 47, 39,
   	        31, 23, 15, 7,  62, 54, 46, 38,
   	        30, 22, 14, 6,  61, 53, 45, 37,
   	        29, 21, 13, 5,  28, 20, 12, 4
   	    ];

   	    // Permuted Choice 2 constants
   	    var PC2 = [
   	        14, 17, 11, 24, 1,  5,
   	        3,  28, 15, 6,  21, 10,
   	        23, 19, 12, 4,  26, 8,
   	        16, 7,  27, 20, 13, 2,
   	        41, 52, 31, 37, 47, 55,
   	        30, 40, 51, 45, 33, 48,
   	        44, 49, 39, 56, 34, 53,
   	        46, 42, 50, 36, 29, 32
   	    ];

   	    // Cumulative bit shift constants
   	    var BIT_SHIFTS = [1,  2,  4,  6,  8,  10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28];

   	    // SBOXes and round permutation constants
   	    var SBOX_P = [
   	        {
   	            0x0: 0x808200,
   	            0x10000000: 0x8000,
   	            0x20000000: 0x808002,
   	            0x30000000: 0x2,
   	            0x40000000: 0x200,
   	            0x50000000: 0x808202,
   	            0x60000000: 0x800202,
   	            0x70000000: 0x800000,
   	            0x80000000: 0x202,
   	            0x90000000: 0x800200,
   	            0xa0000000: 0x8200,
   	            0xb0000000: 0x808000,
   	            0xc0000000: 0x8002,
   	            0xd0000000: 0x800002,
   	            0xe0000000: 0x0,
   	            0xf0000000: 0x8202,
   	            0x8000000: 0x0,
   	            0x18000000: 0x808202,
   	            0x28000000: 0x8202,
   	            0x38000000: 0x8000,
   	            0x48000000: 0x808200,
   	            0x58000000: 0x200,
   	            0x68000000: 0x808002,
   	            0x78000000: 0x2,
   	            0x88000000: 0x800200,
   	            0x98000000: 0x8200,
   	            0xa8000000: 0x808000,
   	            0xb8000000: 0x800202,
   	            0xc8000000: 0x800002,
   	            0xd8000000: 0x8002,
   	            0xe8000000: 0x202,
   	            0xf8000000: 0x800000,
   	            0x1: 0x8000,
   	            0x10000001: 0x2,
   	            0x20000001: 0x808200,
   	            0x30000001: 0x800000,
   	            0x40000001: 0x808002,
   	            0x50000001: 0x8200,
   	            0x60000001: 0x200,
   	            0x70000001: 0x800202,
   	            0x80000001: 0x808202,
   	            0x90000001: 0x808000,
   	            0xa0000001: 0x800002,
   	            0xb0000001: 0x8202,
   	            0xc0000001: 0x202,
   	            0xd0000001: 0x800200,
   	            0xe0000001: 0x8002,
   	            0xf0000001: 0x0,
   	            0x8000001: 0x808202,
   	            0x18000001: 0x808000,
   	            0x28000001: 0x800000,
   	            0x38000001: 0x200,
   	            0x48000001: 0x8000,
   	            0x58000001: 0x800002,
   	            0x68000001: 0x2,
   	            0x78000001: 0x8202,
   	            0x88000001: 0x8002,
   	            0x98000001: 0x800202,
   	            0xa8000001: 0x202,
   	            0xb8000001: 0x808200,
   	            0xc8000001: 0x800200,
   	            0xd8000001: 0x0,
   	            0xe8000001: 0x8200,
   	            0xf8000001: 0x808002
   	        },
   	        {
   	            0x0: 0x40084010,
   	            0x1000000: 0x4000,
   	            0x2000000: 0x80000,
   	            0x3000000: 0x40080010,
   	            0x4000000: 0x40000010,
   	            0x5000000: 0x40084000,
   	            0x6000000: 0x40004000,
   	            0x7000000: 0x10,
   	            0x8000000: 0x84000,
   	            0x9000000: 0x40004010,
   	            0xa000000: 0x40000000,
   	            0xb000000: 0x84010,
   	            0xc000000: 0x80010,
   	            0xd000000: 0x0,
   	            0xe000000: 0x4010,
   	            0xf000000: 0x40080000,
   	            0x800000: 0x40004000,
   	            0x1800000: 0x84010,
   	            0x2800000: 0x10,
   	            0x3800000: 0x40004010,
   	            0x4800000: 0x40084010,
   	            0x5800000: 0x40000000,
   	            0x6800000: 0x80000,
   	            0x7800000: 0x40080010,
   	            0x8800000: 0x80010,
   	            0x9800000: 0x0,
   	            0xa800000: 0x4000,
   	            0xb800000: 0x40080000,
   	            0xc800000: 0x40000010,
   	            0xd800000: 0x84000,
   	            0xe800000: 0x40084000,
   	            0xf800000: 0x4010,
   	            0x10000000: 0x0,
   	            0x11000000: 0x40080010,
   	            0x12000000: 0x40004010,
   	            0x13000000: 0x40084000,
   	            0x14000000: 0x40080000,
   	            0x15000000: 0x10,
   	            0x16000000: 0x84010,
   	            0x17000000: 0x4000,
   	            0x18000000: 0x4010,
   	            0x19000000: 0x80000,
   	            0x1a000000: 0x80010,
   	            0x1b000000: 0x40000010,
   	            0x1c000000: 0x84000,
   	            0x1d000000: 0x40004000,
   	            0x1e000000: 0x40000000,
   	            0x1f000000: 0x40084010,
   	            0x10800000: 0x84010,
   	            0x11800000: 0x80000,
   	            0x12800000: 0x40080000,
   	            0x13800000: 0x4000,
   	            0x14800000: 0x40004000,
   	            0x15800000: 0x40084010,
   	            0x16800000: 0x10,
   	            0x17800000: 0x40000000,
   	            0x18800000: 0x40084000,
   	            0x19800000: 0x40000010,
   	            0x1a800000: 0x40004010,
   	            0x1b800000: 0x80010,
   	            0x1c800000: 0x0,
   	            0x1d800000: 0x4010,
   	            0x1e800000: 0x40080010,
   	            0x1f800000: 0x84000
   	        },
   	        {
   	            0x0: 0x104,
   	            0x100000: 0x0,
   	            0x200000: 0x4000100,
   	            0x300000: 0x10104,
   	            0x400000: 0x10004,
   	            0x500000: 0x4000004,
   	            0x600000: 0x4010104,
   	            0x700000: 0x4010000,
   	            0x800000: 0x4000000,
   	            0x900000: 0x4010100,
   	            0xa00000: 0x10100,
   	            0xb00000: 0x4010004,
   	            0xc00000: 0x4000104,
   	            0xd00000: 0x10000,
   	            0xe00000: 0x4,
   	            0xf00000: 0x100,
   	            0x80000: 0x4010100,
   	            0x180000: 0x4010004,
   	            0x280000: 0x0,
   	            0x380000: 0x4000100,
   	            0x480000: 0x4000004,
   	            0x580000: 0x10000,
   	            0x680000: 0x10004,
   	            0x780000: 0x104,
   	            0x880000: 0x4,
   	            0x980000: 0x100,
   	            0xa80000: 0x4010000,
   	            0xb80000: 0x10104,
   	            0xc80000: 0x10100,
   	            0xd80000: 0x4000104,
   	            0xe80000: 0x4010104,
   	            0xf80000: 0x4000000,
   	            0x1000000: 0x4010100,
   	            0x1100000: 0x10004,
   	            0x1200000: 0x10000,
   	            0x1300000: 0x4000100,
   	            0x1400000: 0x100,
   	            0x1500000: 0x4010104,
   	            0x1600000: 0x4000004,
   	            0x1700000: 0x0,
   	            0x1800000: 0x4000104,
   	            0x1900000: 0x4000000,
   	            0x1a00000: 0x4,
   	            0x1b00000: 0x10100,
   	            0x1c00000: 0x4010000,
   	            0x1d00000: 0x104,
   	            0x1e00000: 0x10104,
   	            0x1f00000: 0x4010004,
   	            0x1080000: 0x4000000,
   	            0x1180000: 0x104,
   	            0x1280000: 0x4010100,
   	            0x1380000: 0x0,
   	            0x1480000: 0x10004,
   	            0x1580000: 0x4000100,
   	            0x1680000: 0x100,
   	            0x1780000: 0x4010004,
   	            0x1880000: 0x10000,
   	            0x1980000: 0x4010104,
   	            0x1a80000: 0x10104,
   	            0x1b80000: 0x4000004,
   	            0x1c80000: 0x4000104,
   	            0x1d80000: 0x4010000,
   	            0x1e80000: 0x4,
   	            0x1f80000: 0x10100
   	        },
   	        {
   	            0x0: 0x80401000,
   	            0x10000: 0x80001040,
   	            0x20000: 0x401040,
   	            0x30000: 0x80400000,
   	            0x40000: 0x0,
   	            0x50000: 0x401000,
   	            0x60000: 0x80000040,
   	            0x70000: 0x400040,
   	            0x80000: 0x80000000,
   	            0x90000: 0x400000,
   	            0xa0000: 0x40,
   	            0xb0000: 0x80001000,
   	            0xc0000: 0x80400040,
   	            0xd0000: 0x1040,
   	            0xe0000: 0x1000,
   	            0xf0000: 0x80401040,
   	            0x8000: 0x80001040,
   	            0x18000: 0x40,
   	            0x28000: 0x80400040,
   	            0x38000: 0x80001000,
   	            0x48000: 0x401000,
   	            0x58000: 0x80401040,
   	            0x68000: 0x0,
   	            0x78000: 0x80400000,
   	            0x88000: 0x1000,
   	            0x98000: 0x80401000,
   	            0xa8000: 0x400000,
   	            0xb8000: 0x1040,
   	            0xc8000: 0x80000000,
   	            0xd8000: 0x400040,
   	            0xe8000: 0x401040,
   	            0xf8000: 0x80000040,
   	            0x100000: 0x400040,
   	            0x110000: 0x401000,
   	            0x120000: 0x80000040,
   	            0x130000: 0x0,
   	            0x140000: 0x1040,
   	            0x150000: 0x80400040,
   	            0x160000: 0x80401000,
   	            0x170000: 0x80001040,
   	            0x180000: 0x80401040,
   	            0x190000: 0x80000000,
   	            0x1a0000: 0x80400000,
   	            0x1b0000: 0x401040,
   	            0x1c0000: 0x80001000,
   	            0x1d0000: 0x400000,
   	            0x1e0000: 0x40,
   	            0x1f0000: 0x1000,
   	            0x108000: 0x80400000,
   	            0x118000: 0x80401040,
   	            0x128000: 0x0,
   	            0x138000: 0x401000,
   	            0x148000: 0x400040,
   	            0x158000: 0x80000000,
   	            0x168000: 0x80001040,
   	            0x178000: 0x40,
   	            0x188000: 0x80000040,
   	            0x198000: 0x1000,
   	            0x1a8000: 0x80001000,
   	            0x1b8000: 0x80400040,
   	            0x1c8000: 0x1040,
   	            0x1d8000: 0x80401000,
   	            0x1e8000: 0x400000,
   	            0x1f8000: 0x401040
   	        },
   	        {
   	            0x0: 0x80,
   	            0x1000: 0x1040000,
   	            0x2000: 0x40000,
   	            0x3000: 0x20000000,
   	            0x4000: 0x20040080,
   	            0x5000: 0x1000080,
   	            0x6000: 0x21000080,
   	            0x7000: 0x40080,
   	            0x8000: 0x1000000,
   	            0x9000: 0x20040000,
   	            0xa000: 0x20000080,
   	            0xb000: 0x21040080,
   	            0xc000: 0x21040000,
   	            0xd000: 0x0,
   	            0xe000: 0x1040080,
   	            0xf000: 0x21000000,
   	            0x800: 0x1040080,
   	            0x1800: 0x21000080,
   	            0x2800: 0x80,
   	            0x3800: 0x1040000,
   	            0x4800: 0x40000,
   	            0x5800: 0x20040080,
   	            0x6800: 0x21040000,
   	            0x7800: 0x20000000,
   	            0x8800: 0x20040000,
   	            0x9800: 0x0,
   	            0xa800: 0x21040080,
   	            0xb800: 0x1000080,
   	            0xc800: 0x20000080,
   	            0xd800: 0x21000000,
   	            0xe800: 0x1000000,
   	            0xf800: 0x40080,
   	            0x10000: 0x40000,
   	            0x11000: 0x80,
   	            0x12000: 0x20000000,
   	            0x13000: 0x21000080,
   	            0x14000: 0x1000080,
   	            0x15000: 0x21040000,
   	            0x16000: 0x20040080,
   	            0x17000: 0x1000000,
   	            0x18000: 0x21040080,
   	            0x19000: 0x21000000,
   	            0x1a000: 0x1040000,
   	            0x1b000: 0x20040000,
   	            0x1c000: 0x40080,
   	            0x1d000: 0x20000080,
   	            0x1e000: 0x0,
   	            0x1f000: 0x1040080,
   	            0x10800: 0x21000080,
   	            0x11800: 0x1000000,
   	            0x12800: 0x1040000,
   	            0x13800: 0x20040080,
   	            0x14800: 0x20000000,
   	            0x15800: 0x1040080,
   	            0x16800: 0x80,
   	            0x17800: 0x21040000,
   	            0x18800: 0x40080,
   	            0x19800: 0x21040080,
   	            0x1a800: 0x0,
   	            0x1b800: 0x21000000,
   	            0x1c800: 0x1000080,
   	            0x1d800: 0x40000,
   	            0x1e800: 0x20040000,
   	            0x1f800: 0x20000080
   	        },
   	        {
   	            0x0: 0x10000008,
   	            0x100: 0x2000,
   	            0x200: 0x10200000,
   	            0x300: 0x10202008,
   	            0x400: 0x10002000,
   	            0x500: 0x200000,
   	            0x600: 0x200008,
   	            0x700: 0x10000000,
   	            0x800: 0x0,
   	            0x900: 0x10002008,
   	            0xa00: 0x202000,
   	            0xb00: 0x8,
   	            0xc00: 0x10200008,
   	            0xd00: 0x202008,
   	            0xe00: 0x2008,
   	            0xf00: 0x10202000,
   	            0x80: 0x10200000,
   	            0x180: 0x10202008,
   	            0x280: 0x8,
   	            0x380: 0x200000,
   	            0x480: 0x202008,
   	            0x580: 0x10000008,
   	            0x680: 0x10002000,
   	            0x780: 0x2008,
   	            0x880: 0x200008,
   	            0x980: 0x2000,
   	            0xa80: 0x10002008,
   	            0xb80: 0x10200008,
   	            0xc80: 0x0,
   	            0xd80: 0x10202000,
   	            0xe80: 0x202000,
   	            0xf80: 0x10000000,
   	            0x1000: 0x10002000,
   	            0x1100: 0x10200008,
   	            0x1200: 0x10202008,
   	            0x1300: 0x2008,
   	            0x1400: 0x200000,
   	            0x1500: 0x10000000,
   	            0x1600: 0x10000008,
   	            0x1700: 0x202000,
   	            0x1800: 0x202008,
   	            0x1900: 0x0,
   	            0x1a00: 0x8,
   	            0x1b00: 0x10200000,
   	            0x1c00: 0x2000,
   	            0x1d00: 0x10002008,
   	            0x1e00: 0x10202000,
   	            0x1f00: 0x200008,
   	            0x1080: 0x8,
   	            0x1180: 0x202000,
   	            0x1280: 0x200000,
   	            0x1380: 0x10000008,
   	            0x1480: 0x10002000,
   	            0x1580: 0x2008,
   	            0x1680: 0x10202008,
   	            0x1780: 0x10200000,
   	            0x1880: 0x10202000,
   	            0x1980: 0x10200008,
   	            0x1a80: 0x2000,
   	            0x1b80: 0x202008,
   	            0x1c80: 0x200008,
   	            0x1d80: 0x0,
   	            0x1e80: 0x10000000,
   	            0x1f80: 0x10002008
   	        },
   	        {
   	            0x0: 0x100000,
   	            0x10: 0x2000401,
   	            0x20: 0x400,
   	            0x30: 0x100401,
   	            0x40: 0x2100401,
   	            0x50: 0x0,
   	            0x60: 0x1,
   	            0x70: 0x2100001,
   	            0x80: 0x2000400,
   	            0x90: 0x100001,
   	            0xa0: 0x2000001,
   	            0xb0: 0x2100400,
   	            0xc0: 0x2100000,
   	            0xd0: 0x401,
   	            0xe0: 0x100400,
   	            0xf0: 0x2000000,
   	            0x8: 0x2100001,
   	            0x18: 0x0,
   	            0x28: 0x2000401,
   	            0x38: 0x2100400,
   	            0x48: 0x100000,
   	            0x58: 0x2000001,
   	            0x68: 0x2000000,
   	            0x78: 0x401,
   	            0x88: 0x100401,
   	            0x98: 0x2000400,
   	            0xa8: 0x2100000,
   	            0xb8: 0x100001,
   	            0xc8: 0x400,
   	            0xd8: 0x2100401,
   	            0xe8: 0x1,
   	            0xf8: 0x100400,
   	            0x100: 0x2000000,
   	            0x110: 0x100000,
   	            0x120: 0x2000401,
   	            0x130: 0x2100001,
   	            0x140: 0x100001,
   	            0x150: 0x2000400,
   	            0x160: 0x2100400,
   	            0x170: 0x100401,
   	            0x180: 0x401,
   	            0x190: 0x2100401,
   	            0x1a0: 0x100400,
   	            0x1b0: 0x1,
   	            0x1c0: 0x0,
   	            0x1d0: 0x2100000,
   	            0x1e0: 0x2000001,
   	            0x1f0: 0x400,
   	            0x108: 0x100400,
   	            0x118: 0x2000401,
   	            0x128: 0x2100001,
   	            0x138: 0x1,
   	            0x148: 0x2000000,
   	            0x158: 0x100000,
   	            0x168: 0x401,
   	            0x178: 0x2100400,
   	            0x188: 0x2000001,
   	            0x198: 0x2100000,
   	            0x1a8: 0x0,
   	            0x1b8: 0x2100401,
   	            0x1c8: 0x100401,
   	            0x1d8: 0x400,
   	            0x1e8: 0x2000400,
   	            0x1f8: 0x100001
   	        },
   	        {
   	            0x0: 0x8000820,
   	            0x1: 0x20000,
   	            0x2: 0x8000000,
   	            0x3: 0x20,
   	            0x4: 0x20020,
   	            0x5: 0x8020820,
   	            0x6: 0x8020800,
   	            0x7: 0x800,
   	            0x8: 0x8020000,
   	            0x9: 0x8000800,
   	            0xa: 0x20800,
   	            0xb: 0x8020020,
   	            0xc: 0x820,
   	            0xd: 0x0,
   	            0xe: 0x8000020,
   	            0xf: 0x20820,
   	            0x80000000: 0x800,
   	            0x80000001: 0x8020820,
   	            0x80000002: 0x8000820,
   	            0x80000003: 0x8000000,
   	            0x80000004: 0x8020000,
   	            0x80000005: 0x20800,
   	            0x80000006: 0x20820,
   	            0x80000007: 0x20,
   	            0x80000008: 0x8000020,
   	            0x80000009: 0x820,
   	            0x8000000a: 0x20020,
   	            0x8000000b: 0x8020800,
   	            0x8000000c: 0x0,
   	            0x8000000d: 0x8020020,
   	            0x8000000e: 0x8000800,
   	            0x8000000f: 0x20000,
   	            0x10: 0x20820,
   	            0x11: 0x8020800,
   	            0x12: 0x20,
   	            0x13: 0x800,
   	            0x14: 0x8000800,
   	            0x15: 0x8000020,
   	            0x16: 0x8020020,
   	            0x17: 0x20000,
   	            0x18: 0x0,
   	            0x19: 0x20020,
   	            0x1a: 0x8020000,
   	            0x1b: 0x8000820,
   	            0x1c: 0x8020820,
   	            0x1d: 0x20800,
   	            0x1e: 0x820,
   	            0x1f: 0x8000000,
   	            0x80000010: 0x20000,
   	            0x80000011: 0x800,
   	            0x80000012: 0x8020020,
   	            0x80000013: 0x20820,
   	            0x80000014: 0x20,
   	            0x80000015: 0x8020000,
   	            0x80000016: 0x8000000,
   	            0x80000017: 0x8000820,
   	            0x80000018: 0x8020820,
   	            0x80000019: 0x8000020,
   	            0x8000001a: 0x8000800,
   	            0x8000001b: 0x0,
   	            0x8000001c: 0x20800,
   	            0x8000001d: 0x820,
   	            0x8000001e: 0x20020,
   	            0x8000001f: 0x8020800
   	        }
   	    ];

   	    // Masks that select the SBOX input
   	    var SBOX_MASK = [
   	        0xf8000001, 0x1f800000, 0x01f80000, 0x001f8000,
   	        0x0001f800, 0x00001f80, 0x000001f8, 0x8000001f
   	    ];

   	    /**
   	     * DES block cipher algorithm.
   	     */
   	    var DES = C_algo.DES = BlockCipher.extend({
   	        _doReset: function () {
   	            // Shortcuts
   	            var key = this._key;
   	            var keyWords = key.words;

   	            // Select 56 bits according to PC1
   	            var keyBits = [];
   	            for (var i = 0; i < 56; i++) {
   	                var keyBitPos = PC1[i] - 1;
   	                keyBits[i] = (keyWords[keyBitPos >>> 5] >>> (31 - keyBitPos % 32)) & 1;
   	            }

   	            // Assemble 16 subkeys
   	            var subKeys = this._subKeys = [];
   	            for (var nSubKey = 0; nSubKey < 16; nSubKey++) {
   	                // Create subkey
   	                var subKey = subKeys[nSubKey] = [];

   	                // Shortcut
   	                var bitShift = BIT_SHIFTS[nSubKey];

   	                // Select 48 bits according to PC2
   	                for (var i = 0; i < 24; i++) {
   	                    // Select from the left 28 key bits
   	                    subKey[(i / 6) | 0] |= keyBits[((PC2[i] - 1) + bitShift) % 28] << (31 - i % 6);

   	                    // Select from the right 28 key bits
   	                    subKey[4 + ((i / 6) | 0)] |= keyBits[28 + (((PC2[i + 24] - 1) + bitShift) % 28)] << (31 - i % 6);
   	                }

   	                // Since each subkey is applied to an expanded 32-bit input,
   	                // the subkey can be broken into 8 values scaled to 32-bits,
   	                // which allows the key to be used without expansion
   	                subKey[0] = (subKey[0] << 1) | (subKey[0] >>> 31);
   	                for (var i = 1; i < 7; i++) {
   	                    subKey[i] = subKey[i] >>> ((i - 1) * 4 + 3);
   	                }
   	                subKey[7] = (subKey[7] << 5) | (subKey[7] >>> 27);
   	            }

   	            // Compute inverse subkeys
   	            var invSubKeys = this._invSubKeys = [];
   	            for (var i = 0; i < 16; i++) {
   	                invSubKeys[i] = subKeys[15 - i];
   	            }
   	        },

   	        encryptBlock: function (M, offset) {
   	            this._doCryptBlock(M, offset, this._subKeys);
   	        },

   	        decryptBlock: function (M, offset) {
   	            this._doCryptBlock(M, offset, this._invSubKeys);
   	        },

   	        _doCryptBlock: function (M, offset, subKeys) {
   	            // Get input
   	            this._lBlock = M[offset];
   	            this._rBlock = M[offset + 1];

   	            // Initial permutation
   	            exchangeLR.call(this, 4,  0x0f0f0f0f);
   	            exchangeLR.call(this, 16, 0x0000ffff);
   	            exchangeRL.call(this, 2,  0x33333333);
   	            exchangeRL.call(this, 8,  0x00ff00ff);
   	            exchangeLR.call(this, 1,  0x55555555);

   	            // Rounds
   	            for (var round = 0; round < 16; round++) {
   	                // Shortcuts
   	                var subKey = subKeys[round];
   	                var lBlock = this._lBlock;
   	                var rBlock = this._rBlock;

   	                // Feistel function
   	                var f = 0;
   	                for (var i = 0; i < 8; i++) {
   	                    f |= SBOX_P[i][((rBlock ^ subKey[i]) & SBOX_MASK[i]) >>> 0];
   	                }
   	                this._lBlock = rBlock;
   	                this._rBlock = lBlock ^ f;
   	            }

   	            // Undo swap from last round
   	            var t = this._lBlock;
   	            this._lBlock = this._rBlock;
   	            this._rBlock = t;

   	            // Final permutation
   	            exchangeLR.call(this, 1,  0x55555555);
   	            exchangeRL.call(this, 8,  0x00ff00ff);
   	            exchangeRL.call(this, 2,  0x33333333);
   	            exchangeLR.call(this, 16, 0x0000ffff);
   	            exchangeLR.call(this, 4,  0x0f0f0f0f);

   	            // Set output
   	            M[offset] = this._lBlock;
   	            M[offset + 1] = this._rBlock;
   	        },

   	        keySize: 64/32,

   	        ivSize: 64/32,

   	        blockSize: 64/32
   	    });

   	    // Swap bits across the left and right words
   	    function exchangeLR(offset, mask) {
   	        var t = ((this._lBlock >>> offset) ^ this._rBlock) & mask;
   	        this._rBlock ^= t;
   	        this._lBlock ^= t << offset;
   	    }

   	    function exchangeRL(offset, mask) {
   	        var t = ((this._rBlock >>> offset) ^ this._lBlock) & mask;
   	        this._lBlock ^= t;
   	        this._rBlock ^= t << offset;
   	    }

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.DES.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.DES.decrypt(ciphertext, key, cfg);
   	     */
   	    C.DES = BlockCipher._createHelper(DES);

   	    /**
   	     * Triple-DES block cipher algorithm.
   	     */
   	    var TripleDES = C_algo.TripleDES = BlockCipher.extend({
   	        _doReset: function () {
   	            // Shortcuts
   	            var key = this._key;
   	            var keyWords = key.words;
   	            // Make sure the key length is valid (64, 128 or >= 192 bit)
   	            if (keyWords.length !== 2 && keyWords.length !== 4 && keyWords.length < 6) {
   	                throw new Error('Invalid key length - 3DES requires the key length to be 64, 128, 192 or >192.');
   	            }

   	            // Extend the key according to the keying options defined in 3DES standard
   	            var key1 = keyWords.slice(0, 2);
   	            var key2 = keyWords.length < 4 ? keyWords.slice(0, 2) : keyWords.slice(2, 4);
   	            var key3 = keyWords.length < 6 ? keyWords.slice(0, 2) : keyWords.slice(4, 6);

   	            // Create DES instances
   	            this._des1 = DES.createEncryptor(WordArray.create(key1));
   	            this._des2 = DES.createEncryptor(WordArray.create(key2));
   	            this._des3 = DES.createEncryptor(WordArray.create(key3));
   	        },

   	        encryptBlock: function (M, offset) {
   	            this._des1.encryptBlock(M, offset);
   	            this._des2.decryptBlock(M, offset);
   	            this._des3.encryptBlock(M, offset);
   	        },

   	        decryptBlock: function (M, offset) {
   	            this._des3.decryptBlock(M, offset);
   	            this._des2.encryptBlock(M, offset);
   	            this._des1.decryptBlock(M, offset);
   	        },

   	        keySize: 192/32,

   	        ivSize: 64/32,

   	        blockSize: 64/32
   	    });

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.TripleDES.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.TripleDES.decrypt(ciphertext, key, cfg);
   	     */
   	    C.TripleDES = BlockCipher._createHelper(TripleDES);
   	}());


   	return CryptoJS.TripleDES;

   }));
   });

   var rc4 = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, encBase64, md5, evpkdf, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var StreamCipher = C_lib.StreamCipher;
   	    var C_algo = C.algo;

   	    /**
   	     * RC4 stream cipher algorithm.
   	     */
   	    var RC4 = C_algo.RC4 = StreamCipher.extend({
   	        _doReset: function () {
   	            // Shortcuts
   	            var key = this._key;
   	            var keyWords = key.words;
   	            var keySigBytes = key.sigBytes;

   	            // Init sbox
   	            var S = this._S = [];
   	            for (var i = 0; i < 256; i++) {
   	                S[i] = i;
   	            }

   	            // Key setup
   	            for (var i = 0, j = 0; i < 256; i++) {
   	                var keyByteIndex = i % keySigBytes;
   	                var keyByte = (keyWords[keyByteIndex >>> 2] >>> (24 - (keyByteIndex % 4) * 8)) & 0xff;

   	                j = (j + S[i] + keyByte) % 256;

   	                // Swap
   	                var t = S[i];
   	                S[i] = S[j];
   	                S[j] = t;
   	            }

   	            // Counters
   	            this._i = this._j = 0;
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            M[offset] ^= generateKeystreamWord.call(this);
   	        },

   	        keySize: 256/32,

   	        ivSize: 0
   	    });

   	    function generateKeystreamWord() {
   	        // Shortcuts
   	        var S = this._S;
   	        var i = this._i;
   	        var j = this._j;

   	        // Generate keystream word
   	        var keystreamWord = 0;
   	        for (var n = 0; n < 4; n++) {
   	            i = (i + 1) % 256;
   	            j = (j + S[i]) % 256;

   	            // Swap
   	            var t = S[i];
   	            S[i] = S[j];
   	            S[j] = t;

   	            keystreamWord |= S[(S[i] + S[j]) % 256] << (24 - n * 8);
   	        }

   	        // Update counters
   	        this._i = i;
   	        this._j = j;

   	        return keystreamWord;
   	    }

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.RC4.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.RC4.decrypt(ciphertext, key, cfg);
   	     */
   	    C.RC4 = StreamCipher._createHelper(RC4);

   	    /**
   	     * Modified RC4 stream cipher algorithm.
   	     */
   	    var RC4Drop = C_algo.RC4Drop = RC4.extend({
   	        /**
   	         * Configuration options.
   	         *
   	         * @property {number} drop The number of keystream words to drop. Default 192
   	         */
   	        cfg: RC4.cfg.extend({
   	            drop: 192
   	        }),

   	        _doReset: function () {
   	            RC4._doReset.call(this);

   	            // Drop
   	            for (var i = this.cfg.drop; i > 0; i--) {
   	                generateKeystreamWord.call(this);
   	            }
   	        }
   	    });

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.RC4Drop.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.RC4Drop.decrypt(ciphertext, key, cfg);
   	     */
   	    C.RC4Drop = StreamCipher._createHelper(RC4Drop);
   	}());


   	return CryptoJS.RC4;

   }));
   });

   var rabbit = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, encBase64, md5, evpkdf, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var StreamCipher = C_lib.StreamCipher;
   	    var C_algo = C.algo;

   	    // Reusable objects
   	    var S  = [];
   	    var C_ = [];
   	    var G  = [];

   	    /**
   	     * Rabbit stream cipher algorithm
   	     */
   	    var Rabbit = C_algo.Rabbit = StreamCipher.extend({
   	        _doReset: function () {
   	            // Shortcuts
   	            var K = this._key.words;
   	            var iv = this.cfg.iv;

   	            // Swap endian
   	            for (var i = 0; i < 4; i++) {
   	                K[i] = (((K[i] << 8)  | (K[i] >>> 24)) & 0x00ff00ff) |
   	                       (((K[i] << 24) | (K[i] >>> 8))  & 0xff00ff00);
   	            }

   	            // Generate initial state values
   	            var X = this._X = [
   	                K[0], (K[3] << 16) | (K[2] >>> 16),
   	                K[1], (K[0] << 16) | (K[3] >>> 16),
   	                K[2], (K[1] << 16) | (K[0] >>> 16),
   	                K[3], (K[2] << 16) | (K[1] >>> 16)
   	            ];

   	            // Generate initial counter values
   	            var C = this._C = [
   	                (K[2] << 16) | (K[2] >>> 16), (K[0] & 0xffff0000) | (K[1] & 0x0000ffff),
   	                (K[3] << 16) | (K[3] >>> 16), (K[1] & 0xffff0000) | (K[2] & 0x0000ffff),
   	                (K[0] << 16) | (K[0] >>> 16), (K[2] & 0xffff0000) | (K[3] & 0x0000ffff),
   	                (K[1] << 16) | (K[1] >>> 16), (K[3] & 0xffff0000) | (K[0] & 0x0000ffff)
   	            ];

   	            // Carry bit
   	            this._b = 0;

   	            // Iterate the system four times
   	            for (var i = 0; i < 4; i++) {
   	                nextState.call(this);
   	            }

   	            // Modify the counters
   	            for (var i = 0; i < 8; i++) {
   	                C[i] ^= X[(i + 4) & 7];
   	            }

   	            // IV setup
   	            if (iv) {
   	                // Shortcuts
   	                var IV = iv.words;
   	                var IV_0 = IV[0];
   	                var IV_1 = IV[1];

   	                // Generate four subvectors
   	                var i0 = (((IV_0 << 8) | (IV_0 >>> 24)) & 0x00ff00ff) | (((IV_0 << 24) | (IV_0 >>> 8)) & 0xff00ff00);
   	                var i2 = (((IV_1 << 8) | (IV_1 >>> 24)) & 0x00ff00ff) | (((IV_1 << 24) | (IV_1 >>> 8)) & 0xff00ff00);
   	                var i1 = (i0 >>> 16) | (i2 & 0xffff0000);
   	                var i3 = (i2 << 16)  | (i0 & 0x0000ffff);

   	                // Modify counter values
   	                C[0] ^= i0;
   	                C[1] ^= i1;
   	                C[2] ^= i2;
   	                C[3] ^= i3;
   	                C[4] ^= i0;
   	                C[5] ^= i1;
   	                C[6] ^= i2;
   	                C[7] ^= i3;

   	                // Iterate the system four times
   	                for (var i = 0; i < 4; i++) {
   	                    nextState.call(this);
   	                }
   	            }
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Shortcut
   	            var X = this._X;

   	            // Iterate the system
   	            nextState.call(this);

   	            // Generate four keystream words
   	            S[0] = X[0] ^ (X[5] >>> 16) ^ (X[3] << 16);
   	            S[1] = X[2] ^ (X[7] >>> 16) ^ (X[5] << 16);
   	            S[2] = X[4] ^ (X[1] >>> 16) ^ (X[7] << 16);
   	            S[3] = X[6] ^ (X[3] >>> 16) ^ (X[1] << 16);

   	            for (var i = 0; i < 4; i++) {
   	                // Swap endian
   	                S[i] = (((S[i] << 8)  | (S[i] >>> 24)) & 0x00ff00ff) |
   	                       (((S[i] << 24) | (S[i] >>> 8))  & 0xff00ff00);

   	                // Encrypt
   	                M[offset + i] ^= S[i];
   	            }
   	        },

   	        blockSize: 128/32,

   	        ivSize: 64/32
   	    });

   	    function nextState() {
   	        // Shortcuts
   	        var X = this._X;
   	        var C = this._C;

   	        // Save old counter values
   	        for (var i = 0; i < 8; i++) {
   	            C_[i] = C[i];
   	        }

   	        // Calculate new counter values
   	        C[0] = (C[0] + 0x4d34d34d + this._b) | 0;
   	        C[1] = (C[1] + 0xd34d34d3 + ((C[0] >>> 0) < (C_[0] >>> 0) ? 1 : 0)) | 0;
   	        C[2] = (C[2] + 0x34d34d34 + ((C[1] >>> 0) < (C_[1] >>> 0) ? 1 : 0)) | 0;
   	        C[3] = (C[3] + 0x4d34d34d + ((C[2] >>> 0) < (C_[2] >>> 0) ? 1 : 0)) | 0;
   	        C[4] = (C[4] + 0xd34d34d3 + ((C[3] >>> 0) < (C_[3] >>> 0) ? 1 : 0)) | 0;
   	        C[5] = (C[5] + 0x34d34d34 + ((C[4] >>> 0) < (C_[4] >>> 0) ? 1 : 0)) | 0;
   	        C[6] = (C[6] + 0x4d34d34d + ((C[5] >>> 0) < (C_[5] >>> 0) ? 1 : 0)) | 0;
   	        C[7] = (C[7] + 0xd34d34d3 + ((C[6] >>> 0) < (C_[6] >>> 0) ? 1 : 0)) | 0;
   	        this._b = (C[7] >>> 0) < (C_[7] >>> 0) ? 1 : 0;

   	        // Calculate the g-values
   	        for (var i = 0; i < 8; i++) {
   	            var gx = X[i] + C[i];

   	            // Construct high and low argument for squaring
   	            var ga = gx & 0xffff;
   	            var gb = gx >>> 16;

   	            // Calculate high and low result of squaring
   	            var gh = ((((ga * ga) >>> 17) + ga * gb) >>> 15) + gb * gb;
   	            var gl = (((gx & 0xffff0000) * gx) | 0) + (((gx & 0x0000ffff) * gx) | 0);

   	            // High XOR low
   	            G[i] = gh ^ gl;
   	        }

   	        // Calculate new state values
   	        X[0] = (G[0] + ((G[7] << 16) | (G[7] >>> 16)) + ((G[6] << 16) | (G[6] >>> 16))) | 0;
   	        X[1] = (G[1] + ((G[0] << 8)  | (G[0] >>> 24)) + G[7]) | 0;
   	        X[2] = (G[2] + ((G[1] << 16) | (G[1] >>> 16)) + ((G[0] << 16) | (G[0] >>> 16))) | 0;
   	        X[3] = (G[3] + ((G[2] << 8)  | (G[2] >>> 24)) + G[1]) | 0;
   	        X[4] = (G[4] + ((G[3] << 16) | (G[3] >>> 16)) + ((G[2] << 16) | (G[2] >>> 16))) | 0;
   	        X[5] = (G[5] + ((G[4] << 8)  | (G[4] >>> 24)) + G[3]) | 0;
   	        X[6] = (G[6] + ((G[5] << 16) | (G[5] >>> 16)) + ((G[4] << 16) | (G[4] >>> 16))) | 0;
   	        X[7] = (G[7] + ((G[6] << 8)  | (G[6] >>> 24)) + G[5]) | 0;
   	    }

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.Rabbit.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.Rabbit.decrypt(ciphertext, key, cfg);
   	     */
   	    C.Rabbit = StreamCipher._createHelper(Rabbit);
   	}());


   	return CryptoJS.Rabbit;

   }));
   });

   var rabbitLegacy = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, encBase64, md5, evpkdf, cipherCore);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	(function () {
   	    // Shortcuts
   	    var C = CryptoJS;
   	    var C_lib = C.lib;
   	    var StreamCipher = C_lib.StreamCipher;
   	    var C_algo = C.algo;

   	    // Reusable objects
   	    var S  = [];
   	    var C_ = [];
   	    var G  = [];

   	    /**
   	     * Rabbit stream cipher algorithm.
   	     *
   	     * This is a legacy version that neglected to convert the key to little-endian.
   	     * This error doesn't affect the cipher's security,
   	     * but it does affect its compatibility with other implementations.
   	     */
   	    var RabbitLegacy = C_algo.RabbitLegacy = StreamCipher.extend({
   	        _doReset: function () {
   	            // Shortcuts
   	            var K = this._key.words;
   	            var iv = this.cfg.iv;

   	            // Generate initial state values
   	            var X = this._X = [
   	                K[0], (K[3] << 16) | (K[2] >>> 16),
   	                K[1], (K[0] << 16) | (K[3] >>> 16),
   	                K[2], (K[1] << 16) | (K[0] >>> 16),
   	                K[3], (K[2] << 16) | (K[1] >>> 16)
   	            ];

   	            // Generate initial counter values
   	            var C = this._C = [
   	                (K[2] << 16) | (K[2] >>> 16), (K[0] & 0xffff0000) | (K[1] & 0x0000ffff),
   	                (K[3] << 16) | (K[3] >>> 16), (K[1] & 0xffff0000) | (K[2] & 0x0000ffff),
   	                (K[0] << 16) | (K[0] >>> 16), (K[2] & 0xffff0000) | (K[3] & 0x0000ffff),
   	                (K[1] << 16) | (K[1] >>> 16), (K[3] & 0xffff0000) | (K[0] & 0x0000ffff)
   	            ];

   	            // Carry bit
   	            this._b = 0;

   	            // Iterate the system four times
   	            for (var i = 0; i < 4; i++) {
   	                nextState.call(this);
   	            }

   	            // Modify the counters
   	            for (var i = 0; i < 8; i++) {
   	                C[i] ^= X[(i + 4) & 7];
   	            }

   	            // IV setup
   	            if (iv) {
   	                // Shortcuts
   	                var IV = iv.words;
   	                var IV_0 = IV[0];
   	                var IV_1 = IV[1];

   	                // Generate four subvectors
   	                var i0 = (((IV_0 << 8) | (IV_0 >>> 24)) & 0x00ff00ff) | (((IV_0 << 24) | (IV_0 >>> 8)) & 0xff00ff00);
   	                var i2 = (((IV_1 << 8) | (IV_1 >>> 24)) & 0x00ff00ff) | (((IV_1 << 24) | (IV_1 >>> 8)) & 0xff00ff00);
   	                var i1 = (i0 >>> 16) | (i2 & 0xffff0000);
   	                var i3 = (i2 << 16)  | (i0 & 0x0000ffff);

   	                // Modify counter values
   	                C[0] ^= i0;
   	                C[1] ^= i1;
   	                C[2] ^= i2;
   	                C[3] ^= i3;
   	                C[4] ^= i0;
   	                C[5] ^= i1;
   	                C[6] ^= i2;
   	                C[7] ^= i3;

   	                // Iterate the system four times
   	                for (var i = 0; i < 4; i++) {
   	                    nextState.call(this);
   	                }
   	            }
   	        },

   	        _doProcessBlock: function (M, offset) {
   	            // Shortcut
   	            var X = this._X;

   	            // Iterate the system
   	            nextState.call(this);

   	            // Generate four keystream words
   	            S[0] = X[0] ^ (X[5] >>> 16) ^ (X[3] << 16);
   	            S[1] = X[2] ^ (X[7] >>> 16) ^ (X[5] << 16);
   	            S[2] = X[4] ^ (X[1] >>> 16) ^ (X[7] << 16);
   	            S[3] = X[6] ^ (X[3] >>> 16) ^ (X[1] << 16);

   	            for (var i = 0; i < 4; i++) {
   	                // Swap endian
   	                S[i] = (((S[i] << 8)  | (S[i] >>> 24)) & 0x00ff00ff) |
   	                       (((S[i] << 24) | (S[i] >>> 8))  & 0xff00ff00);

   	                // Encrypt
   	                M[offset + i] ^= S[i];
   	            }
   	        },

   	        blockSize: 128/32,

   	        ivSize: 64/32
   	    });

   	    function nextState() {
   	        // Shortcuts
   	        var X = this._X;
   	        var C = this._C;

   	        // Save old counter values
   	        for (var i = 0; i < 8; i++) {
   	            C_[i] = C[i];
   	        }

   	        // Calculate new counter values
   	        C[0] = (C[0] + 0x4d34d34d + this._b) | 0;
   	        C[1] = (C[1] + 0xd34d34d3 + ((C[0] >>> 0) < (C_[0] >>> 0) ? 1 : 0)) | 0;
   	        C[2] = (C[2] + 0x34d34d34 + ((C[1] >>> 0) < (C_[1] >>> 0) ? 1 : 0)) | 0;
   	        C[3] = (C[3] + 0x4d34d34d + ((C[2] >>> 0) < (C_[2] >>> 0) ? 1 : 0)) | 0;
   	        C[4] = (C[4] + 0xd34d34d3 + ((C[3] >>> 0) < (C_[3] >>> 0) ? 1 : 0)) | 0;
   	        C[5] = (C[5] + 0x34d34d34 + ((C[4] >>> 0) < (C_[4] >>> 0) ? 1 : 0)) | 0;
   	        C[6] = (C[6] + 0x4d34d34d + ((C[5] >>> 0) < (C_[5] >>> 0) ? 1 : 0)) | 0;
   	        C[7] = (C[7] + 0xd34d34d3 + ((C[6] >>> 0) < (C_[6] >>> 0) ? 1 : 0)) | 0;
   	        this._b = (C[7] >>> 0) < (C_[7] >>> 0) ? 1 : 0;

   	        // Calculate the g-values
   	        for (var i = 0; i < 8; i++) {
   	            var gx = X[i] + C[i];

   	            // Construct high and low argument for squaring
   	            var ga = gx & 0xffff;
   	            var gb = gx >>> 16;

   	            // Calculate high and low result of squaring
   	            var gh = ((((ga * ga) >>> 17) + ga * gb) >>> 15) + gb * gb;
   	            var gl = (((gx & 0xffff0000) * gx) | 0) + (((gx & 0x0000ffff) * gx) | 0);

   	            // High XOR low
   	            G[i] = gh ^ gl;
   	        }

   	        // Calculate new state values
   	        X[0] = (G[0] + ((G[7] << 16) | (G[7] >>> 16)) + ((G[6] << 16) | (G[6] >>> 16))) | 0;
   	        X[1] = (G[1] + ((G[0] << 8)  | (G[0] >>> 24)) + G[7]) | 0;
   	        X[2] = (G[2] + ((G[1] << 16) | (G[1] >>> 16)) + ((G[0] << 16) | (G[0] >>> 16))) | 0;
   	        X[3] = (G[3] + ((G[2] << 8)  | (G[2] >>> 24)) + G[1]) | 0;
   	        X[4] = (G[4] + ((G[3] << 16) | (G[3] >>> 16)) + ((G[2] << 16) | (G[2] >>> 16))) | 0;
   	        X[5] = (G[5] + ((G[4] << 8)  | (G[4] >>> 24)) + G[3]) | 0;
   	        X[6] = (G[6] + ((G[5] << 16) | (G[5] >>> 16)) + ((G[4] << 16) | (G[4] >>> 16))) | 0;
   	        X[7] = (G[7] + ((G[6] << 8)  | (G[6] >>> 24)) + G[5]) | 0;
   	    }

   	    /**
   	     * Shortcut functions to the cipher's object interface.
   	     *
   	     * @example
   	     *
   	     *     var ciphertext = CryptoJS.RabbitLegacy.encrypt(message, key, cfg);
   	     *     var plaintext  = CryptoJS.RabbitLegacy.decrypt(ciphertext, key, cfg);
   	     */
   	    C.RabbitLegacy = StreamCipher._createHelper(RabbitLegacy);
   	}());


   	return CryptoJS.RabbitLegacy;

   }));
   });

   var cryptoJs = createCommonjsModule(function (module, exports) {
   (function (root, factory, undef) {
   	{
   		// CommonJS
   		module.exports = exports = factory(core, x64Core, libTypedarrays, encUtf16, encBase64, md5, sha1, sha256, sha224, sha512, sha384, sha3, ripemd160, hmac, pbkdf2, evpkdf, cipherCore, modeCfb, modeCtr, modeCtrGladman, modeOfb, modeEcb, padAnsix923, padIso10126, padIso97971, padZeropadding, padNopadding, formatHex, aes, tripledes, rc4, rabbit, rabbitLegacy);
   	}
   }(commonjsGlobal, function (CryptoJS) {

   	return CryptoJS;

   }));
   });

   // from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString

   if (!Date.prototype.toISOString) {
     (function () {
       function pad(number) {
         var r = String(number);

         if (r.length === 1) {
           r = '0' + r;
         }

         return r;
       }

       Date.prototype.toISOString = function () {
         return this.getUTCFullYear() + '-' + pad(this.getUTCMonth() + 1) + '-' + pad(this.getUTCDate()) + 'T' + pad(this.getUTCHours()) + ':' + pad(this.getUTCMinutes()) + ':' + pad(this.getUTCSeconds()) + '.' + String((this.getUTCMilliseconds() / 1000).toFixed(3)).slice(2, 5) + 'Z';
       };
     })();
   } // shim for old-style Base64 lib


   function toBase64(text) {
     if (cryptoJs && cryptoJs.enc.Base64) return cryptoJs.enc.Base64.stringify(cryptoJs.enc.Latin1.parse(text));else return Base64.encode(text);
   } // shim for old-style crypto lib


   function toSHA1(text) {
     if (cryptoJs && cryptoJs.SHA1) return cryptoJs.SHA1(text).toString();else return Crypto.util.bytesToHex(Crypto.SHA1(text, {
       asBytes: true
     }));
   }

   function toSHA256(content) {
     if (Object.prototype.toString.call(content) !== "[object ArrayBuffer]") {
       return cryptoJs.SHA256(content).toString(cryptoJs.enc.Hex);
     } // Create a WordArray from the ArrayBuffer.


     var i8a = new Uint8Array(content);
     var a = [];

     for (var i = 0; i < i8a.length; i += 4) {
       a.push(i8a[i] << 24 | i8a[i + 1] << 16 | i8a[i + 2] << 8 | i8a[i + 3]);
     }

     return cryptoJs.SHA256(cryptoJs.lib.WordArray.create(a, i8a.length)).toString(cryptoJs.enc.Hex);
   } // check if string or object is date, if it is, return date object
   // feburary 31st == march 3rd in this solution


   function isDate(date) {
     // check if object is being passed
     if (Object.prototype.toString.call(date) === "[object Date]") var d = date;else var d = new Date(date); // deep check on date object

     if (Object.prototype.toString.call(d) === "[object Date]") {
       // it is a date
       if (isNaN(d.valueOf())) {
         ADL.XAPIWrapper.log("Invalid date String passed");
         return null;
       } else {
         return d;
       }
     } else {
       // not a date
       ADL.XAPIWrapper.log("Invalid date object");
       return null;
     }
   }

   function setup$5(ADL) {
     log.debug = false;
     /*
      * Config object used w/ url params to configure the lrs object
      * change these to match your lrs
      * @return {object} config object
      * @example
      * var conf = {
      *    "endpoint" : "https://lrs.adlnet.gov/xapi/",
      *    "auth" : "Basic " + toBase64('tom:1234'),
      * };
      * ADL.XAPIWrapper.changeConfig(conf);
      */


     var Config = function () {
       var conf = {};
       conf['endpoint'] = "http://localhost:8000/xapi/"; //try
       //{

       conf['auth'] = "Basic " + toBase64('tom:1234'); //}
       //catch (e)
       //{
       //    log("Exception in Config trying to encode auth: " + e);
       //}
       // Statement defaults
       // conf["actor"] = {"mbox":"default@example.com"};
       // conf["registration"] =  ruuid();
       // conf["grouping"] = {"id":"ctxact:default/grouping"};
       // conf["activity_platform"] = "default platform";
       // Behavior defaults
       // conf["strictCallbacks"] = false; // Strict error-first callbacks

       return conf;
     }();
     /*
      * XAPIWrapper Constructor
      * @param {object} config   with a minimum of an endoint property
      * @param {boolean} verifyxapiversion   indicating whether to verify the version of the LRS is compatible with this wrapper
      */


     var XAPIWrapper = function (config, verifyxapiversion) {
       this.lrs = getLRSObject(config || {});
       if (this.lrs.user && this.lrs.password) updateAuth(this.lrs, this.lrs.user, this.lrs.password);
       this.base = getbase(this.lrs.endpoint);
       this.withCredentials = false;

       if (config && typeof config.withCredentials != 'undefined') {
         this.withCredentials = config.withCredentials;
       } // Ensure that callbacks are always executed, first param is error (null if no error) followed
       // by the result(s)


       this.strictCallbacks = false;
       this.strictCallbacks = config && config.strictCallbacks;

       function getbase(url) {
         var l = document.createElement("a");
         l.href = url;

         if (l.protocol && l.host) {
           return l.protocol + "//" + l.host;
         } else if (l.href) {
           // IE 11 fix.
           var parts = l.href.split("//");
           return parts[0] + "//" + parts[1].substr(0, parts[1].indexOf("/"));
         } else ADL.XAPIWrapper.log("Couldn't create base url from endpoint: " + url);
       }

       function updateAuth(obj, username, password) {
         obj.auth = "Basic " + toBase64(username + ":" + password);
       }

       if (verifyxapiversion && testConfig.call(this)) {
         window.ADL.XHR_request(this.lrs, this.lrs.endpoint + "about", "GET", null, null, function (r) {
           if (r.status == 200) {
             try {
               var lrsabout = JSON.parse(r.response);
               var versionOK = false;

               for (var idx in lrsabout.version) {
                 if (lrsabout.version.hasOwnProperty(idx)) if (lrsabout.version[idx] == ADL.XAPIWrapper.xapiVersion) {
                   versionOK = true;
                   break;
                 }
               }

               if (!versionOK) {
                 ADL.XAPIWrapper.log("The lrs version [" + lrsabout.version + "]" + " does not match this wrapper's XAPI version [" + ADL.XAPIWrapper.xapiVersion + "]");
               }
             } catch (e) {
               ADL.XAPIWrapper.log("The response was not an about object");
             }
           } else {
             ADL.XAPIWrapper.log("The request to get information about the LRS failed: " + r);
           }
         }, null, false, null, this.withCredentials, false);
       }

       this.searchParams = function () {
         var sp = {
           "format": "exact"
         };
         return sp;
       };

       this.hash = function (tohash) {
         if (!tohash) return null;

         try {
           return toSHA1(tohash);
         } catch (e) {
           ADL.XAPIWrapper.log("Error trying to hash -- " + e);
           return null;
         }
       };

       this.changeConfig = function (config) {
         try {
           ADL.XAPIWrapper.log("updating lrs object with new configuration");
           this.lrs = mergeRecursive(this.lrs, config);
           if (config.user && config.password) this.updateAuth(this.lrs, config.user, config.password);
           this.base = getbase(this.lrs.endpoint);
           this.withCredentials = config.withCredentials;
           this.strictCallbacks = config.strictCallbacks;
         } catch (e) {
           ADL.XAPIWrapper.log("error while changing configuration -- " + e);
         }
       };

       this.updateAuth = updateAuth;
     }; // This wrapper is based on the Experience API Spec version:


     XAPIWrapper.prototype.xapiVersion = "1.0.1";
     /*
      * Adds info from the lrs object to the statement, if available.
      * These values could be initialized from the Config object or from the url query string.
      * @param {object} stmt   the statement object
      */

     XAPIWrapper.prototype.prepareStatement = function (stmt) {
       if (stmt.actor === undefined) {
         stmt.actor = JSON.parse(this.lrs.actor);
       } else if (typeof stmt.actor === "string") {
         stmt.actor = JSON.parse(stmt.actor);
       }

       if (this.lrs.grouping || this.lrs.registration || this.lrs.activity_platform) {
         if (!stmt.context) {
           stmt.context = {};
         }
       }

       if (this.lrs.grouping) {
         if (!stmt.context.contextActivities) {
           stmt.context.contextActivities = {};
         } // PR from brian-learningpool to resolve context overwriting


         if (!Array.isArray(stmt.context.contextActivities.grouping)) {
           stmt.context.contextActivities.grouping = [{
             id: this.lrs.grouping
           }];
         } else {
           stmt.context.contextActivities.grouping.splice(0, 0, {
             id: this.lrs.grouping
           });
         }
       }

       if (this.lrs.registration) {
         stmt.context.registration = this.lrs.registration;
       }

       if (this.lrs.activity_platform) {
         stmt.context.platform = this.lrs.activity_platform;
       }
     }; // tests the configuration of the lrs object


     XAPIWrapper.prototype.testConfig = testConfig; // writes to the console if available

     XAPIWrapper.prototype.log = log; // Default encoding

     XAPIWrapper.prototype.defaultEncoding = 'utf-8';
     /*
      * Send a single statement to the LRS. Makes a Javascript object
      * with the statement id as 'id' available to the callback function.
      * @param {object} stmt   statement object to send
      * @param {function} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      *            and an object with an id property assigned the id
      *            of the statement
      * @return {object} object containing xhr object and id of statement
      * @example
      * // Send Statement
      * var stmt = {"actor" : {"mbox" : "mailto:tom@example.com"},
      *             "verb" : {"id" : "http://adlnet.gov/expapi/verbs/answered",
      *                       "display" : {"en-US" : "answered"}},
      *             "object" : {"id" : "http://adlnet.gov/expapi/activities/question"}};
      * var resp_obj = ADL.XAPIWrapper.sendStatement(stmt);
      * ADL.XAPIWrapper.log("[" + resp_obj.id + "]: " + resp_obj.xhr.status + " - " + resp_obj.xhr.statusText);
      * >> [3e616d1c-5394-42dc-a3aa-29414f8f0dfe]: 204 - NO CONTENT
      *
      * // Send Statement with Callback
      * var stmt = {"actor" : {"mbox" : "mailto:tom@example.com"},
      *             "verb" : {"id" : "http://adlnet.gov/expapi/verbs/answered",
      *                       "display" : {"en-US" : "answered"}},
      *             "object" : {"id" : "http://adlnet.gov/expapi/activities/question"}};
      * ADL.XAPIWrapper.sendStatement(stmt, function(resp, obj){
      *     ADL.XAPIWrapper.log("[" + obj.id + "]: " + resp.status + " - " + resp.statusText);});
      * >> [4edfe763-8b84-41f1-a355-78b7601a6fe8]: 204 - NO CONTENT
      */

     XAPIWrapper.prototype.sendStatement = function (stmt, callback, attachments) {
       if (this.testConfig()) {
         this.prepareStatement(stmt);
         var id;

         if (stmt['id']) {
           id = stmt['id'];
         } else {
           id = ADL.ruuid();
           stmt['id'] = id;
         }

         var payload = JSON.stringify(stmt);
         var extraHeaders = null;

         if (attachments && attachments.length > 0) {
           extraHeaders = {};
           payload = this.buildMultipartPost(stmt, attachments, extraHeaders);
         }

         var resp = ADL.XHR_request(this.lrs, this.lrs.endpoint + "statements", "POST", payload, this.lrs.auth, callback, {
           "id": id
         }, null, extraHeaders, this.withCredentials, this.strictCallbacks);
         if (!callback) return {
           "xhr": resp,
           "id": id
         };
       }
     };

     XAPIWrapper.prototype.stringToArrayBuffer = function (content, encoding) {
       encoding = encoding || ADL.XAPIWrapper.defaultEncoding;
       return new TextEncoder(encoding).encode(content).buffer;
     };

     XAPIWrapper.prototype.stringFromArrayBuffer = function (content, encoding) {
       encoding = encoding || ADL.XAPIWrapper.defaultEncoding;
       return new TextDecoder(encoding).decode(content);
     };
     /*
     * Build the post body to include the multipart boundries, edit the statement to include the attachment types
     * extraHeaders should be an object. It will have the multipart boundary value set
     * attachments should be an array of objects of the type
     * {
           type:"signature" || {
               usageType : URI,
               display: Language-map
               description: Language-map
           },
           value : a UTF8 string containing the binary data of the attachment. For string values, this can just be the JS string.
        }
     */


     XAPIWrapper.prototype.buildMultipartPost = function (statement, attachments, extraHeaders) {
       statement.attachments = [];

       for (var i = 0; i < attachments.length; i++) {
         // Replace the term 'signature' with the hard coded definition for a signature attachment
         if (attachments[i].type == "signature") {
           attachments[i].type = {
             "usageType": "http://adlnet.gov/expapi/attachments/signature",
             "display": {
               "en-US": "A JWT signature"
             },
             "description": {
               "en-US": "A signature proving the statement was not modified"
             },
             "contentType": "application/octet-stream"
           };
         }

         if (typeof attachments[i].value === 'string') {
           // Convert the string value to an array buffer.
           attachments[i].value = this.stringToArrayBuffer(attachments[i].value);
         } // Compute the length and the sha2 of the attachment


         attachments[i].type.length = attachments[i].value.byteLength;
         attachments[i].type.sha2 = toSHA256(attachments[i].value); // Attach the attachment metadata to the statement.

         statement.attachments.push(attachments[i].type);
       }

       var blobParts = [];
       var boundary = (Math.random() + ' ').substring(2, 10) + (Math.random() + ' ').substring(2, 10);
       extraHeaders["Content-Type"] = "multipart/mixed; boundary=" + boundary;
       var CRLF = "\r\n";
       var header = ["--" + boundary, "Content-Type: application/json", "Content-Disposition: form-data; name=\"statement\"", "", JSON.stringify(statement)].join(CRLF) + CRLF;
       blobParts.push(header);

       for (var i in attachments) {
         if (attachments.hasOwnProperty(i)) {
           var attachmentHeader = ["--" + boundary, "Content-Type: " + attachments[i].type.contentType, "Content-Transfer-Encoding: binary", "X-Experience-API-Hash: " + attachments[i].type.sha2].join(CRLF) + CRLF + CRLF;
           blobParts.push(attachmentHeader);
           blobParts.push(attachments[i].value);
         }
       }

       blobParts.push(CRLF + "--" + boundary + "--" + CRLF);
       return new Blob(blobParts);
     };
     /*
      * Send a list of statements to the LRS.
      * @param {array} stmtArray   the list of statement objects to send
      * @param {function} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object
      * @example
      * var stmt = {"actor" : {"mbox" : "mailto:tom@example.com"},
      *             "verb" : {"id" : "http://adlnet.gov/expapi/verbs/answered",
      *                       "display" : {"en-US" : "answered"}},
      *             "object" : {"id" : "http://adlnet.gov/expapi/activities/question"}};
      * var resp_obj = ADL.XAPIWrapper.sendStatement(stmt);
      * ADL.XAPIWrapper.getStatements({"statementId":resp_obj.id});
      * >> {"version": "1.0.0",
      *     "timestamp": "2013-09-09 21:36:40.185841+00:00",
      *     "object": {"id": "http://adlnet.gov/expapi/activities/question", "objectType": "Activity"},
      *     "actor": {"mbox": "mailto:tom@example.com", "name": "tom creighton", "objectType": "Agent"},
      *     "stored": "2013-09-09 21:36:40.186124+00:00",
      *     "verb": {"id": "http://adlnet.gov/expapi/verbs/answered", "display": {"en-US": "answered"}},
      *     "authority": {"mbox": "mailto:tom@adlnet.gov", "name": "tom", "objectType": "Agent"},
      *     "context": {"registration": "51a6f860-1997-11e3-8ffd-0800200c9a66"},
      *     "id": "ea9c1d01-0606-4ec7-8e5d-20f87b1211ed"}
      */


     XAPIWrapper.prototype.sendStatements = function (stmtArray, callback) {
       if (this.testConfig()) {
         for (var i in stmtArray) {
           if (stmtArray.hasOwnProperty(i)) this.prepareStatement(stmtArray[i]);
         }

         var resp = ADL.XHR_request(this.lrs, this.lrs.endpoint + "statements", "POST", JSON.stringify(stmtArray), this.lrs.auth, callback, null, false, null, this.withCredentials, this.strictCallbacks);

         if (!callback) {
           return resp;
         }
       }
     };
     /*
      * Get statement(s) based on the searchparams or more url.
      * @param {object} searchparams   an ADL.XAPIWrapper.searchParams object of
      *                key(search parameter)-value(parameter value) pairs.
      *                Example:
      *                  var myparams = ADL.XAPIWrapper.searchParams();
      *                  myparams['verb'] = ADL.verbs.completed.id;
      *                  var completedStmts = ADL.XAPIWrapper.getStatements(myparams);
      * @param {string} more   the more url found in the StatementResults object, if there are more
      *        statements available based on your get statements request. Pass the
      *        more url as this parameter to retrieve those statements.
      * @param {function} [callback] - function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * var ret = ADL.XAPIWrapper.getStatements();
      * if (ret)
      *     ADL.XAPIWrapper.log(ret.statements);
      *
      * >> <Array of statements>
      */


     XAPIWrapper.prototype.getStatements = function (searchparams, more, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "statements";

         if (more) {
           url = this.base + more;
         } else {
           var urlparams = new Array();

           for (var s in searchparams) {
             if (searchparams.hasOwnProperty(s)) {
               if (s == "until" || s == "since") {
                 var d = new Date(searchparams[s]);
                 urlparams.push(s + "=" + encodeURIComponent(d.toISOString()));
               } else {
                 urlparams.push(s + "=" + encodeURIComponent(searchparams[s]));
               }
             }
           }

           if (urlparams.length > 0) url = url + "?" + urlparams.join("&");
         }

         var res = ADL.XHR_request(this.lrs, url, "GET", null, this.lrs.auth, callback, null, false, null, this.withCredentials, this.strictCallbacks);

         if (res === undefined || res.status == 404) {
           return null;
         }

         try {
           return JSON.parse(res.response);
         } catch (e) {
           return res.response;
         }
       }
     };
     /*
      * Gets the Activity object from the LRS.
      * @param {string} activityid   the id of the Activity to get
      * @param {function} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * var res = ADL.XAPIWrapper.getActivities("http://adlnet.gov/expapi/activities/question");
      * ADL.XAPIWrapper.log(res);
      * >> <Activity object>
      */


     XAPIWrapper.prototype.getActivities = function (activityid, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities?activityId=<activityid>";
         url = url.replace('<activityid>', encodeURIComponent(activityid));
         var result = ADL.XHR_request(this.lrs, url, "GET", null, this.lrs.auth, callback, null, true, null, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result.response;
         }
       }
     };
     /*
      * Store activity state in the LRS
      * @param {string} activityid   the id of the Activity this state is about
      * @param {object} agent   the agent this Activity state is related to
      * @param {string} stateid   the id you want associated with this state
      * @param {string} [registration]   the registraton id associated with this state
      * @param {string} stateval   the state
      * @param {string} [matchHash]    the hash of the state to replace or * to replace any
      * @param {string} [noneMatchHash]    the hash of the current state or * to indicate no previous state
      * @param {function} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {boolean} false if no activity state is included
      * @example
      * var stateval = {"info":"the state info"};
      * ADL.XAPIWrapper.sendState("http://adlnet.gov/expapi/activities/question",
      *                    {"mbox":"mailto:tom@example.com"},
      *                    "questionstate", null, stateval);
      */


     XAPIWrapper.prototype.sendState = function (activityid, agent, stateid, registration, stateval, matchHash, noneMatchHash, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities/state?activityId=<activity ID>&agent=<agent>&stateId=<stateid>";
         url = url.replace('<activity ID>', encodeURIComponent(activityid));
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));
         url = url.replace('<stateid>', encodeURIComponent(stateid));

         if (registration) {
           url += "&registration=" + encodeURIComponent(registration);
         }

         var headers = null;

         if (matchHash && noneMatchHash) {
           log("Can't have both If-Match and If-None-Match");
         } else if (matchHash) {
           headers = {
             "If-Match": ADL.formatHash(matchHash)
           };
         } else if (noneMatchHash) {
           headers = {
             "If-None-Match": ADL.formatHash(noneMatchHash)
           };
         }

         var method = "PUT";

         if (stateval) {
           if (stateval instanceof Array) {
             stateval = JSON.stringify(stateval);
             headers = headers || {};
             headers["Content-Type"] = "application/json";
           } else if (stateval instanceof Object) {
             stateval = JSON.stringify(stateval);
             headers = headers || {};
             headers["Content-Type"] = "application/json";
             method = "POST";
           } else {
             headers = headers || {};
             headers["Content-Type"] = "application/octet-stream";
           }
         } else {
           this.log("No activity state was included.");
           return false;
         } //(lrs, url, method, data, auth, callback, callbackargs, ignore404, extraHeaders)


         ADL.XHR_request(this.lrs, url, method, stateval, this.lrs.auth, callback, null, null, headers, this.withCredentials, this.strictCallbacks);
       }
     };
     /*
      * Get activity state from the LRS
      * @param {string} activityid   the id of the Activity this state is about
      * @param {object} agent   the agent this Activity state is related to
      * @param {string} [stateid]    the id of the state, if not included, the response will be a list of stateids
      *            associated with the activity and agent)
      * @param {string} [registration]   the registraton id associated with this state
      * @param {object} [since]    date object or date string telling the LRS to return objects newer than the date supplied
      * @param {function} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * ADL.XAPIWrapper.getState("http://adlnet.gov/expapi/activities/question",
      *                  {"mbox":"mailto:tom@example.com"}, "questionstate");
      * >> {info: "the state info"}
      */


     XAPIWrapper.prototype.getState = function (activityid, agent, stateid, registration, since, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities/state?activityId=<activity ID>&agent=<agent>";
         url = url.replace('<activity ID>', encodeURIComponent(activityid));
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));

         if (stateid) {
           url += "&stateId=" + encodeURIComponent(stateid);
         }

         if (registration) {
           url += "&registration=" + encodeURIComponent(registration);
         }

         if (since) {
           since = isDate(since);

           if (since != null) {
             url += '&since=' + encodeURIComponent(since.toISOString());
           }
         }

         var result = ADL.XHR_request(this.lrs, url, "GET", null, this.lrs.auth, callback, null, true, null, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result.response;
         }
       }
     };
     /*
      * Delete activity state in the LRS
      * @param {string} activityid   the id of the Activity this state is about
      * @param {object} agent   the agent this Activity state is related to
      * @param {string} stateid   the id you want associated with this state
      * @param {string} [registration]   the registraton id associated with this state
      * @param {string} [matchHash]    the hash of the state to replace or * to replace any
      * @param {string} [noneMatchHash]    the hash of the current state or * to indicate no previous state
      * @param {string} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * var stateval = {"info":"the state info"};
      * ADL.XAPIWrapper.sendState("http://adlnet.gov/expapi/activities/question",
      *                           {"mbox":"mailto:tom@example.com"},
      *                           "questionstate", null, stateval);
      * ADL.XAPIWrapper.getState("http://adlnet.gov/expapi/activities/question",
      *                         {"mbox":"mailto:tom@example.com"}, "questionstate");
      * >> {info: "the state info"}
      *
      * ADL.XAPIWrapper.deleteState("http://adlnet.gov/expapi/activities/question",
      *                         {"mbox":"mailto:tom@example.com"}, "questionstate");
      * >> XMLHttpRequest {statusText: "NO CONTENT", status: 204, response: "", responseType: "", responseXML: null…}
      *
      * ADL.XAPIWrapper.getState("http://adlnet.gov/expapi/activities/question",
      *                         {"mbox":"mailto:tom@example.com"}, "questionstate");
      * >> 404
      */


     XAPIWrapper.prototype.deleteState = function (activityid, agent, stateid, registration, matchHash, noneMatchHash, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities/state?activityId=<activity ID>&agent=<agent>&stateId=<stateid>";
         url = url.replace('<activity ID>', encodeURIComponent(activityid));
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));
         url = url.replace('<stateid>', encodeURIComponent(stateid));

         if (registration) {
           url += "&registration=" + encodeURIComponent(registration);
         }

         var headers = null;

         if (matchHash && noneMatchHash) {
           log("Can't have both If-Match and If-None-Match");
         } else if (matchHash) {
           headers = {
             "If-Match": ADL.formatHash(matchHash)
           };
         } else if (noneMatchHash) {
           headers = {
             "If-None-Match": ADL.formatHash(noneMatchHash)
           };
         }

         var result = ADL.XHR_request(this.lrs, url, "DELETE", null, this.lrs.auth, callback, null, false, headers, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result;
         }
       }
     };
     /*
      * Store activity profile in the LRS
      * @param {string} activityid   the id of the Activity this profile is about
      * @param {string} profileid   the id you want associated with this profile
      * @param {string} profileval   the profile
      * @param {string} [matchHash]    the hash of the profile to replace or * to replace any
      * @param {string} [noneMatchHash]    the hash of the current profile or * to indicate no previous profile
      * @param {string} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {bolean} false if no activity profile is included
      * @example
      * var profile = {"info":"the profile"};
      * ADL.XAPIWrapper.sendActivityProfile("http://adlnet.gov/expapi/activities/question",
      *                                     "actprofile", profile, null, "*");
      */


     XAPIWrapper.prototype.sendActivityProfile = function (activityid, profileid, profileval, matchHash, noneMatchHash, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities/profile?activityId=<activity ID>&profileId=<profileid>";
         url = url.replace('<activity ID>', encodeURIComponent(activityid));
         url = url.replace('<profileid>', encodeURIComponent(profileid));
         var headers = null;

         if (matchHash && noneMatchHash) {
           log("Can't have both If-Match and If-None-Match");
         } else if (matchHash) {
           headers = {
             "If-Match": ADL.formatHash(matchHash)
           };
         } else if (noneMatchHash) {
           headers = {
             "If-None-Match": ADL.formatHash(noneMatchHash)
           };
         }

         var method = "PUT";

         if (profileval) {
           if (profileval instanceof Array) {
             profileval = JSON.stringify(profileval);
             headers = headers || {};
             headers["Content-Type"] = "application/json";
           } else if (profileval instanceof Object) {
             profileval = JSON.stringify(profileval);
             headers = headers || {};
             headers["Content-Type"] = "application/json";
             method = "POST";
           } else {
             headers = headers || {};
             headers["Content-Type"] = "application/octet-stream";
           }
         } else {
           this.log("No activity profile was included.");
           return false;
         }

         ADL.XHR_request(this.lrs, url, method, profileval, this.lrs.auth, callback, null, false, headers, this.withCredentials, this.strictCallbacks);
       }
     };
     /*
      * Get activity profile from the LRS
      * @param {string} activityid   the id of the Activity this profile is about
      * @param {string} [profileid]    the id of the profile, if not included, the response will be a list of profileids
      *              associated with the activity
      * @param {object} [since]    date object or date string telling the LRS to return objects newer than the date supplied
      * @param {function [callback]    function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * ADL.XAPIWrapper.getActivityProfile("http://adlnet.gov/expapi/activities/question",
      *                                    "actprofile", null,
      *                                    function(r){ADL.XAPIWrapper.log(JSON.parse(r.response));});
      * >> {info: "the profile"}
      */


     XAPIWrapper.prototype.getActivityProfile = function (activityid, profileid, since, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities/profile?activityId=<activity ID>";
         url = url.replace('<activity ID>', encodeURIComponent(activityid));

         if (profileid) {
           url += "&profileId=" + encodeURIComponent(profileid);
         }

         if (since) {
           since = isDate(since);

           if (since != null) {
             url += '&since=' + encodeURIComponent(since.toISOString());
           }
         }

         var result = ADL.XHR_request(this.lrs, url, "GET", null, this.lrs.auth, callback, null, true, null, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result.response;
         }
       }
     };
     /*
      * Delete activity profile in the LRS
      * @param {string} activityid   the id of the Activity this profile is about
      * @param {string} profileid   the id you want associated with this profile
      * @param {string} [matchHash]    the hash of the profile to replace or * to replace any
      * @param {string} [noneMatchHash]    the hash of the current profile or * to indicate no previous profile
      * @param {string} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * ADL.XAPIWrapper.deleteActivityProfile("http://adlnet.gov/expapi/activities/question",
      *                                       "actprofile");
      * >> XMLHttpRequest {statusText: "NO CONTENT", status: 204, response: "", responseType: "", responseXML: null…}
      */


     XAPIWrapper.prototype.deleteActivityProfile = function (activityid, profileid, matchHash, noneMatchHash, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "activities/profile?activityId=<activity ID>&profileId=<profileid>";
         url = url.replace('<activity ID>', encodeURIComponent(activityid));
         url = url.replace('<profileid>', encodeURIComponent(profileid));
         var headers = null;

         if (matchHash && noneMatchHash) {
           log("Can't have both If-Match and If-None-Match");
         } else if (matchHash) {
           headers = {
             "If-Match": ADL.formatHash(matchHash)
           };
         } else if (noneMatchHash) {
           headers = {
             "If-None-Match": ADL.formatHash(noneMatchHash)
           };
         }

         var result = ADL.XHR_request(this.lrs, url, "DELETE", null, this.lrs.auth, callback, null, false, headers, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result;
         }
       }
     };
     /*
      * Gets the Person object from the LRS based on an agent object.
      * The Person object may contain more information about an agent.
      * See the xAPI Spec for details.
      * @param {object} agent   the agent object to get a Person
      * @param {function [callback]    function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * var res = ADL.XAPIWrapper.getAgents({"mbox":"mailto:tom@example.com"});
      * ADL.XAPIWrapper.log(res);
      * >> <Person object>
      */


     XAPIWrapper.prototype.getAgents = function (agent, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "agents?agent=<agent>";
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));
         var result = ADL.XHR_request(this.lrs, url, "GET", null, this.lrs.auth, callback, null, true, null, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result.response;
         }
       }
     };
     /*
      * Store agent profile in the LRS
      * @param {object} agent   the agent this profile is related to
      * @param {string} profileid   the id you want associated with this profile
      * @param {string} profileval   the profile
      * @param {string} [matchHash]    the hash of the profile to replace or * to replace any
      * @param {string} [noneMatchHash]    the hash of the current profile or * to indicate no previous profile
      * @param {string} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} false if no agent profile is included
      * @example
      * var profile = {"info":"the agent profile"};
      * ADL.XAPIWrapper.sendAgentProfile({"mbox":"mailto:tom@example.com"},
      *                                   "agentprofile", profile, null, "*");
      */


     XAPIWrapper.prototype.sendAgentProfile = function (agent, profileid, profileval, matchHash, noneMatchHash, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "agents/profile?agent=<agent>&profileId=<profileid>";
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));
         url = url.replace('<profileid>', encodeURIComponent(profileid));
         var headers = null;

         if (matchHash && noneMatchHash) {
           log("Can't have both If-Match and If-None-Match");
         } else if (matchHash) {
           headers = {
             "If-Match": ADL.formatHash(matchHash)
           };
         } else if (noneMatchHash) {
           headers = {
             "If-None-Match": ADL.formatHash(noneMatchHash)
           };
         }

         var method = "PUT";

         if (profileval) {
           if (profileval instanceof Array) {
             profileval = JSON.stringify(profileval);
             headers = headers || {};
             headers["Content-Type"] = "application/json";
           } else if (profileval instanceof Object) {
             profileval = JSON.stringify(profileval);
             headers = headers || {};
             headers["Content-Type"] = "application/json";
             method = "POST";
           } else {
             headers = headers || {};
             headers["Content-Type"] = "application/octet-stream";
           }
         } else {
           this.log("No agent profile was included.");
           return false;
         }

         ADL.XHR_request(this.lrs, url, method, profileval, this.lrs.auth, callback, null, false, headers, this.withCredentials, this.strictCallbacks);
       }
     };
     /*
      * Get agnet profile from the LRS
      * @param {object} agent   the agent associated with this profile
      * @param {string} [profileid]    the id of the profile, if not included, the response will be a list of profileids
      *              associated with the agent
      * @param {object} [since]    date object or date string telling the LRS to return objects newer than the date supplied
      * @param {function} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * ADL.XAPIWrapper.getAgentProfile({"mbox":"mailto:tom@example.com"},
      *                                  "agentprofile", null,
      *                                  function(r){ADL.XAPIWrapper.log(JSON.parse(r.response));});
      * >> {info: "the agent profile"}
      */


     XAPIWrapper.prototype.getAgentProfile = function (agent, profileid, since, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "agents/profile?agent=<agent>";
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));
         url = url.replace('<profileid>', encodeURIComponent(profileid));

         if (profileid) {
           url += "&profileId=" + encodeURIComponent(profileid);
         }

         if (since) {
           since = isDate(since);

           if (since != null) {
             url += '&since=' + encodeURIComponent(since.toISOString());
           }
         }

         var result = ADL.XHR_request(this.lrs, url, "GET", null, this.lrs.auth, callback, null, true, null, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result.response;
         }
       }
     };
     /*
      * Delete agent profile in the LRS
      * @param {oject} agent   the id of the Agent this profile is about
      * @param {string} profileid   the id you want associated with this profile
      * @param {string} [matchHash]    the hash of the profile to replace or * to replace any
      * @param {string} [noneMatchHash]    the hash of the current profile or * to indicate no previous profile
      * @param {string} [callback]   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      *            the function will be passed the XMLHttpRequest object
      * @return {object} xhr response object or null if 404
      * @example
      * ADL.XAPIWrapper.deleteAgentProfile({"mbox":"mailto:tom@example.com"},
      *                                     "agentprofile");
      * >> XMLHttpRequest {statusText: "NO CONTENT", status: 204, response: "", responseType: "", responseXML: null…}
      */


     XAPIWrapper.prototype.deleteAgentProfile = function (agent, profileid, matchHash, noneMatchHash, callback) {
       if (this.testConfig()) {
         var url = this.lrs.endpoint + "agents/profile?agent=<agent>&profileId=<profileid>";
         url = url.replace('<agent>', encodeURIComponent(JSON.stringify(agent)));
         url = url.replace('<profileid>', encodeURIComponent(profileid));
         var headers = null;

         if (matchHash && noneMatchHash) {
           log("Can't have both If-Match and If-None-Match");
         } else if (matchHash) {
           headers = {
             "If-Match": ADL.formatHash(matchHash)
           };
         } else if (noneMatchHash) {
           headers = {
             "If-None-Match": ADL.formatHash(noneMatchHash)
           };
         }

         var result = ADL.XHR_request(this.lrs, url, "DELETE", null, this.lrs.auth, callback, null, false, headers, this.withCredentials, this.strictCallbacks);

         if (result === undefined || result.status == 404) {
           return null;
         }

         try {
           return JSON.parse(result.response);
         } catch (e) {
           return result;
         }
       }
     };
     /*
      * Tests the configuration of the lrs object
      */


     function testConfig() {
       try {
         return this.lrs.endpoint != undefined && this.lrs.endpoint != "";
       } catch (e) {
         return false;
       }
     } // outputs the message to the console if available


     function log(message) {
       if (!log.debug) return false;

       try {
         console.log(message);
         return true;
       } catch (e) {
         return false;
       }
     } // merges two object


     function mergeRecursive(obj1, obj2) {
       for (var p in obj2) {
         if (obj2.hasOwnProperty(p) == false) continue;
         var prop = obj2[p];
         log(p + " : " + prop);

         try {
           // Property in destination object set; update its value.
           if (obj2[p].constructor == Object) {
             obj1[p] = mergeRecursive(obj1[p], obj2[p]);
           } else {
             if (obj1 == undefined) {
               obj1 = new Object();
             }

             obj1[p] = obj2[p];
           }
         } catch (e) {
           if (obj1 == undefined) {
             obj1 = new Object();
           } // Property in destination object not set; create it and set its value.


           obj1[p] = obj2[p];
         }
       }

       return obj1;
     } // iniitializes an lrs object with settings from
     // a config file and from the url query string


     function getLRSObject(config) {
       var lrsProps = ["endpoint", "auth", "actor", "registration", "activity_id", "grouping", "activity_platform"];
       var lrs = new Object();
       var qsVars, prop;
       qsVars = parseQueryString();

       if (qsVars !== undefined && Object.keys(qsVars).length !== 0) {
         for (var i = 0; i < lrsProps.length; i++) {
           prop = lrsProps[i];

           if (qsVars[prop]) {
             lrs[prop] = qsVars[prop];
             delete qsVars[prop];
           }
         } // if (Object.keys(qsVars).length !== 0) {
         //     lrs.extended = qsVars;
         // }


         lrs = mergeRecursive(config, lrs);
       } else {
         lrs = config;
       }

       return lrs;
     } // parses the params in the url query string


     function parseQueryString() {
       var qs, pairs, pair, ii, parsed;
       qs = window.location.search.substr(1);
       pairs = qs.split('&');
       parsed = {};

       for (ii = 0; ii < pairs.length; ii++) {
         pair = pairs[ii].split('=');

         if (pair.length === 2 && pair[0]) {
           parsed[pair[0]] = decodeURIComponent(pair[1]);
         }
       }

       return parsed;
     }

     function delay() {
       var xhr = new XMLHttpRequest();
       var url = window.location + '?forcenocache=' + ADL.ruuid();
       xhr.open('GET', url, false);
       xhr.send(null);
     }
     /*
      * formats a request in a way that IE will allow
      * @param {string} method   the http request method (ex: "PUT", "GET")
      * @param {string} url   the url to the request (ex: ADL.XAPIWrapper.lrs.endpoint + "statements")
      * @param {array} [headers]   headers to include in the request
      * @param {string} [data]   the body of the request, if there is one
      * @return {object} xhr response object
      */


     function ie_request(method, url, headers, data) {
       var newUrl = url; //Everything that was on query string goes into form vars

       var formData = new Array();
       var qsIndex = newUrl.indexOf('?');

       if (qsIndex > 0) {
         formData.push(newUrl.substr(qsIndex + 1));
         newUrl = newUrl.substr(0, qsIndex);
       } //Method has to go on querystring, and nothing else


       newUrl = newUrl + '?method=' + method; //Headers

       if (headers !== null) {
         for (var headerName in headers) {
           if (headers.hasOwnProperty(headerName)) formData.push(headerName + "=" + encodeURIComponent(headers[headerName]));
         }
       } //The original data is repackaged as "content" form var


       if (data !== null) {
         formData.push('content=' + encodeURIComponent(data));
       }

       return {
         "method": "POST",
         "url": newUrl,
         "headers": {},
         "data": formData.join("&")
       };
     }
     /*!
     Excerpt from: Math.uuid.js (v1.4)
     http://www.broofa.com
     mailto:robert@broofa.com
     Copyright (c) 2010 Robert Kieffer
     Dual licensed under the MIT and GPL licenses.
     */


     ADL.ruuid = function () {
       return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
         var r = Math.random() * 16 | 0,
             v = c == 'x' ? r : r & 0x3 | 0x8;
         return v.toString(16);
       });
     };
     /*
      * dateFromISOString
      * parses an ISO string into a date object
      * isostr - the ISO string
      */


     ADL.dateFromISOString = function (isostr) {
       var regexp = "([0-9]{4})(-([0-9]{2})(-([0-9]{2})" + "([T| ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?" + "(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?";
       var d = isostr.match(new RegExp(regexp));
       var offset = 0;
       var date = new Date(d[1], 0, 1);

       if (d[3]) {
         date.setMonth(d[3] - 1);
       }

       if (d[5]) {
         date.setDate(d[5]);
       }

       if (d[7]) {
         date.setHours(d[7]);
       }

       if (d[8]) {
         date.setMinutes(d[8]);
       }

       if (d[10]) {
         date.setSeconds(d[10]);
       }

       if (d[12]) {
         date.setMilliseconds(Number("0." + d[12]) * 1000);
       }

       if (d[14]) {
         offset = Number(d[16]) * 60 + Number(d[17]);
         offset *= d[15] == '-' ? 1 : -1;
       }

       offset -= date.getTimezoneOffset();
       time = Number(date) + offset * 60 * 1000;
       var dateToReturn = new Date();
       dateToReturn.setTime(Number(time));
       return dateToReturn;
     }; // Synchronous if callback is not provided (not recommended)

     /*
      * makes a request to a server (if possible, use functions provided in XAPIWrapper)
      * @param {string} lrs   the lrs connection info, such as endpoint, auth, etc
      * @param {string} url   the url of this request
      * @param {string} method   the http request method
      * @param {string} data   the payload
      * @param {string} auth   the value for the Authorization header
      * @param {function} callback   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      * @param {object} [callbackargs]   additional javascript object to be passed to the callback function
      * @param {boolean} ignore404    allow page not found errors to pass
      * @param {object} extraHeaders   other header key-values to be added to this request
      * @param {boolean} withCredentials
      * @param {boolean} strictCallbacks Callback must be executed and first param is error or null if no error
      * @return {object} xhr response object
      */


     ADL.XHR_request = function (lrs, url, method, data, auth, callback, callbackargs, ignore404, extraHeaders, withCredentials, strictCallbacks) {

       var xhr,
           finished = false,
           xDomainRequest = false,
           ieXDomain = false,
           ieModeRequest,
           urlparts = url.toLowerCase().match(/^(.+):\/\/([^:\/]*):?(\d+)?(\/.*)?$/),
           location = window.location,
           urlPort,
           result,
           extended,
           prop,
           until; //Consolidate headers

       var headers = {};
       headers["Content-Type"] = "application/json";
       headers["Authorization"] = auth;
       headers['X-Experience-API-Version'] = ADL.XAPIWrapper.xapiVersion;

       if (extraHeaders !== null) {
         for (var headerName in extraHeaders) {
           if (extraHeaders.hasOwnProperty(headerName)) headers[headerName] = extraHeaders[headerName];
         }
       } //See if this really is a cross domain


       xDomainRequest = location.protocol.toLowerCase() !== urlparts[1] || location.hostname.toLowerCase() !== urlparts[2];

       if (!xDomainRequest) {
         urlPort = urlparts[3] === null ? urlparts[1] === 'http' ? '80' : '443' : urlparts[3];
         xDomainRequest = urlPort === location.port;
       } //Add extended LMS-specified values to the URL


       if (lrs !== null && lrs.extended !== undefined) {
         extended = new Array();

         for (prop in lrs.extended) {
           extended.push(prop + "=" + encodeURIComponent(lrs.extended[prop]));
         }

         if (extended.length > 0) {
           url += (url.indexOf("?") > -1 ? "&" : "?") + extended.join("&");
         }
       } //If it's not cross domain or we're not using IE, use the usual XmlHttpRequest


       var windowsVersionCheck = window.XDomainRequest && window.XMLHttpRequest && new XMLHttpRequest().responseType === undefined;

       if (!xDomainRequest || windowsVersionCheck === undefined || windowsVersionCheck === false) {
         xhr = new XMLHttpRequest();
         xhr.withCredentials = withCredentials; //allow cross domain cookie based auth

         xhr.open(method, url, callback != null);

         for (var headerName in headers) {
           xhr.setRequestHeader(headerName, headers[headerName]);
         }
       } //Otherwise, use IE's XDomainRequest object
       else {
           ieXDomain = true;
           ieModeRequest = ie_request(method, url, headers, data);
           xhr = new XDomainRequest();
           xhr.open(ieModeRequest.method, ieModeRequest.url);
         } //Setup request callback


       function requestComplete() {
         if (!finished) {
           // may be in sync or async mode, using XMLHttpRequest or IE XDomainRequest, onreadystatechange or
           // onload or both might fire depending upon browser, just covering all bases with event hooks and
           // using 'finished' flag to avoid triggering events multiple times
           finished = true;
           var notFoundOk = ignore404 && xhr.status === 404;

           if (xhr.status === undefined || xhr.status >= 200 && xhr.status < 400 || notFoundOk) {
             if (callback) {
               if (callbackargs) {
                 strictCallbacks ? callback(null, xhr, callbackargs) : callback(xhr, callbackargs);
               } else {
                 var body;

                 try {
                   body = JSON.parse(xhr.responseText);
                 } catch (e) {
                   body = xhr.responseText;
                 }

                 strictCallbacks ? callback(null, xhr, body) : callback(xhr, body);
               }
             } else {
               result = xhr;
               return xhr;
             }
           } else {
             var warning;

             try {
               warning = "There was a problem communicating with the Learning Record Store. ( " + xhr.status + " | " + xhr.response + " )" + url;
             } catch (ex) {
               warning = ex.toString();
             }

             ADL.XAPIWrapper.log(warning);
             ADL.xhrRequestOnError(xhr, method, url, callback, callbackargs, strictCallbacks);
             result = xhr;
             return xhr;
           }
         } else {
           return result;
         }
       }

       xhr.onreadystatechange = function () {
         if (xhr.readyState === 4) {
           return requestComplete();
         }
       };

       xhr.onload = requestComplete;
       xhr.onerror = requestComplete; //xhr.onerror =  ADL.xhrRequestOnError(xhr, method, url);

       xhr.send(ieXDomain ? ieModeRequest.data : data);

       if (!callback) {
         // synchronous
         if (ieXDomain) {
           // synchronous call in IE, with no asynchronous mode available.
           until = 1000 + new Date();

           while (new Date() < until && xhr.readyState !== 4 && !finished) {
             delay();
           }
         }

         return requestComplete();
       }
     };
     /*
      * Holder for custom global error callback
      * @param {object} xhr   xhr object or null
      * @param {string} method   XMLHttpRequest request method
      * @param {string} url   full endpoint url
      * @param {function} callback   function to be called after the LRS responds
      *            to this request (makes the call asynchronous)
      * @param {object} [callbackargs]   additional javascript object to be passed to the callback function
      * @param {boolean} strictCallbacks Callback must be executed and first param is error or null if no error
      * @example
      * ADL.xhrRequestOnError = function(xhr, method, url, callback, callbackargs) {
      *   console.log(xhr);
      *   alert(xhr.status + " " + xhr.statusText + ": " + xhr.response);
      * };
      */


     ADL.xhrRequestOnError = function (xhr, method, url, callback, callbackargs, strictCallbacks) {
       if (callback && strictCallbacks) {
         var status = xhr ? xhr.status : undefined;
         var error;

         if (status) {
           error = new Error('Request error: ' + xhr.status);
         } else if (status === 0 || status === null) {
           // 0 and null = aborted
           error = new Error('Request error: aborted');
         } else {
           error = new Error('Reqeust error: unknown');
         }

         if (callbackargs) {
           callback(error, xhr, callbackargs);
         } else {
           var body;

           try {
             body = JSON.parse(xhr.responseText);
           } catch (e) {
             body = xhr.responseText;
           }

           callback(error, xhr, body);
         }
       }
     };

     ADL.formatHash = function (hash) {
       return hash === "*" ? hash : '"' + hash + '"';
     };

     ADL.XAPIWrapper = new XAPIWrapper(Config, false);
   }

   let ADL$1;
   function _ADL() {
     if (ADL$1) return ADL$1;
     ADL$1 = {};
     setup(ADL$1);
     setup$1(ADL$1);
     setup$2(ADL$1);
     setup$3(ADL$1);
     setup$4(ADL$1);
     setup$5(ADL$1);
     return ADL$1;
   }

   /** ***********************************************************************
   *
   * Veracity Technology Consultants CONFIDENTIAL
   * __________________
   *
   *  2019 Veracity Technology Consultants
   *  All Rights Reserved.
   *
   * NOTICE:  All information contained herein is, and remains
   * the property of Veracity Technology Consultants and its suppliers,
   * if any.  The intellectual and technical concepts contained
   * herein are proprietary to Veracity Technology Consultants
   * and its suppliers and may be covered by U.S. and Foreign Patents,
   * patents in process, and are protected by trade secret or copyright law.
   * Dissemination of this information or reproduction of this material
   * is strictly forbidden unless prior written permission is obtained
   * from Veracity Technology Consultants.
   */
   // VERB CONSTANTS
   const INITIALIZED = 'initialized';
   const COMPLETED = 'completed';
   const PASSED = 'passed';
   const FAILED = 'failed';
   const TERMINATED = 'terminated';
   const SUSPENDED = 'suspended';
   const RESUMED = 'resumed';
   const RESPONDED = 'responded';
   const SCORED = 'scored';
   const BOOKMARKED = 'bookmarked';
   const VIEWED = "viewed";
   const ACCESSED = "accessed"; // INTERACTION TYPE CONSTANTS

   const TRUE_FALSE = 'true-false';
   const CHOICE = 'choice';
   const CHOICE_WITH_EXPLANATION = 'choice-with-explanation';
   const FILL_IN = 'fill-in';
   const LONG_FILL_IN = 'long-fill-in';
   const MATCHING = 'matching';
   const PERFORMANCE = 'performance';
   const SEQUENCING = 'sequencing';
   const LIKERT = 'likert';
   const NUMERIC = 'numeric';
   const OTHER = 'other';
   const OTHER_UPLOAD = 'other-upload';
   const OTHER_UPLOAD_ATTACHMENT = 'other-attachment'; // SCORE TYPES

   const SCALED = 'scaled';
   const RAW = 'raw';
   const MIN = 'min';
   const MAX = 'max'; // ACTIVITY TYPES

   const LESSON = 'http://adlnet.gov/expapi/activities/lesson';
   const COURSE = 'http://adlnet.gov/expapi/activities/course';
   const ASSESSMENT = 'http://adlnet.gov/expapi/activities/assessment';
   const PAGE = 'https://w3id.org/xapi/acrossx/activities/page';
   const PROFILE = 'http://adlnet.gov/expapi/activities/profile';
   const SECTION = 'https://w3id.org/xapi/netc-e-learning/activity-types/section'; // EXTENSIONS

   const SCHOOL_CENTER = 'https://w3id.org/xapi/netc/extensions/school-center';
   const LAUNCH_LOCATION = 'https://w3id.org/xapi/navy/extensions/launch-location';
   const USER_AGENT = 'https://w3id.org/xapi/netc/extensions/user-agent';
   const RESPONSE_EXPLANATION = 'https://w3id.org/xapi/netc-assessment/extensions/result/response-explanation';
   const RESPONSE_TYPE = 'https://w3id.org/xapi/netc-assessment/extensions/result/response-type'; // ERRORS

   const INVALID_ACTIVITY_TYPE = 'Invalid activity type. Activity type must be either "course" or "lesson".';
   const COURSE_INITIALIZE_SUCCEED_LESSON_ERROR = 'You must initialize a course before initializing a lesson.';
   const NO_COURSE_REGISTRATION_ID_ERROR = 'No registration id for this course attempt.';
   const NO_LESSON_REGISTRATION_ID_ERROR = 'No registration id is associated with this attempt. You must call initialize() before suspending an attempt.';
   const ATTEMPT_TERMINATED_ERROR = "This attempt has been terminated and can't be resumed.";
   const NO_SUSPENDED_DATA_ERROR = "There's no suspended data for this attempt.";
   const COMPLETED_NOT_SET_ERROR = 'completed is undefined';
   const COMPLETED_NOT_TYPE_BOOLEAN_ERROR = 'completed must be type boolean.';
   const SUCCESS_NOT_SET_ERROR = 'success is undefined';
   const SUCCESS_NOT_TYPE_BOOLEAN_ERROR = 'success must be type boolean.';
   const TYPE_NOT_SET_ERROR = 'type is undefined';
   const TYPE_NOT_TYPE_STRING_ERROR = 'type must be a string.';
   const SCORE_NOT_SET_ERROR = 'score is undefined';
   const SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR = 'score must be type string or number.';
   const INTERACTION_TYPE_NOT_SET_ERROR = 'You must provided an "interactionType" value.';
   const ID_NOT_SET_ERROR = 'You must provided an "id" value.';
   const CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR = 'correctResponsesPattern has to be an array.';
   const RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR = 'Response Pattern has to be an array.';
   const CHOICES_NOT_TYPE_ARRAY_ERROR = 'choices has to be an array.';
   const SCALE_NOT_TYPE_ARRAY_ERROR = 'scale has to be an array.';
   const SOURCE_NOT_TYPE_ARRAY_ERROR = 'source has to be an array.';
   const TARGET_NOT_TYPE_ARRAY_ERROR = 'target has to be an array.';
   const STEPS_NOT_TYPE_ARRAY_ERROR = 'steps has to be an array.';
   const RESPONSE_VALUE_UNDEFINED_ERROR = 'A response value is required.';
   const INTERACTION_UNDEFINED_ERROR = 'The interaction argument is required.';
   const INTERACTION_NOT_TYPE_OBJECT_ERROR = 'The interaction argument is not an object.';
   const INTERACTION_ID_UNDEFINED_ERROR = 'The interaction property id is required.';
   const INTERACTION_TYPE_UNDEFINED_ERROR = 'The interaction property interactionType is required.';
   const PARENT_UNDEFINED_ERROR = 'The parent argument is required.';
   const PARENT_NOT_TYPE_OBJECT_ERROR = 'The parent argument is not an object.';
   const ACTIVITY_ID_UNDEFINED_ERROR = 'Activity id required.';
   const ACTIVITY_ID_NOT_TYPE_STRING_ERROR = 'Activity id is not a string.';
   const ACTIVITY_NAME_UNDEFINED_ERROR = 'Activity name is required.';
   const ACTIVITY_NAME_NOT_TYPE_STRING_ERROR = 'Activity name is not a string';
   const ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR = 'Activity description is not a string';
   const GROUP_ID_UNDEFINED_ERROR = 'Group id required.';
   const GROUP_ID_NOT_TYPE_STRING_ERROR = 'Group id is not a string.';
   const GROUP_NAME_UNDEFINED_ERROR = 'Group name is required.';
   const GROUP_NAME_NOT_TYPE_STRING_ERROR = 'Group name is not a string';
   const GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR = 'Group description is not a string';
   const EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR = 'key must be either a string or an object.';
   const EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR = 'value must be either a string or an object.';
   const PARENT_ID_UNDEFINED_ERROR = 'parent id is required.';
   const LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR = 'launch parameters or configuration object required.';
   const ACTIVITY_AND_GROUP_OBJECT_REQUIRED_ERROR = 'You must create a statement by using setActivity() and setGroup().';
   const ACTIVITY_OBJECT_REQUIRED_ERROR = 'An activity is required to be able to send statements. Use setActivity() to add an activity';
   const GROUP_OBJECT_REQUIRED_ERROR = 'A group is required to be able to send statements. Use setGroup() to add a group';
   const COURSE_IS_TERMINATED_ERROR = 'This course attempt has been terminated. Create a new course attempt by calling initialize().';
   const LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR = 'This lesson attempt needs to be initialized before resuming. Create a new lesson attempt by calling initialize().';
   const LESSON_ATTEMPT_TERMINATED = 'This lesson attempt has been terminated. Create a new lesson attempt by calling initialize().';
   const LESSON_ATTEMPT_NOT_SUSPENDED_ERROR = 'This lesson attempt has not been suspended. Calling suspend() will suspend this lesson.';
   const EMPTY_ACTIVITY_STATE_ERROR = 'You must call initialize on a course or lesson before terminating or resume from a suspended lesson.';
   const LESSON_ATTEMPT_NOT_FOUND = 'Lesson attempt not found.';
   const OTHER_RESPONSE_NOT_PROVIDED_ERROR = 'You must provide a value for "otherResponse" when using the "CHOICE_WITH_EXPLANATION" interaction type.';
   const ATTACHMENT_NOT_PROVIDED_ERROR = 'You must provide an "attachment" value when using "OTHER_UPLOAD_ATTACHMENT interaction type';
   const EXTENSION_KEY_NOT_PROVIDED_ERROR = 'You must provide a "key" value.';
   const ATTACHMENT_PARAM_NOT_AN_ARRAY = 'You must provide an array for attachment value.';
   const OTHER_RESPONSE_NOT_PROVIDED = 'You must provide a "otherResponse" value';
   const INVALID_INTERACTION_TYPE_COMPONENT_LIST = "Invalid interactionType. This interaction type most likely dosen't need a component list.";
   const INVALID_MATCHING_PERFORMANCE_RESPONSE = 'matching and performance response pattern must be in the correct format.';
   const ERROR = {
     INVALID_ACTIVITY_TYPE,
     COURSE_INITIALIZE_SUCCEED_LESSON_ERROR,
     NO_COURSE_REGISTRATION_ID_ERROR,
     NO_LESSON_REGISTRATION_ID_ERROR,
     ATTEMPT_TERMINATED_ERROR,
     NO_SUSPENDED_DATA_ERROR,
     COMPLETED_NOT_SET_ERROR,
     COMPLETED_NOT_TYPE_BOOLEAN_ERROR,
     SUCCESS_NOT_SET_ERROR,
     SUCCESS_NOT_TYPE_BOOLEAN_ERROR,
     TYPE_NOT_SET_ERROR,
     TYPE_NOT_TYPE_STRING_ERROR,
     SCORE_NOT_SET_ERROR,
     SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR,
     INTERACTION_TYPE_NOT_SET_ERROR,
     ID_NOT_SET_ERROR,
     CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR,
     INVALID_INTERACTION_TYPE_COMPONENT_LIST,
     RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR,
     CHOICES_NOT_TYPE_ARRAY_ERROR,
     SCALE_NOT_TYPE_ARRAY_ERROR,
     SOURCE_NOT_TYPE_ARRAY_ERROR,
     TARGET_NOT_TYPE_ARRAY_ERROR,
     STEPS_NOT_TYPE_ARRAY_ERROR,
     RESPONSE_VALUE_UNDEFINED_ERROR,
     INTERACTION_UNDEFINED_ERROR,
     INTERACTION_NOT_TYPE_OBJECT_ERROR,
     INTERACTION_ID_UNDEFINED_ERROR,
     INTERACTION_TYPE_UNDEFINED_ERROR,
     PARENT_UNDEFINED_ERROR,
     PARENT_NOT_TYPE_OBJECT_ERROR,
     ACTIVITY_ID_UNDEFINED_ERROR,
     ACTIVITY_ID_NOT_TYPE_STRING_ERROR,
     ACTIVITY_NAME_UNDEFINED_ERROR,
     ACTIVITY_NAME_NOT_TYPE_STRING_ERROR,
     ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR,
     GROUP_ID_UNDEFINED_ERROR,
     GROUP_ID_NOT_TYPE_STRING_ERROR,
     GROUP_NAME_UNDEFINED_ERROR,
     GROUP_NAME_NOT_TYPE_STRING_ERROR,
     GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR,
     EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR,
     EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR,
     PARENT_ID_UNDEFINED_ERROR,
     LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR,
     ACTIVITY_AND_GROUP_OBJECT_REQUIRED_ERROR,
     ACTIVITY_OBJECT_REQUIRED_ERROR,
     GROUP_OBJECT_REQUIRED_ERROR,
     COURSE_IS_TERMINATED_ERROR,
     LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR,
     LESSON_ATTEMPT_TERMINATED,
     LESSON_ATTEMPT_NOT_SUSPENDED_ERROR,
     EMPTY_ACTIVITY_STATE_ERROR,
     LESSON_ATTEMPT_NOT_FOUND,
     OTHER_RESPONSE_NOT_PROVIDED_ERROR,
     ATTACHMENT_NOT_PROVIDED_ERROR,
     EXTENSION_KEY_NOT_PROVIDED_ERROR,
     ATTACHMENT_PARAM_NOT_AN_ARRAY,
     OTHER_RESPONSE_NOT_PROVIDED,
     INVALID_MATCHING_PERFORMANCE_RESPONSE
   };
   const EXTENSIONS = {
     SCHOOL_CENTER,
     LAUNCH_LOCATION,
     USER_AGENT,
     RESPONSE_EXPLANATION,
     RESPONSE_TYPE
   };
   const VERB = {
     INITIALIZED,
     COMPLETED,
     PASSED,
     FAILED,
     TERMINATED,
     SUSPENDED,
     RESUMED,
     RESPONDED,
     SCORED,
     BOOKMARKED,
     VIEWED,
     ACCESSED
   };
   const INTERACTION = {
     TRUE_FALSE,
     CHOICE,
     CHOICE_WITH_EXPLANATION,
     FILL_IN,
     LONG_FILL_IN,
     MATCHING,
     PERFORMANCE,
     SEQUENCING,
     LIKERT,
     NUMERIC,
     OTHER,
     OTHER_UPLOAD,
     OTHER_UPLOAD_ATTACHMENT
   };
   const SCORE = {
     SCALED,
     RAW,
     MIN,
     MAX
   };
   const ACTIVITY_TYPES = {
     LESSON,
     COURSE,
     ASSESSMENT,
     SECTION,
     PROFILE,
     PAGE
   };
   var ACTIVITY;

   (function (ACTIVITY) {
     ACTIVITY[ACTIVITY["COURSE"] = 0] = "COURSE";
     ACTIVITY[ACTIVITY["LESSON"] = 1] = "LESSON";
     ACTIVITY[ACTIVITY["ASSESSMENT"] = 2] = "ASSESSMENT";
   })(ACTIVITY || (ACTIVITY = {}));

   // constants
   // initialized, suspend, resume,
   // complete, score, passed, failed,
   // terminated, responded
   // only initialized and terminated course doesn't use the scorm profile

   const isObject = o => (o == null ? void 0 : o.constructor) === Object;
   const getVerbID = verb => {
     switch (verb) {
       case VERB.INITIALIZED:
       case VERB.COMPLETED:
       case VERB.PASSED:
       case VERB.FAILED:
       case VERB.TERMINATED:
       case VERB.SUSPENDED:
       case VERB.RESUMED:
       case VERB.RESPONDED:
       case VERB.SCORED:
       case VERB.BOOKMARKED:
         return `http://adlnet.gov/expapi/verbs/${verb}`;

       case VERB.VIEWED:
         return `http://id.tincanapi.com/verb/${verb}`;

       case VERB.ACCESSED:
         return `https://w3id.org/xapi/performance-support/verbs/${verb}`;

       default:
         return `http://adlnet.gov/expapi/verbs/${verb}`;
     }
   };
   const parseLaunchParams = (parsedURL, baseconf) => {
     const sp = parsedURL.search;
     const params = new URLSearchParams(sp);

     if (!baseconf) {
       baseconf = {};
     }

     if (params.get("endpoint")) {
       baseconf["endpoint"] = params.get("endpoint");
     }

     if (params.get("auth")) {
       baseconf["auth"] = params.get("auth");
     }

     if (params.get("actor")) {
       baseconf["actor"] = params.get("actor");
     }

     if (params.get("registration")) {
       baseconf["registration"] = params.get("registration");
     }

     return baseconf;
   };
   const ruuid = () => {
     return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
       var r = Math.random() * 16 | 0,
           v = c == "x" ? r : r & 0x3 | 0x8;
       return v.toString(16);
     });
   };

   /** ***********************************************************************
   *
   * Veracity Technology Consultants CONFIDENTIAL
   * __________________
   *
   *  2019 Veracity Technology Consultants
   *  All Rights Reserved.
   *
   * NOTICE:  All information contained herein is, and remains
   * the property of Veracity Technology Consultants and its suppliers,
   * if any.  The intellectual and technical concepts contained
   * herein are proprietary to Veracity Technology Consultants
   * and its suppliers and may be covered by U.S. and Foreign Patents,
   * patents in process, and are protected by trade secret or copyright law.
   * Dissemination of this information or reproduction of this material
   * is strictly forbidden unless prior written permission is obtained
   * from Veracity Technology Consultants.
   */

   const addActor = (stmt, {
     actor
   }) => {
     return { ...stmt,
       actor
     };
   };

   const addVerb = (statement, {
     verb
   }) => {
     // Don't mutate original source, so spread into a new object
     let stmt = { ...statement
     };
     stmt.verb = {
       id: getVerbID(verb),
       display: {
         en: verb
       }
     };
     return stmt;
   };

   const addObject = (stmt, {
     activityType,
     activity
   }) => {
     return { ...stmt,
       object: { ...activity,
         definition: { ...activity.definition,
           type: activityType
         }
       }
     };
   };

   const addContext = (stmt, {
     registration
   }) => {
     let context = {
       contextActivities: {},
       registration
     };
     return { ...stmt,
       context: context
     };
   };

   const addParent = (stmt, {
     activityType,
     parent
   }) => {
     let p = [{ ...parent
     }];
     return { ...stmt,
       context: { ...stmt.context,
         contextActivities: { ...stmt.context.contextActivities,
           parent: p
         }
       }
     };
   }; // make category configurable


   const addCategory = (stmt, {
     isScorm,
     interaction
   }) => {
     let category = []; // default category

     category.push({
       id: "https://w3id.org/xapi/netc/v1.0",
       definition: {
         type: "http://adlnet.gov/expapi/activities/profile"
       }
     });
     category.push({
       id: "https://w3id.org/xapi/netc-e-learning/v1.0",
       definition: {
         type: "http://adlnet.gov/expapi/activities/profile"
       }
     });

     if (isScorm) {
       category.push({
         id: "https://w3id.org/xapi/scorm",
         definition: {
           type: "http://adlnet.gov/expapi/activities/profile"
         }
       });
     }

     if (interaction || stmt.object.type === ACTIVITY_TYPES.ASSESSMENT) {
       category.push({
         id: "https://w3id.org/xapi/netc-assessment/v1.0",
         definition: {
           type: "http://adlnet.gov/expapi/activities/profile"
         }
       });
     }

     return { ...stmt,
       context: { ...stmt.context,
         contextActivities: {
           category
         }
       }
     };
   };

   const addGrouping = (stmt, {
     activityType,
     group
   }) => {
     let grouping = [{ ...group,
       definition: { ...group.definition,
         type: activityType
       }
     }];
     return { ...stmt,
       context: { ...stmt.context,
         contextActivities: { ...stmt.context.contextActivities,
           grouping
         }
       }
     };
   };

   const addInteraction = (statement, {
     activity,
     interaction
   }) => {
     const stmt = { ...statement
     };
     const {
       id,
       interactionType,
       definition
     } = interaction;
     stmt.object = {
       id,
       definition: { ...definition,
         // spread the rest of the possible options
         type: "http://adlnet.gov/expapi/activities/cmi.interaction",
         interactionType
       }
     }; // current activity is now part of grouping since the interaction
     // is now the object

     if (stmt.context.contextActivities.grouping) {
       stmt.context = { ...stmt.context,
         contextActivities: { ...stmt.context.contextActivities,
           grouping: [...stmt.context.contextActivities.grouping, { ...activity,
             definition: { ...activity.definition,
               type: "http://adlnet.gov/expapi/activities/lesson"
             }
           }]
         }
       };
     }

     return stmt;
   };

   const addGranular = (statement, {
     activity,
     object,
     objectType
   }) => {
     const stmt = { ...statement
     };
     const {
       id,
       definition
     } = object;
     stmt.object = {
       id,
       definition: { ...definition,
         // spread the rest of the possible options
         type: objectType
       }
     }; // current activity is now part of grouping since the interaction
     // is now the object

     if (stmt.context.contextActivities.grouping) {
       stmt.context = { ...stmt.context,
         contextActivities: { ...stmt.context.contextActivities,
           grouping: [...stmt.context.contextActivities.grouping, { ...activity,
             definition: { ...activity.definition,
               type: "http://adlnet.gov/expapi/activities/lesson"
             }
           }]
         }
       };
     }

     return stmt;
   }; // can't be added to statement? Can only be added to the wrapper sendstatement method

   const addTimestamp = stmt => {
     return { ...stmt,
       timestamp: new Date().toISOString()
     };
   };

   const addResult = (stmt, {
     result
   }) => {
     return { ...stmt,
       result
     };
   };

   const addExtensions = (stmt, {
     extensions
   }) => {
     return { ...stmt,
       context: { ...stmt.context,
         extensions
       }
     };
   };

   const addID = stmt => {
     return { ...stmt,
       id: ruuid()
     };
   };

   const addPlatform = (stmt, {
     platform
   }) => {
     return { ...stmt,
       context: { ...stmt.context,
         platform
       }
     };
   };

   const generateStatement = config => {
     let statement = buildPipeline(config).reduce((statement, func) => func(statement, config), {});
     return statement;
   }; // pipe of life

   const buildPipeline = config => {
     const {
       group,
       parent = null,
       result = null,
       interaction = null,
       platform,
       object
     } = config;
     let pipeline = [addActor, addVerb, addObject, addContext, addCategory, addExtensions, addID, addTimestamp];

     if (group) {
       pipeline.push(addGrouping);
     }

     if (parent) {
       pipeline.push(addParent);
     }

     if (result) {
       pipeline.push(addResult);
     }

     if (interaction) {
       pipeline.push(addInteraction);
     }

     if (platform) {
       pipeline.push(addPlatform);
     }

     if (object) {
       pipeline.push(addGranular);
     }

     return pipeline;
   };

   /** ***********************************************************************
    *
    * Veracity Technology Consultants CONFIDENTIAL
    * __________________
    *
    *  2019 Veracity Technology Consultants
    *  All Rights Reserved.
    *
    * NOTICE:  All information contained herein is, and remains
    * the property of Veracity Technology Consultants and its suppliers,
    * if any.  The intellectual and technical concepts contained
    * herein are proprietary to Veracity Technology Consultants
    * and its suppliers and may be covered by U.S. and Foreign Patents,
    * patents in process, and are protected by trade secret or copyright law.
    * Dissemination of this information or reproduction of this material
    * is strictly forbidden unless prior written permission is obtained
    * from Veracity Technology Consultants.
    */
   class ELearningXAPI {
     // private _isStandAlone : boolean = false;

     /**
      * @constructor
      * @param {Configuration} config - A configuration object that contains auth credentials to an lrs endpoint
      */
     constructor(config) {
       this._ADL = null;
       this._config = null; // statement object

       this._statementObject = {};
       this._statement = {};
       this._extensions = {};
       this._activityID = null;
       this._platform = '';
       /**
        * Store initialize value to true for a Course or lesson attempt activity state and send an initialized statement to the configured LRS.
        * @param {string} activityType
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */

       this.initialize = async activityType => {
         const {
           activity: {
             id
           }
         } = this._statementObject;
         const registration = ruuid();
         let stmt, validStatementObject;

         if (activityType !== ACTIVITY.COURSE && activityType !== ACTIVITY.LESSON) {
           throw Error(ERROR.INVALID_ACTIVITY_TYPE);
         } // An Initialized Statement for a course SHALL precede the Initialized Statement
         // for a lesson if a lesson is part of a broader course


         if (this._lessonRegistration && activityType === ACTIVITY.COURSE) {
           throw Error(ERROR.COURSE_INITIALIZE_SUCCEED_LESSON_ERROR);
         }

         if (!this._courseRegistration && activityType === ACTIVITY.LESSON) {
           throw Error(ERROR.COURSE_INITIALIZE_SUCCEED_LESSON_ERROR);
         }

         if (activityType === ACTIVITY.COURSE) {
           validStatementObject = this.validateCourseActivity(this._statementObject);
         } else {
           validStatementObject = this.validateLessonActivity(this._statementObject);
         }

         if (activityType === ACTIVITY.COURSE && validStatementObject) {
           // save current course session
           this.courseRegistration = registration;
           this._activityState = this.generateInitialCourseAttemptState({
             registration: registration,
             initialized: true
           }); // start building the statement stmt

           stmt = this.buildConfiguration(VERB.INITIALIZED, this.courseRegistration, { ...this._statementObject,
             activityType: ACTIVITY_TYPES.COURSE,
             activity: this._statementObject.group,
             group: undefined
           });
         } else if (validStatementObject) {
           // save current session lesson registration for suspended data
           this.lessonRegistration = registration; // start building the statement stmt

           stmt = this.buildConfiguration(VERB.INITIALIZED, this.lessonRegistration, { ...this._statementObject,
             activityType: ACTIVITY_TYPES.LESSON
           });
         }

         if (this.courseRegistration && this.lessonRegistration) {
           const state = await this.getActivityState(this.activityID).catch(error => console.warn(error));
           const initLessonAttempt = this.generateInitialLessonAttemptState({
             registration: this.lessonRegistration,
             initialized: true
           });
           this._activityState = this.addLessonAttempt(state, id, initLessonAttempt);
           this._currentLessonAttempt = initLessonAttempt;
         }

         await this.setActivityState(this.activityID, this._activityState).catch(error => console.warn(error)); // send statement    

         return this.sendStatement(stmt);
       };
       /**
        * Suspend an attempt using activity state and send a suspend statement to the configured LRS.
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.suspend = async () => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject); // update current lesson state

         this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt, {
           suspended: true
         }); // update course with new update lesson state

         this._activityState = this.updateCourseActivityState(this._activityState, ACTIVITY.LESSON, this._currentLessonAttempt); //save state

         await this.setActivityState(this.activityID, this._activityState).catch(error => console.warn(error)); // build and send statement

         let stmt = this.buildConfiguration(VERB.SUSPENDED, this.lessonRegistration, { ...this._statementObject,
           objectType: ACTIVITY_TYPES.LESSON,
           activityType: ACTIVITY_TYPES.COURSE
         });
         return this.sendStatement(stmt);
       };
       /**
        * Resume an attempt using stored activity state and send a resume statement to the configured LRS..
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.resume = async () => {
         var _this$_activityState, _this$_currentLessonA4;

         const {
           activity: {
             id
           }
         } = this._statementObject;
         const state = await this.getActivityState(this._statementObject.group.id).catch(() => {
           throw Error(ERROR.LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR);
         });
         this._activityState = { ...state
         }; // course resume validation check

         if ((_this$_activityState = this._activityState) != null && _this$_activityState.hasOwnProperty('lessons')) {
           var _this$_currentLessonA, _this$_currentLessonA2;

           if (state.terminated) {
             throw Error(ERROR.COURSE_IS_TERMINATED_ERROR);
           }

           if (!this._activityState['lessons'].hasOwnProperty(id)) {
             throw Error(ERROR.LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR);
           } // looks like this lesson has been attempted, so lets see if it has been terminated 


           this._currentLessonAttempt = this._activityState['lessons'][id][state.lessons[id].length - 1];

           if ((_this$_currentLessonA = this._currentLessonAttempt) != null && _this$_currentLessonA.terminated) {
             throw Error(ERROR.LESSON_ATTEMPT_TERMINATED);
           }

           if (!((_this$_currentLessonA2 = this._currentLessonAttempt) != null && _this$_currentLessonA2.suspended)) {
             throw Error(ERROR.LESSON_ATTEMPT_NOT_SUSPENDED_ERROR);
           }
         } else {
           var _this$_currentLessonA3;

           // stand alone lesson validation check
           // looks like this lesson has been attempted, so lets see if it has been terminated
           this._currentLessonAttempt = this._activityState['attempts'][id][state.lessons[id].length - 1];

           if ((_this$_currentLessonA3 = this._currentLessonAttempt) != null && _this$_currentLessonA3.terminated) {
             throw Error(ERROR.LESSON_ATTEMPT_TERMINATED);
           }
         }

         this.courseRegistration = state.registration;
         this.lessonRegistration = (_this$_currentLessonA4 = this._currentLessonAttempt) == null ? void 0 : _this$_currentLessonA4.registration;
         let stmt = this.buildConfiguration(VERB.RESUMED, this.lessonRegistration, { ...this._statementObject,
           objectType: ACTIVITY_TYPES.LESSON,
           activityType: ACTIVITY_TYPES.COURSE
         });
         return this.sendStatement(stmt);
       };
       /**
        * Set terminated value to true for a Course or Lesson attempt activity state and send an terminated statement to the configured LRS.
        * @param {string} activityType
        * @param {string} duration
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.terminate = async (activityType, duration) => {
         // validate _statementObject
         this.validateLessonActivity(this._statementObject);

         if (!this._activityState) {
           throw Error(ERROR.EMPTY_ACTIVITY_STATE_ERROR);
         }

         let stmt;

         if (activityType === ACTIVITY.COURSE) {
           this.updateCourseActivityState(this._activityState, ACTIVITY.COURSE, {
             terminated: true
           });
           stmt = this.buildConfiguration(VERB.TERMINATED, this._courseRegistration, { ...this._statementObject,
             objectType: ACTIVITY_TYPES.COURSE,
             // old switcheroo since a course attempt is the activity
             activity: this._statementObject.group,
             group: undefined,
             duration
           });
           this._courseRegistration = null;
         } else {
           var _this$_currentLessonA5;

           this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt, {
             terminated: true
           });
           this._activityState = this.updateCourseActivityState(this._activityState, ACTIVITY.LESSON, this._currentLessonAttempt);
           await this.setActivityState(this.activityID, this._activityState).catch(error => console.warn(error));
           stmt = this.buildConfiguration(VERB.TERMINATED, this._lessonRegistration, { ...this._statementObject,
             result: { ...((_this$_currentLessonA5 = this._currentLessonAttempt) == null ? void 0 : _this$_currentLessonA5.result),
               duration: duration ? duration : undefined
             },
             objectType: ACTIVITY_TYPES.LESSON,
             activityType: ACTIVITY_TYPES.COURSE,
             duration
           });
           this._lessonRegistration = null;
         }

         return this.sendStatement(stmt);
       };
       /**
        * Sends completion statement to the configured LRS.
        * @param {boolean} completed - A boolean value representing if a user has completed an activity
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.setComplete = completed => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         if (typeof completed === 'undefined') {
           throw Error(ERROR.COMPLETED_NOT_SET_ERROR);
         }

         if (typeof completed !== 'boolean') {
           throw TypeError(ERROR.COMPLETED_NOT_TYPE_BOOLEAN_ERROR);
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject); // update current lesson state

         this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt, {
           result: {
             completion: completed
           }
         }); // save completion boolean to be used in terminate/end attempt result

         this.updateCourseActivityState(this._activityState, ACTIVITY.LESSON, this._currentLessonAttempt); // if passed, send pass statement too

         if (completed) {
           let stmt = this.buildConfiguration(VERB.COMPLETED, this._lessonRegistration, { ...this._statementObject,
             objectType: ACTIVITY_TYPES.LESSON,
             activityType: ACTIVITY_TYPES.COURSE
           });
           return this.sendStatement(stmt);
         }

         return undefined;
       };
       /**
        * Sends success statement to the configured LRS.
        * @param {boolean} completed - A boolean value representing if a user pass or fail a success criteria.
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.setSuccess = success => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         if (typeof success === 'undefined') {
           throw Error(ERROR.SUCCESS_NOT_SET_ERROR);
         }

         if (typeof success !== 'boolean') {
           throw TypeError(ERROR.SUCCESS_NOT_TYPE_BOOLEAN_ERROR);
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject); // update current lesson state

         this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt, {
           result: {
             success: success
           }
         }); // update activity state with success property and value

         this.updateCourseActivityState(this._activityState, ACTIVITY.LESSON, this._currentLessonAttempt);
         let stmt = this.buildConfiguration(success ? VERB.PASSED : VERB.FAILED, this._lessonRegistration, { ...this._statementObject,
           objectType: ACTIVITY_TYPES.LESSON,
           activityType: ACTIVITY_TYPES.COURSE
         }); // send passed or failed statements

         return this.sendStatement(stmt);
       };
       /**
        * Sends success statement to the configured LRS.
        * @param {string} type - The score type. For instance, scaled, raw, min, or max.
        * @param {string} score - The learner's score value.
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.setScore = (type, score) => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         if (typeof type !== 'string' && typeof score === 'undefined') {
           throw TypeError(ERROR.TYPE_NOT_TYPE_STRING_ERROR);
         }

         if (typeof score !== 'number' && typeof score === 'undefined') {
           throw TypeError(`score ${ERROR.SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR}`);
         }

         if (type === SCORE.SCALED && score > 1) {
           throw Error('Scaled value must have a maximum value of 1');
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject); // update current lesson state

         this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt, {
           result: {
             score: {
               [type]: score
             }
           }
         }); // update activity state with success property and value

         this.updateCourseActivityState(this._activityState, ACTIVITY.LESSON, this._currentLessonAttempt); // If type equals scaled or raw then send a statement else just store in local storage
         // to be used in a completion or terminate statement

         if (type === SCORE.SCALED || type === SCORE.RAW) {
           let stmt = this.buildConfiguration(VERB.SCORED, this._lessonRegistration, { ...this._statementObject,
             objectType: ACTIVITY_TYPES.LESSON,
             activityType: ACTIVITY_TYPES.COURSE,
             result: {
               score: {
                 [type]: score
               }
             }
           });
           return this.sendStatement(stmt);
         }

         return true;
       };
       /**
        * Set the bookmark property to the location value within the configured LRS activity state
        * @param {string} location - A string describing a location.
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.setBookmark = async location => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt, {
           bookmark: location
         });
         this._activityState = this.updateCourseActivityState(this._activityState, ACTIVITY.LESSON, this._currentLessonAttempt);
         return this.setActivityState(this.activityID, this._activityState);
       };
       /**
        * Returns the bookmark property value within the activity state
        * @returns {string}
        */


       this.getBookmark = () => {
         var _this$_currentLessonA6;

         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         return (_this$_currentLessonA6 = this._currentLessonAttempt) == null ? void 0 : _this$_currentLessonA6.bookmark;
       };
       /**
        * Creates an interaction definition object to be used within the statement "object" property body
        * @param {string} interactionType - A predefined String value for the type of interaction.
        * @param {string} id - A unique identifier for the interaction according to the Activity ID requirements in the NETC Common Reference Profile
        * @param {string} name - A value that represents the official name or title of the interaction activity.
        * @param {string} description - A value that represents a short description of the interaction activity.
        * @param {Array} correctResponsesPattern - An array of patterns representing the correct response to the interaction.
        * @param {Array} interactionComponentList - An array list of interaction component(s) list or an object with a source and target property which values are arrays of interaction component list for a matching interaction type.
        * @returns {Object <Interaction>}
        */


       this.createInteraction = (interactionType, id, name, description, correctResponsesPattern, interactionComponentList) => {
         if (!interactionType) {
           throw Error(ERROR.INTERACTION_TYPE_NOT_SET_ERROR);
         } // id is required, but name and description are optional


         if (!id) {
           throw Error(ERROR.ID_NOT_SET_ERROR);
         } // lets build the interactionObject


         let interactionObject = {
           id,
           interactionType
         }; // if a name was provided add it to the interactionObject

         if (name) {
           interactionObject.definition = { ...interactionObject.definition,
             name: {
               en: name
             }
           };
         } // if a description was provided add it to the interactionObject


         if (description) {
           interactionObject.definition = { ...interactionObject.definition,
             description: {
               en: description
             }
           };
         } // if definition then there must be a name or description value that created the definition property


         if (interactionObject.definition) {
           if (correctResponsesPattern) {
             if (!Array.isArray(correctResponsesPattern)) {
               throw TypeError(ERROR.CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR);
             }
           }

           if (interactionComponentList) {
             if (interactionType !== INTERACTION.MATCHING && !Array.isArray(interactionComponentList)) {
               throw TypeError(ERROR.CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR);
             } else if (interactionType === INTERACTION.MATCHING && !isObject(interactionComponentList)) {
               throw TypeError('Matching response pattern must be an object');
             }
           } // note : property won't set if it's undefined
           // ternary operator used to catch passed in null values


           interactionObject.definition = { ...interactionObject.definition,
             correctResponsesPattern: correctResponsesPattern,
             choices: interactionType === INTERACTION.CHOICE || interactionType === INTERACTION.CHOICE_WITH_EXPLANATION || interactionType === INTERACTION.SEQUENCING ? interactionComponentList : undefined,
             scale: interactionType === INTERACTION.LIKERT ? interactionComponentList : undefined,
             source: interactionType === INTERACTION.MATCHING ? interactionComponentList['source'] : undefined,
             target: interactionType === INTERACTION.MATCHING ? interactionComponentList['target'] : undefined,
             steps: interactionType === INTERACTION.PERFORMANCE ? interactionComponentList : undefined
           };
         }

         return interactionObject;
       };
       /**
        * Sends interaction response to the configured LRS.
        * @param {Array <strings} response - An array that holds the response(s) of an interaction.
        * @param {Object <Interaction>} interaction - The interaction object defining the interaction activity
        * @param {Object <AssessmentParent>} parent - The parent configuration object.
        * @param {string} otherResponse - A provided extra response if an interaction has multiple responses
        * @param {Array <IAttachment>} attachments - An array of attachment objects
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.sendInteraction = (response, interaction, parent, otherResponse = '', attachments) => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject);

         if (!response) {
           throw Error(ERROR.RESPONSE_VALUE_UNDEFINED_ERROR);
         }

         if (!interaction) {
           throw Error(ERROR.INTERACTION_UNDEFINED_ERROR);
         }

         if (interaction) {
           if (typeof interaction !== 'object') {
             throw Error(ERROR.INTERACTION_NOT_TYPE_OBJECT_ERROR);
           }

           if (!interaction.id) {
             throw Error(ERROR.INTERACTION_ID_UNDEFINED_ERROR);
           }

           if (!interaction.interactionType) {
             throw Error(ERROR.INTERACTION_TYPE_UNDEFINED_ERROR);
           }
         }

         if (!parent) {
           throw Error(ERROR.PARENT_UNDEFINED_ERROR);
         }

         if (parent) {
           if (typeof parent !== 'object') {
             throw Error(ERROR.PARENT_NOT_TYPE_OBJECT_ERROR);
           }
         }

         const {
           interactionType
         } = interaction;

         if (interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT && !attachments) {
           throw Error(ERROR.ATTACHMENT_NOT_PROVIDED_ERROR);
         }

         if (interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT && attachments.length < 1) {
           throw Error(ERROR.ATTACHMENT_NOT_PROVIDED_ERROR);
         }

         if (!parent.hasOwnProperty('type')) {
           // defaults to
           if (parent.definition) {
             parent.definition.type = 'http://adlnet.gov/expapi/activities/assessment';
           }
         }

         let extensions;

         if (interactionType === INTERACTION.CHOICE_WITH_EXPLANATION) {
           interaction.interactionType = INTERACTION.CHOICE;
           extensions = {
             [EXTENSIONS.RESPONSE_EXPLANATION]: otherResponse
           };
         }

         if (interactionType === INTERACTION.OTHER_UPLOAD || interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT) {
           interaction.interactionType = INTERACTION.OTHER;
           extensions = {
             [EXTENSIONS.RESPONSE_TYPE]: 'upload'
           };
         }

         if (interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT) {
           if (!Array.isArray(attachments)) {
             throw Error(ERROR.ATTACHMENT_PARAM_NOT_AN_ARRAY);
           }
         }

         let stmt = this.buildConfiguration(VERB.RESPONDED, this._lessonRegistration, { ...this._statementObject,
           objectType: ACTIVITY_TYPES.LESSON,
           activityType: ACTIVITY_TYPES.COURSE,
           interaction: interaction,
           parent,
           result: {
             response: response[0].toString(),
             extensions
           }
         });
         return this.sendStatement(stmt, attachments);
       };
       /**
        * Send a "viewed" statement to the configured LRS.
        * @param {string} id
        * @param {string} name
        * @param {string} description
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.viewed = (id, name, description) => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         if (!id) {
           throw Error(ERROR.ID_NOT_SET_ERROR);
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject);
         let stmt = this.buildConfiguration(VERB.VIEWED, this._lessonRegistration, { ...this._statementObject,
           object: {
             id,
             definition: {
               name: {
                 en: name
               },
               description: {
                 en: description
               }
             }
           },
           objectType: ACTIVITY_TYPES.PAGE,
           activityType: ACTIVITY_TYPES.COURSE,
           isScorm: false
         });
         return this.sendStatement(stmt);
       };
       /**
        * Send a "section" statement to the configured LRS.
        * @param {string} id
        * @param {string} name
        * @param {string} description
        * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
        */


       this.section = (id, name, description) => {
         // validate if a registration uuid has been assign yet
         if (!this._lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         if (!id) {
           throw Error(ERROR.ID_NOT_SET_ERROR);
         } // validate _statementObject


         this.validateLessonActivity(this._statementObject);
         let stmt = this.buildConfiguration(VERB.COMPLETED, this._lessonRegistration, { ...this._statementObject,
           object: {
             id,
             definition: {
               name: {
                 en: name
               },
               description: {
                 en: description
               }
             }
           },
           objectType: ACTIVITY_TYPES.SECTION,
           activityType: ACTIVITY_TYPES.COURSE,
           isScorm: false
         });
         return this.sendStatement(stmt);
       };

       this.createParent = (id, name, description) => {
         let stmt = {};

         if (!id) {
           throw Error(ERROR.PARENT_ID_UNDEFINED_ERROR);
         }

         stmt = {
           id
         }; // if a name was provided add it to the activity object

         if (name) {
           stmt.definition = { ...stmt.definition,
             name: {
               en: name
             }
           };
         } // if a description was provided add it to the activity object


         if (description) {
           stmt.definition = { ...stmt.definition,
             description: {
               en: description
             }
           };
         }

         return stmt;
       };

       this.createInteractionComponentList = (id, description) => {
         if (typeof id !== 'string' && typeof description !== 'string') throw Error(ERROR.TYPE_NOT_TYPE_STRING_ERROR);
         return {
           id,
           description: {
             en: description
           }
         };
       };

       this.createResponsePattern = (interactionType, response) => {
         if (!Array.isArray(response)) throw Error(ERROR.RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR);
         return this.formatResponsePattern(interactionType, response);
       };

       this.formatResponsePattern = (interactionType, response) => {
         let crp;

         if (response.length === 1) {
           if (interactionType === INTERACTION.NUMERIC) {
             const split = response[0].split(':');
             return split.length === 1 ? response : [`${split[0]}[:]${split[1]}`];
           } else {
             return response;
           }
         }

         crp = response.reduce((accumulator, current, index) => {
           if (interactionType === INTERACTION.MATCHING || interactionType === INTERACTION.PERFORMANCE) {
             const split = current.split(':');
             if (split.length === 1) throw Error(ERROR.INVALID_MATCHING_PERFORMANCE_RESPONSE);
             accumulator += split[0] + '[.]' + split[1];
           } else {
             accumulator += current;
           }

           if (index < response.length - 1) {
             accumulator += '[,]';
           }

           return accumulator;
         }, '');
         return [crp];
       };

       this.attachment = (value, usageType, contentType, display, description) => {
         if (!value) {
           throw new Error('You must provide a value.');
         }

         if (!usageType && typeof usageType !== 'string') {
           throw new TypeError(`usageType ${ERROR.TYPE_NOT_TYPE_STRING_ERROR}`);
         }

         if (!contentType) {
           throw new Error(`contentType ${ERROR.TYPE_NOT_TYPE_STRING_ERROR}`);
         }

         if (!display) {
           throw new Error(`display ${ERROR.TYPE_NOT_TYPE_STRING_ERROR}`);
         } // build attachment array


         return {
           type: {
             usageType: this.getUsageType(usageType),
             display: {
               en: display
             },
             description: description ? {
               en: description
             } : undefined,
             contentType
           },
           value
         };
       };

       this.getUsageType = type => {
         let types = {};
         types['image'] = 'http://adlnet.gov/expapi/attachments/image';
         types['signature'] = 'http://adlnet.gov/expapi/attachments/signature';
         types['certification'] = 'http://id.tincanapi.com/attachment/certificate-of-completion';
         return types[type];
       };

       this.getLessonRegistration = () => {
         if (!this.lessonRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         return this.lessonRegistration;
       };

       this.getCourseRegistration = () => {
         if (!this.courseRegistration) {
           throw Error(ERROR.NO_LESSON_REGISTRATION_ID_ERROR);
         }

         return this.courseRegistration;
       }; // private methods


       this.launch = () => {
         //grab url parameters if any
         let launchParams = parseLaunchParams(new URL(window.location.href), this._config); // check to see if required url parameters were passed in

         if (launchParams.hasOwnProperty('endpoint') && launchParams.hasOwnProperty('auth') && launchParams.hasOwnProperty('actor')) {
           // set launch parameters values internally.
           if (launchParams.hasOwnProperty('registration')) {
             this._lessonRegistration = launchParams['registration'];
           } // update xAPIWrapper configuration settings


           this._config = { ...launchParams,
             strictCallbacks: true
           };

           this._ADL.XAPIWrapper.changeConfig(this._config);
         } else {
           this._ADL.launch((err, _launchdata, xAPIWrapper) => {
             if (!err) {
               // this was adl launched, use newly configured wrapper
               this._ADL.XAPIWrapper = xAPIWrapper;
             } else {
               if (this._config === null) {
                 throw new Error(ERROR.LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR);
               }

               this._ADL.XAPIWrapper.changeConfig(this._config);
             }
           }, true);
         }
       };

       this.generateInitialCourseAttemptState = (options = {}) => {
         return {
           registration: '',
           initialized: false,
           terminated: false,
           lessons: {},
           ...options
         };
       };

       this.generateInitialLessonAttemptState = (options = {}) => {
         return {
           registration: '',
           initialized: false,
           terminated: false,
           suspended: false,
           resumed: false,
           bookmark: '',
           result: {},
           ...options
         };
       };

       this.addLessonAttempt = (state, key, lessonAttempt) => {
         let clone = { ...state
         };

         if (clone.lesson && clone.lessons[key]) {
           clone == null ? void 0 : clone.lessons[key].push(lessonAttempt);
         } else {
           clone.lessons = { ...clone.lessons,
             [key]: [lessonAttempt]
           };
         }

         return clone;
       };

       this.updateCourseActivityState = (state, activityType, update = {}) => {
         let clone = { ...state
         };
         const {
           activity: {
             id
           }
         } = this._statementObject;
         if (Object.keys(state.lessons).length === 0) return {}; // push the update lesson attempt state back into the course state object

         if (activityType === ACTIVITY.LESSON) {
           let foundLessonIndex = -1;

           if (state.lessons) {
             foundLessonIndex = state.lessons[id].findIndex(lesson => lesson.registration === this.lessonRegistration);
           }

           if (foundLessonIndex === -1) {
             throw Error(ERROR.LESSON_ATTEMPT_NOT_FOUND);
           }

           clone.lessons[id][foundLessonIndex] = { ...clone.lessons[id][foundLessonIndex],
             ...update
           };
         } else {
           // course state update
           clone = { ...clone,
             ...state
           };
         }

         return clone;
       };

       this.updateLessonActivityState = (state, update = {}) => {
         let clone = { ...state
         };

         if ('result' in update) {
           var _clone, _clone2, _clone2$result, _update$result;

           clone = { ...clone,
             result: { ...((_clone = clone) == null ? void 0 : _clone.result),
               ...(update == null ? void 0 : update.result),
               score: { ...((_clone2 = clone) == null ? void 0 : (_clone2$result = _clone2.result) == null ? void 0 : _clone2$result.score),
                 ...(update == null ? void 0 : (_update$result = update.result) == null ? void 0 : _update$result.score)
               }
             }
           };
         } else {
           clone = { ...clone,
             ...update
           };
         }

         return clone;
       };
       /**
        *
        * @param activityID
        * @param registration
        * @returns
        */


       this.getActivityState = activityID => {
         const {
           actor
         } = this._config;
         return new Promise((resolve, reject) => {
           this._ADL.XAPIWrapper.getState(activityID, actor, 'https://w3id.org/xapi/scorm/activity-state', null, null, (error, {
             response,
             status
           }, _obj) => {
             if (status >= 200 && status < 300) {
               resolve(JSON.parse(response));
             } else {
               reject(error);
             }
           });
         });
       };
       /**
        *
        * @param id
        * @param registration
        * @param attempt
        * @returns
        */


       this.setActivityState = (id, state) => {
         return new Promise((resolve, reject) => {
           if (this._config) {
             this._ADL.XAPIWrapper.sendState(id, this._config.actor, 'https://w3id.org/xapi/scorm/activity-state', null, state, null, //  matchHash
             null, // noneMatchHash
             (error, {
               response,
               status
             }, _obj) => {
               if (status >= 200 && status < 300) {
                 resolve({});
               } else {
                 reject(error);
               }
             });
           } else {
             reject('No configured actor set.');
           }
         });
       };
       /**
        * Validate options of elearning xapi
        */


       this.validateCourseActivity = stmt => {
         if (!stmt.group) {
           throw new Error(ERROR.GROUP_OBJECT_REQUIRED_ERROR);
         }

         return true;
       };

       this.validateLessonActivity = options => {
         // statementObj still empty?
         if (Object.entries(options).length === 0) {
           throw new Error(ERROR.ACTIVITY_AND_GROUP_OBJECT_REQUIRED_ERROR);
         }

         if (!options.activity) {
           throw new Error(ERROR.ACTIVITY_OBJECT_REQUIRED_ERROR);
         }

         return true;
       };

       if (config) {
         this._config = { ...config,
           strictCallbacks: true
         };
         this._platform = config.platform ? config.platform : '';
       }

       this._ADL = _ADL();
       this._lessonRegistration = null;
       this._useScorm5Profile = false;
       this._activityState = null;
       this._currentLessonAttempt = null;
       this._courseRegistration = null;
       this.launch();
     }
     /**
      * Sets activity definition to the statement object
      * @param {string} id
      * @param {string} name
      * @param {string} description
      */


     setActivity(id, name, description) {
       // id is required, but name and description are optional
       if (!id) {
         throw Error(ERROR.ACTIVITY_ID_UNDEFINED_ERROR);
       }

       if (typeof id !== 'string') {
         throw TypeError(ERROR.ACTIVITY_ID_NOT_TYPE_STRING_ERROR);
       }

       let stmt = { ...this._statementObject
       };
       stmt.activity = {
         id: this._activityID ? this._activityID : id
       }; // if a name was provided add it to the activity object

       if (name) {
         if (typeof name !== 'string') {
           throw TypeError(ERROR.ACTIVITY_NAME_NOT_TYPE_STRING_ERROR);
         }

         stmt.activity.definition = { ...stmt.activity.definition,
           name: {
             en: name
           }
         };
       } // if a description was provided add it to the activity object


       if (description) {
         if (typeof description !== 'string') {
           throw TypeError(ERROR.ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR);
         }

         stmt.activity.definition = { ...stmt.activity.definition,
           description: {
             en: description
           }
         };
       }

       this._statementObject = stmt;
     }
     /**
      * Sets group definition to the statement object
      * @param {string} id - A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile
      * @param {string} name - A value that represents  the official name or title of the Activity
      * @param {string} description - A value that represents a short description of the Activity
      */


     setGroup(id, name, description) {
       // id is required, but name and description are optional
       if (!id) {
         throw Error(ERROR.GROUP_ID_UNDEFINED_ERROR);
       }

       if (typeof id !== 'string') {
         throw TypeError(ERROR.GROUP_ID_NOT_TYPE_STRING_ERROR);
       }

       let stmt = { ...this._statementObject
       };
       stmt.group = {
         id
       }; // if a name was provided add it to the activity object

       if (name) {
         if (typeof name !== 'string') {
           throw TypeError(ERROR.GROUP_NAME_NOT_TYPE_STRING_ERROR);
         }

         stmt.group.definition = { ...stmt.group.definition,
           name: {
             en: name
           }
         };
       } // if a description was provided add it to the activity object


       if (description) {
         if (typeof description !== 'string') {
           throw TypeError(ERROR.GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR);
         }

         stmt.group.definition = { ...stmt.group.definition,
           description: {
             en: description
           }
         };
       }

       this._statementObject = stmt;
     }
     /**
      * Adds a single extension or an object of extensions to the statement object
      * @param {string} key - string identifer of the extension
      * @param {string} value - value of the extension
      */


     addExtensions(key, value) {
       // id is required
       if (!key) {
         throw Error(ERROR.EXTENSION_KEY_NOT_PROVIDED_ERROR);
       }

       if (typeof key !== 'string' && typeof key !== 'object') {
         throw TypeError(ERROR.EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR);
       }

       if (typeof key === 'string') {
         if (!value) {
           throw Error(ERROR.EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR);
         }

         this._extensions[key] = value;
       }

       if (typeof key === 'object') {
         // spread the old and new extensions
         this._extensions = { ...this._extensions,
           ...key
         };
       } // spread them wings


       let stmt = { ...this._statementObject,
         extensions: this._extensions
       };
       this._statementObject = stmt;
     }
     /**
      * Sends statement
      * @param configuration
      * @param [attachments]
      * @returns statement
      */


     sendStatement(configuration, attachments) {
       // set generated internal statement for printing out
       this._statement = generateStatement(configuration);
       return new Promise((resolve, reject) => {
         const cb = (error, {
           response,
           status
         }, obj) => {
           if (status >= 200 && status < 300) {
             resolve({});
           } else {
             reject(error);
           }
         };

         this._ADL.XAPIWrapper.sendStatement(this._statement, cb, attachments);
       });
     }
     /**
      * Builds configuration
      * @param verb
      * @param registration
      * @param [options]
      * @returns configuration
      */


     buildConfiguration(verb, registration, options = null) {
       this._useScorm5Profile = options.group ? true : false;
       let stmt = {
         verb,
         actor: this._ADL.XAPIWrapper.lrs.actor,
         registration: registration,
         isScorm: options.isScorm ? options.isScorm : this._useScorm5Profile,
         platform: this._platform ? this._platform : undefined,
         ...options
       };
       return stmt;
     }

     get activityID() {
       return this._statementObject.group.id;
     }

     set activityID(id) {}

     set lessonRegistration(registration) {
       this._lessonRegistration = registration;
     }

     get lessonRegistration() {
       return this._lessonRegistration;
     }

     set courseRegistration(registration) {
       this._courseRegistration = registration;
     }

     get courseRegistration() {
       return this._courseRegistration;
     } // ACCESSORS


     get ACTIVITY() {
       return ACTIVITY;
     }

     get VERB() {
       return VERB;
     }

     get INTERACTION() {
       return INTERACTION;
     }

     get SCORE() {
       return SCORE;
     }

     get EXTENSIONS() {
       return EXTENSIONS;
     } // statement to string


     get statement() {
       return JSON.stringify(this._statement, undefined, 2);
     }

   }

   const parseLaunchParams$1 = (parsedURL, baseconf) => {
     const sp = parsedURL.search;
     const params = new URLSearchParams(sp);

     if (!baseconf) {
       baseconf = {};
     }

     if (params.get("endpoint")) {
       baseconf["endpoint"] = params.get("endpoint");
     }

     if (params.get("auth")) {
       baseconf["auth"] = params.get("auth");
     }

     if (params.get("actor")) {
       baseconf["actor"] = params.get("actor");
     }

     if (params.get("registration")) {
       baseconf["registration"] = params.get("registration");
     }

     return baseconf;
   };
   const ruuid$1 = () => {
     return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
       var r = Math.random() * 16 | 0,
           v = c == "x" ? r : r & 0x3 | 0x8;
       return v.toString(16);
     });
   };

   // VERB TYPES
   const ACCESSED$1 = "accessed";
   const COMPLETED$1 = "completed";
   const DESELECTED = "deselected";
   const DISLIKED = "disliked";
   const INITIALIZED$1 = "initialized";
   const LIKED = "liked";
   const OPENED = "opened";
   const SEARCHED = "searched";
   const SELECTED = "selected";
   const TERMINATED$1 = "terminated";
   const UPLOADED = "uploaded";
   const VIEWED$1 = "viewed";
   const CLOSED = "closed";
   const PRINTED = "printed"; // ACTIVITY TYPE

   const APPLICATION = "application";
   const CHECKLIST = "checklist";
   const CHECKLIST_ITEM = "checklist-item";
   const IMAGE = "image";
   const MENU = "menu";
   const MENU_ITEM = "menu-item";
   const SEARCH_ENGINE = "search-engine";
   const STEP = "step";
   const PROCEDURE = "procedure";
   const FILE = "file";
   const LINK = "link";
   const PAGE$1 = "page"; // EXTENSIONS

   const SCHOOL_CENTER$1 = "https://w3id.org/xapi/netc/extensions/school-center";
   const W3ID_EXTENSION = "https://w3id.org/xapi/performance-support/extensions"; // VERB BASE IRI

   const VERB_TINCAN_IRI = "http://id.tincanapi.com/verb";
   const VERB_ACROSSX_IRI = "https://w3id.org/xapi/acrossx/verbs";
   const VERB_EXAPI_IRI = "http://adlnet.gov/expapi/verbs";
   const VERB_W3ID = "https://w3id.org/xapi/performance-support/verbs"; // ACTIVITY BASE IRI

   const ACTIVITY_W3ID_IRI = "https://w3id.org/xapi/performance-support/activity-types";
   const ACTIVITY_TINCAN_IRI = "http://id.tincanapi.com/activitytype";
   const ACTIVITY_ADL_IRI = "http://adlnet.gov/expapi/activities";
   const ACTIVITY_ACROSSX_IRI = "https://w3id.org/xapi/acrossx/activities";
   const ACTIVITY_W3ID_NETC = "https://w3id.org/xapi/netc/verbs/";
   const PERFORMANCE_SUPPORT_IRI = "https://w3id.org/xapi/perfomance-support/v1.0";
   const CATEGORY_TYPE_IRI = "http://adlnet.gov/expapi/activities/profile";
   const CATEGORY_NETC_IRI = "https://w3id.org/xapi/netc/v1.0"; // grouping context = application / simulation / course

   const getParentType = type => {
     switch (type) {
       case LINK:
       case SEARCH_ENGINE:
         return `${ACTIVITY_ACROSSX_IRI}/${SEARCH_ENGINE}`;

       case FILE:
       case IMAGE:
         return `${ACTIVITY_ACROSSX_IRI}/${PAGE$1}`;

       case CHECKLIST_ITEM:
       case CHECKLIST:
         return `${ACTIVITY_TINCAN_IRI}/${CHECKLIST}`;

       case STEP:
         return `${ACTIVITY_W3ID_IRI}/${type}`;

       case MENU_ITEM:
         return `${ACTIVITY_W3ID_IRI}/${MENU}`;

       default:
         return undefined;
     }
   };
   const getVerbBaseURL = verb => {
     switch (verb) {
       case INITIALIZED$1:
       case TERMINATED$1:
       case COMPLETED$1:
         return VERB_EXAPI_IRI;

       case OPENED:
       case SEARCHED:
       case DESELECTED:
       case ACCESSED$1:
         return VERB_W3ID;

       case SELECTED:
       case VIEWED$1:
         return VERB_TINCAN_IRI;

       case UPLOADED:
         return ACTIVITY_W3ID_NETC;

       case LIKED:
       case DISLIKED:
         return VERB_ACROSSX_IRI;

       default:
         throw new Error(`No such verb type ${verb}`);
     }
   };
   const getActivityTypeID = type => {
     switch (type) {
       case INITIALIZED$1:
       case TERMINATED$1:
         return `${ACTIVITY_W3ID_IRI}/${APPLICATION}`;

       case LINK:
         return `${ACTIVITY_ADL_IRI}/${type}`;

       case CHECKLIST:
         return `${ACTIVITY_TINCAN_IRI}/${type}`;

       case CHECKLIST_ITEM:
         return `${ACTIVITY_TINCAN_IRI}/${CHECKLIST_ITEM}`;

       case FILE:
         return `${ACTIVITY_ADL_IRI}/${FILE}`;

       case MENU:
         return `${PERFORMANCE_SUPPORT_IRI}/${MENU}`;

       case APPLICATION:
       case IMAGE:
       case MENU_ITEM:
       case PROCEDURE:
         return `${ACTIVITY_W3ID_IRI}/${type}`;

       case PAGE$1:
       case SEARCH_ENGINE:
         return `${ACTIVITY_ACROSSX_IRI}/${type}`;

       case STEP:
         return `${ACTIVITY_TINCAN_IRI}/${type}`;

       default:
         return `${ACTIVITY_W3ID_IRI}/${APPLICATION}`;
     }
   };
   const getContextTypeID = verb => {
     return `${ACTIVITY_W3ID_IRI}/${APPLICATION}`;
   };
   const VERB$1 = {
     COMPLETED: COMPLETED$1,
     SEARCHED,
     SELECTED,
     VIEWED: VIEWED$1,
     UPLOADED,
     LIKED,
     DISLIKED,
     INITIALIZED: INITIALIZED$1,
     TERMINATED: TERMINATED$1,
     DESELECTED,
     ACCESSED: ACCESSED$1,
     OPENED,
     CLOSED,
     PRINTED
   };
   const ACTIVITY$1 = {
     INITIALIZED: INITIALIZED$1,
     TERMINATED: TERMINATED$1,
     CHECKLIST,
     CHECKLIST_ITEM,
     FILE,
     IMAGE,
     MENU,
     APPLICATION,
     SEARCH_ENGINE,
     LINK,
     STEP,
     PROCEDURE,
     MENU_ITEM,
     PAGE: PAGE$1
   };
   const EXTENSIONS$1 = {
     SCHOOL_CENTER: SCHOOL_CENTER$1
   };
   const BASE_IRI = {
     VERB_TINCAN_IRI,
     VERB_ACROSSX_IRI,
     VERB_EXAPI_IRI,
     ACTIVITY_W3ID_IRI,
     ACTIVITY_TINCAN_IRI,
     ACTIVITY_ADL_IRI,
     W3ID_EXTENSION,
     PERFORMANCE_SUPPORT_IRI,
     CATEGORY_TYPE_IRI,
     CATEGORY_NETC_IRI
   };

   /** ***********************************************************************
   *
   * Veracity Technology Consultants CONFIDENTIAL
   * __________________
   *
   *  2019 Veracity Technology Consultants
   *  All Rights Reserved.
   *
   * NOTICE:  All information contained herein is, and remains
   * the property of Veracity Technology Consultants and its suppliers,
   * if any.  The intellectual and technical concepts contained
   * herein are proprietary to Veracity Technology Consultants
   * and its suppliers and may be covered by U.S. and Foreign Patents,
   * patents in process, and are protected by trade secret or copyright law.
   * Dissemination of this information or reproduction of this material
   * is strictly forbidden unless prior written permission is obtained
   * from Veracity Technology Consultants.
   */

   const addActor$1 = (stmt, {
     actor
   }) => {
     return { ...stmt,
       actor
     };
   };

   const addVerb$1 = (stmt, {
     verb
   }) => {
     return { ...stmt,
       verb: {
         id: `${getVerbBaseURL(verb)}/${verb}`,
         display: {
           "en": verb
         }
       }
     };
   };

   const addObject$1 = (stmt, {
     activityType,
     activity
   }) => {
     return { ...stmt,
       object: { ...activity,
         definition: { ...activity.definition,
           type: getActivityTypeID(activityType)
         }
       }
     };
   };

   const addContext$1 = (stmt, {
     registration
   }) => {
     let context = {
       contextActivities: {},
       registration
     };
     return { ...stmt,
       context: context
     };
   };

   const addParent$1 = (stmt, {
     activityType,
     parent
   }) => {
     let p = [{ ...parent,
       definition: { ...parent.definition,
         type: getParentType(activityType)
       }
     }];
     return { ...stmt,
       context: { ...stmt.context,
         contextActivities: { ...stmt.context.contextActivities,
           parent: p
         }
       }
     };
   };

   const addCategory$1 = stmt => {
     let category = [{
       id: BASE_IRI.PERFORMANCE_SUPPORT_IRI,
       definition: {
         type: BASE_IRI.CATEGORY_TYPE_IRI
       }
     }, {
       id: BASE_IRI.CATEGORY_NETC_IRI,
       definition: {
         type: BASE_IRI.CATEGORY_TYPE_IRI
       }
     }];
     return { ...stmt,
       context: { ...stmt.context,
         contextActivities: {
           category
         }
       }
     };
   };

   const addGrouping$1 = (stmt, {
     activityType,
     group
   }) => {
     let grouping = [{ ...group,
       definition: { ...group.definition,
         type: getContextTypeID()
       }
     }];
     return { ...stmt,
       context: { ...stmt.context,
         contextActivities: { ...stmt.context.contextActivities,
           grouping
         }
       }
     };
   };

   const addTimestamp$1 = stmt => {
     return { ...stmt,
       timestamp: new Date().toISOString()
     };
   };

   const addResult$1 = (stmt, {
     result
   }) => {
     return { ...stmt,
       result
     };
   };

   const addExtensions$1 = (stmt, {
     extensions
   }) => {
     return { ...stmt,
       context: { ...stmt.context,
         extensions
       }
     };
   };

   const addID$1 = stmt => {
     return { ...stmt,
       id: ruuid$1()
     };
   };

   const addPlatform$1 = (stmt, {
     platform
   }) => {
     return { ...stmt,
       context: { ...stmt.context,
         platform
       }
     };
   }; // print the current statement state

   const generateStatement$1 = config => {
     let statement = buildPipeline$1(config).reduce((statement, func) => func(statement, config), {});
     return statement;
   };

   const buildPipeline$1 = config => {
     const {
       verb,
       parent,
       result,
       platform
     } = config;
     let pipeline = [addActor$1, addVerb$1, addObject$1, addContext$1, addCategory$1, addExtensions$1, addID$1, addTimestamp$1];

     switch (verb) {
       case VERB$1.INITIALIZED:
       case VERB$1.TERMINATED:
         break;

       case VERB$1.SELECTED:
       case VERB$1.DESELECTED:
       case VERB$1.VIEWED:
       case VERB$1.COMPLETED:
       case VERB$1.LIKED:
       case VERB$1.DISLIKED:
       case VERB$1.UPLOADED:
       case VERB$1.SEARCHED:
       case VERB$1.ACCESSED:
       case VERB$1.OPENED:
         pipeline.push(addGrouping$1);
         break;

       default:
         throw new Error(`No such statement type ${verb}`);
     }

     if (parent) {
       pipeline.push(addParent$1);
     }

     if (result) {
       pipeline.push(addResult$1);
     }

     if (platform) {
       pipeline.push(addPlatform$1);
     }

     return pipeline;
   };

   // custom
   class PerformanceSupportXAPI {
     constructor(config) {
       this._config = null; // statement object

       this._statementObject = {};
       this._extensions = {};
       this._activityID = null;
       this._grouping = null;
       this._platform = undefined;

       this.launch = () => {

         let launchParams = parseLaunchParams$1(new URL(window.location.href), this._config); // check to see if required url parameters were passed in

         if (launchParams.hasOwnProperty("endpoint") && launchParams.hasOwnProperty("auth") && launchParams.hasOwnProperty("actor")) {
           // set launch parameters values internally.
           if (launchParams.hasOwnProperty("registration")) {
             this._registration = launchParams["registration"];
           } // update xAPIWrapper configuration settings


           this._config = { ...launchParams,
             strictCallbacks: true
           };

           this._ADL.XAPIWrapper.changeConfig(this._config);
         } else {
           this._ADL.launch((err, launchdata, xAPIWrapper) => {
             if (!err) {
               // this was adl launched, use newly configured wrapper
               this._ADL.XAPIWrapper = xAPIWrapper;
             } else {
               if (this._config == null) {
                 throw new Error("No launch parameters or configuration object provided.");
               }

               this._ADL.XAPIWrapper.changeConfig(this._config);
             }
           }, true);
         }
       };

       this.validateOptions = options => {
         // statementObj still empty?
         if (Object.entries(options).length === 0) {
           throw new Error("You must create a statement by using setActivity() and addGroup()");
         }

         if (!options.group) {
           throw new Error("A group is required to be able to send statements. Use addGroup() to add a group");
         }

         return {};
       };

       this.createParent = (id, name, description) => {
         let stmt = {};

         if (!id) {
           throw Error('You must provided an "id" value.');
         }

         stmt = {
           id
         }; // if a name was provided add it to the activity object

         if (name) {
           stmt.definition = { ...stmt.definition,
             name: {
               en: name
             }
           };
         } // if a description was provided add it to the activity object


         if (description) {
           stmt.definition = { ...stmt.definition,
             description: {
               en: description
             }
           };
         }

         return stmt;
       };

       this.opened = (type, id, name, description, parent) => {
         // validate if a registration uuid has been assign yet
         let isReg = this.hasRegistration(this._registration);
         if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

         let validOptions = this.validateOptions(this._statementObject);

         if (validOptions.error) {
           throw new Error(validOptions.error);
         }

         if (!parent) {
           throw new Error("A parent object is required.");
         }

         let openedValue = type.toLocaleLowerCase();

         if (openedValue !== ACTIVITY$1.FILE && openedValue !== ACTIVITY$1.LINK) {
           throw new Error('You must pass in either "file" or "link" as a value.');
         } // set activity


         this.setActivity(id, name, description);
         let stmt = this.buildConfiguration(VERB$1.OPENED, openedValue, { ...this._statementObject,
           parent
         });
         return this.sendStatement(stmt);
       };

       this.closed = (type, id, name, description, parent) => {
         // validate if a registration uuid has been assign yet
         let isReg = this.hasRegistration(this._registration);
         if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

         let validOptions = this.validateOptions(this._statementObject);

         if (validOptions.error) {
           throw new Error(validOptions.error);
         } // force upper case since constant declarations are always uppercase
         // if a dash is part of the constant value then replace it with a underscore to match
         // the constant declaration name


         const closedValue = type.toUpperCase().replace("-", "_");

         if (!ACTIVITY$1[closedValue]) {
           throw new Error(`${type} isn't a valid activity type.`);
         } // set activity


         this.setActivity(id, name, description);
         let stmt = this.buildConfiguration(VERB$1.CLOSED, closedValue, { ...this._statementObject,
           parent
         });
         return this.sendStatement(stmt);
       };

       this.printed = (type, id, name, description, parent) => {
         // validate if a registration uuid has been assign yet
         let isReg = this.hasRegistration(this._registration);
         if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

         let validOptions = this.validateOptions(this._statementObject);

         if (validOptions.error) {
           throw new Error(validOptions.error);
         } // force upper case since constant declarations are always uppercase
         // if a dash is part of the constant value then replace it with a underscore to match
         // the constant declaration name


         const printedValue = type.toUpperCase().replace("-", "_");

         if (printedValue !== ACTIVITY$1.FILE && printedValue !== ACTIVITY$1.IMAGE && printedValue !== ACTIVITY$1.PAGE) {
           throw new Error('You must pass in either "file", "image" or "page" as a value.');
         } // set activity


         this.setActivity(id, name, description);
         let stmt = this.buildConfiguration(VERB$1.PRINTED, printedValue, { ...this._statementObject,
           parent
         });
         return this.sendStatement(stmt);
       };

       this.attachment = (value, usageType, contentType, display, description) => {
         if (!value) {
           throw new Error("You must provide a value.");
         }

         if (!usageType) {
           throw new Error("You must provide a usageType.");
         }

         if (!contentType) {
           throw new Error("You must provide a contentType.");
         }

         if (!display) {
           throw new Error("You must provide a contentType.");
         } // build attachment array


         return [{
           type: {
             usageType: this.getUsageType(usageType),
             display: {
               en: display
             },
             description: description ? {
               en: description
             } : undefined,
             contentType
           },
           value
         }];
       };

       this.hasRegistration = registration => {
         return registration ? {} : {
           error: `No registration id is associated with this attempt. You must call initialize to register a new attempt.`
         };
       };

       this.getUsageType = type => {
         let types = {};
         types["image"] = "http://adlnet.gov/expapi/attachments/image";
         types["signature"] = "http://adlnet.gov/expapi/attachments/signature";
         types["certification"] = "http://id.tincanapi.com/attachment/certificate-of-completion";
         return types[type];
       };

       this.getAttemptRegistration = () => {
         return this._registration;
       };

       if (config) {
         this._config = { ...config,
           strictCallbacks: true
         };
         this._platform = config.platform ? config.platform : null;
       }

       this._registration = null;
       this._ADL = _ADL();
       this.launch();
     }

     sendStatement(stmt, attachments = []) {
       // set generated internal statement for printing out
       this._statement = generateStatement$1(stmt);
       return new Promise((resolve, reject) => {
         const cb = (error, response, obj) => {
           if (response.status >= 200 && response.status < 300) {
             resolve({});
           } else {
             reject(error);
           }
         };

         this._ADL.XAPIWrapper.sendStatement(this._statement, cb, attachments);
       });
     }

     buildConfiguration(verb, activityType, options = null) {
       let stmt = {
         verb,
         activityType,
         actor: this._ADL.XAPIWrapper.lrs.actor,
         registration: this._registration,
         platform: this._platform ? this._platform : undefined,
         ...options
       };
       return stmt;
     }

     setActivity(id, name, description) {
       // id is required, but name and description are optional
       if (!id) {
         throw new Error('You must provided an "id" value.');
       }

       let stmt = { ...this._statementObject
       }; // check to see if activityID was passed into the library as a launch parameter
       // else used the passed in argument

       stmt.activity = {
         id: this._activityID ? this._activityID : id
       }; // if a name was provided add it to the activity object

       if (name) {
         stmt.activity.definition = { ...stmt.activity.definition,
           name: {
             en: name
           }
         };
       } // if a description was provided add it to the activity object


       if (description) {
         stmt.activity.definition = { ...stmt.activity.definition,
           description: {
             en: description
           }
         };
       }

       this._statementObject = stmt;
     }

     setGroup(id, name, description) {
       // id is required, but name and description are optional
       if (!id) {
         throw new Error('You must provided an "id" value.');
       }

       let stmt = { ...this._statementObject
       };
       stmt.group = {
         id
       }; // if a name was provided add it to the activity object

       if (name) {
         stmt.group.definition = { ...stmt.group.definition,
           name: {
             en: name
           }
         };
       } // if a description was provided add it to the activity object


       if (description) {
         stmt.group.definition = { ...stmt.group.definition,
           description: {
             en: description
           }
         };
       }

       this._statementObject = stmt; // console.log("addGroup", this._statementObject);
     }

     addExtensions(key, value) {
       // id is required
       if (!key) {
         throw new Error('You must provided an "key" value.');
       } // simple type check


       if (typeof key === "string") {
         if (!value) {
           throw new Error("Expecting 2 arguments, but only one was provided.");
         }

         this._extensions[key] = value;
       } // simple type check


       if (typeof key === "object") {
         // spread the old and new extensions
         this._extensions = { ...this._extensions,
           ...key
         };
       } // simple type check


       if (typeof key !== "string" && typeof key !== "object") {
         throw new Error("Parameter 1 needs to be either a string or an object.");
       } // spread them wings


       let stmt = { ...this._statementObject,
         extensions: this._extensions
       };
       this._statementObject = stmt;
     }

     initialize(id, name, description) {
       // set initialized activity
       this.setActivity(id, name, description); // set group to be used with other functions and statements

       this.setGroup(id, name, description);
       this._registration = this._registration ? this._registration : this._ADL.ruuid();
       let stmt = this.buildConfiguration(VERB$1.INITIALIZED, ACTIVITY$1.INITIALIZED, { ...this._statementObject
       });
       return this.sendStatement(stmt);
     }

     terminate(id, name, description) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       }

       let stmt = this.buildConfiguration(VERB$1.TERMINATED, ACTIVITY$1.TERMINATED, { ...this._statementObject,
         activity: this._statementObject.group,
         group: undefined
       });
       return this.sendStatement(stmt);
     }

     accessed(type, id, name, description, parent) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       } // force upper case since constant declarations are always uppercase
       // if a dash is part of the constant value then replace it with a underscore to match
       // the constant declaration name


       let accessedType = type.toUpperCase().replace("-", "_");

       if (type !== ACTIVITY$1.MENU && type !== ACTIVITY$1.MENU_ITEM) {
         throw new Error('accessed type must be either type "menu" or "menu-item"');
       }

       if (!parent && type === ACTIVITY$1.MENU_ITEM) {
         throw new Error("A parent object is required.");
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.ACCESSED, ACTIVITY$1[accessedType], { ...this._statementObject,
         parent
       });
       return this.sendStatement(stmt);
     } //ACTIVITY.LINK


     selected(type, id, name, description, parent) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       }

       if (type !== ACTIVITY$1.CHECKLIST_ITEM && type !== ACTIVITY$1.LINK) {
         throw new Error("Selected must be either type checklist item or link.");
       }

       if (!parent) {
         throw new Error("A parent object is required.");
       } // force upper case since constant declarations are always uppercase
       // if a dash is part of the constant value then replace it with a underscore to match
       // the constant declaration name


       const selected = type.toUpperCase().replace("-", "_"); // set activity

       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.SELECTED, ACTIVITY$1[selected], { ...this._statementObject,
         parent
       });
       return this.sendStatement(stmt);
     }

     deselected(type, id, name, description, parent) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       }

       if (!parent) {
         throw new Error("A parent object is required.");
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.DESELECTED, ACTIVITY$1.CHECKLIST_ITEM, { ...this._statementObject,
         parent
       });
       return this.sendStatement(stmt);
     }

     completedChecklist(id, name, description) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.COMPLETED, ACTIVITY$1.CHECKLIST, { ...this._statementObject
       });
       return this.sendStatement(stmt);
     }

     liked(type, id, name, description, parent) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       } // force upper case since constant declarations are always uppercase
       // if a dash is part of the constant value then replace it with a underscore to match
       // the constant declaration name


       const likedValue = type.toUpperCase().replace("-", "_");

       if (!ACTIVITY$1[likedValue]) {
         throw new Error(`${type} isn't a valid activity type.`);
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.LIKED, ACTIVITY$1[likedValue], { ...this._statementObject,
         parent
       });
       return this.sendStatement(stmt);
     }

     disliked(type, id, name, description, parent) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       } // force upper case since constant declarations are always uppercase
       // if a dash is part of the constant value then replace it with a underscore to match
       // the constant declaration name


       const dislikedValue = type.toUpperCase().replace("-", "_");

       if (!ACTIVITY$1[dislikedValue]) {
         throw new Error(`${type} isn't a valid activity type.`);
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.DISLIKED, ACTIVITY$1[dislikedValue], { ...this._statementObject,
         parent
       });
       return this.sendStatement(stmt);
     }

     searched(value, id, name, description) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.SEARCHED, ACTIVITY$1.SEARCH_ENGINE, { ...this._statementObject,
         result: {
           response: value
         }
       });
       return this.sendStatement(stmt);
     }

     viewed(value, id, name, description, parent) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       }

       let viewedValue = value.toLocaleLowerCase();

       if (viewedValue !== ACTIVITY$1.STEP && viewedValue !== ACTIVITY$1.PROCEDURE && viewedValue !== ACTIVITY$1.PAGE) {
         throw new Error('You must pass in either "step", "procedure", or "page" as a value.');
       }

       if (value === ACTIVITY$1.STEP && !parent) {
         throw new Error("A parent object is required.");
       } // set activity


       this.setActivity(id, name, description);
       let stmt = this.buildConfiguration(VERB$1.VIEWED, viewedValue, { ...this._statementObject,
         parent
       });
       return this.sendStatement(stmt);
     }

     uploaded(id, name, description, attachment) {
       // validate if a registration uuid has been assign yet
       let isReg = this.hasRegistration(this._registration);
       if (isReg.error) throw new Error(isReg.error); // is there an activity and group set yet?

       let validOptions = this.validateOptions(this._statementObject);

       if (validOptions.error) {
         throw new Error(validOptions.error);
       } // set activity


       this.setActivity(id, name, description); // build statement configuration

       let stmt = this.buildConfiguration(VERB$1.UPLOADED, ACTIVITY$1.FILE, { ...this._statementObject
       });
       return this.sendStatement(stmt, attachment);
     } // ACTIVITY CONSTANTS


     get ACTIVITY() {
       return ACTIVITY$1;
     } // EXTENSIONS CONSTATNS


     get EXTENSIONS() {
       return EXTENSIONS$1;
     } // statement to string


     get statement() {
       return JSON.stringify(this._statement, undefined, 2);
     }

   }

   exports.ELearningXAPI = ELearningXAPI;
   exports.PerformanceSupportXAPI = PerformanceSupportXAPI;

   Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=navy.umd.development.js.map

/** ***********************************************************************
*
* Veracity Technology Consultants CONFIDENTIAL
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Veracity Technology Consultants and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Veracity Technology Consultants
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Veracity Technology Consultants.
*/
export interface INameAndDescription {
    language: string;
}
export interface InteractionComponent {
    id: string;
    description: INameAndDescription;
}
export interface IDefinition {
    name?: INameAndDescription;
    description?: INameAndDescription;
    type?: string;
}
export interface ActivityContext {
    activity: {
        id: String;
        definition?: IDefinition;
    };
    contextActivity: {
        id: String;
        definition?: IDefinition;
    };
}
export interface IConfiguration {
    endpoint: string;
    auth: string;
    actor: {
        name: string;
        account: {
            homePage: string;
            name: string;
        };
        objectType: string;
    };
    platform?: string;
    strictCallbacks?: boolean;
}
export interface IAssessmentParent {
    id: string;
    definition?: IDefinition;
}
export interface InteractionDefinition {
    name?: {
        en: string;
    };
    description?: {
        en: string;
    };
    correctResponsesPattern?: Array<string>;
    choices?: InteractionComponent[];
    scale?: InteractionComponent[];
    source?: InteractionComponent[];
    target?: InteractionComponent[];
    steps?: InteractionComponent[];
}
export interface Interaction {
    id: string;
    interactionType: string;
    definition?: InteractionDefinition;
}
export interface Result {
    error?: string;
    data?: string[];
}
export interface Response {
    status: any;
    response: any;
}
export interface Statement {
    verb: string;
    actor: any;
    registration: string;
    extensions?: Array<string>;
    isCMI5?: boolean;
    isNCRP?: boolean;
    options?: any;
}
export interface IAttemptResults {
    duration?: string;
    completion?: boolean;
    success?: boolean;
    score?: {
        scaled?: number;
        raw?: number;
        min?: number;
        max?: number;
    };
}
export interface ILessonAttempt {
    registration: string;
    initialized?: boolean;
    terminated?: boolean;
    suspended?: boolean;
    resumed?: boolean;
    bookmark?: string;
    result?: IAttemptResults;
}
export interface ILesson {
    [key: string]: Array<ILessonAttempt>;
}
export interface ICourseAttempt {
    registration: string;
    initialized?: boolean;
    terminated?: boolean;
    lessons?: ILesson;
}
export interface IStandAloneLessonAttempt {
    attempts: Array<ILessonAttempt>;
}
export interface IAttachment {
    type: {
        usageType: string;
        display: {
            en: string;
        };
        description?: {
            en: string;
        };
        contentType: string;
    };
    value: string;
}
export interface IExtension<T> {
    [key: string]: T | {};
}

import { Result } from "./interfaces";
export declare class PerformanceSupportXAPI {
    private _config;
    private _ADL;
    private _statementObject;
    private _statement;
    private _extensions;
    private _registration;
    private _activityID;
    private _grouping;
    private _platform;
    constructor(config: any);
    private sendStatement;
    private launch;
    private buildConfiguration;
    private validateOptions;
    setActivity(id: string, name: string, description: string): void;
    setGroup(id: string, name: string, description: string): void;
    addExtensions(key: string | {}, value: string): void;
    createParent: (id: string, name: string, description: string) => any;
    initialize(id: string, name: string, description: string): Promise<Result>;
    terminate(id: string, name: string, description: string): Promise<Result>;
    accessed(type: string, id: string, name: string, description: string, parent: string): Promise<Result>;
    selected(type: string, id: string, name: string, description: string, parent: any): Promise<Result>;
    deselected(type: string, id: string, name: string, description: string, parent: any): Promise<Result>;
    completedChecklist(id: string, name: string, description: string): Promise<Result>;
    liked(type: string, id: string, name: string, description: string, parent: any): Promise<Result>;
    disliked(type: string, id: string, name: string, description: string, parent: any): Promise<Result>;
    searched(value: string, id: string, name: string, description: string): Promise<Result>;
    viewed(value: string, id: string, name: string, description: string, parent: any): Promise<Result>;
    opened: (type: string, id: string, name: string, description: string, parent: any) => Promise<Result>;
    closed: (type: string, id: string, name: string, description: string, parent: any) => Promise<Result>;
    printed: (type: string, id: string, name: string, description: string, parent: any) => Promise<Result>;
    attachment: (value: any, usageType: any, contentType: any, display: any, description: any) => {
        type: {
            usageType: any;
            display: {
                en: any;
            };
            description: {
                en: any;
            } | undefined;
            contentType: any;
        };
        value: any;
    }[];
    uploaded(id: string, name: string, description: string, attachment: any): Promise<Result>;
    private hasRegistration;
    private getUsageType;
    getAttemptRegistration: () => string;
    get ACTIVITY(): any;
    get EXTENSIONS(): any;
    get statement(): string;
}
